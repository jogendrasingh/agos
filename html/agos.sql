-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Host: 192.168.0.4
-- Generation Time: Nov 22, 2017 at 06:10 PM
-- Server version: 5.5.42-log
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agos`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_log`
--

CREATE TABLE IF NOT EXISTS `access_log` (
`id` bigint(20) NOT NULL,
  `login` varchar(150) NOT NULL,
  `role` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
`id` int(11) NOT NULL,
  `role` enum('loadmaster','admin') DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `request_token` text
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`id`, `role`, `login`, `email`, `name`, `password`, `active`, `request_token`) VALUES
(1, 'admin', 'admin', 'admin@agos.com', 'Admin', '$2y$10$kX.druVmFppvBmHTzNydIumoXV9a1ZYr6f2dliq6hFTlqPhsioxae', 1, ''),
(2, 'loadmaster', 'jogendra', 'jogendra.singh@formzero.in', 'Jogendra', '$2y$10$pVknSpCMKh3IOMi1rm2RguRw8Jh0HGllQ2shG/X33uTO0RYxmFLQi', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `name`, `client_code`, `active`, `created_at`, `updated_at`) VALUES
(6, 'Emirates', 'EK', '1', '2017-06-20 08:37:10', NULL),
(9, 'Atlas Air', '5Y', '1', '2017-06-20 08:40:16', NULL),
(10, 'ASL Airlines Belgium', '3V', '1', '2017-06-20 08:40:56', '2017-10-17 14:20:09'),
(11, 'AirBridgeCargo Airlines', 'RU', '1', '2017-06-20 08:41:53', '2017-06-20 08:42:40'),
(12, 'CargoLogicAir', 'P3', '1', '2017-06-20 08:42:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_configuration`
--

CREATE TABLE IF NOT EXISTS `cms_configuration` (
  `key` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_configuration`
--

INSERT INTO `cms_configuration` (`key`, `value`) VALUES
('ADMIN_EMAIL', 'webmaster@localhost'),
('DEBUG_MODE', '0'),
('DISPLAY_CHANGELOG', '0'),
('PROFILER', '0'),
('TECHNICAL_WORKS', '0'),
('WIDGETS_CACHE', '0');

-- --------------------------------------------------------

--
-- Table structure for table `cms_javascript`
--

CREATE TABLE IF NOT EXISTS `cms_javascript` (
  `id` varchar(20) NOT NULL,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_javascript`
--

INSERT INTO `cms_javascript` (`id`, `text`) VALUES
('body', '<!-- custom javascript code or any html -->'),
('head', '<!-- custom javascript code or any html -->');

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

CREATE TABLE IF NOT EXISTS `flights` (
`id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `support_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aircraft_registration_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`id`, `client_id`, `support_type`, `aircraft_registration_no`, `active`, `created_at`, `updated_at`) VALUES
(2, 9, 'local', NULL, '1', '2017-09-22 18:48:35', '2017-09-22 18:50:49'),
(3, 10, 'flying', NULL, '1', '2017-09-22 18:59:50', '2017-09-22 19:00:04'),
(4, 11, 'local', NULL, '1', '2017-10-05 16:25:21', '2017-10-05 16:27:46');

-- --------------------------------------------------------

--
-- Table structure for table `flight_assign`
--

CREATE TABLE IF NOT EXISTS `flight_assign` (
`id` bigint(20) NOT NULL,
  `flight_id` int(11) DEFAULT NULL,
  `leg_id` int(11) NOT NULL,
  `load_master_id` int(11) NOT NULL,
  `support` enum('Flying','Local') NOT NULL,
  `commercial` enum('Yes','No') DEFAULT NULL,
  `visa` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `flight_assign`
--

INSERT INTO `flight_assign` (`id`, `flight_id`, `leg_id`, `load_master_id`, `support`, `commercial`, `visa`, `created_at`, `updated_at`) VALUES
(1, NULL, 3, 1, 'Flying', NULL, 'Crew', '2017-06-19 01:19:38', NULL),
(2, NULL, 4, 1, 'Flying', NULL, 'General Declaration', '2017-06-20 00:05:57', NULL),
(3, NULL, 5, 1, 'Flying', NULL, 'General Declaration', '2017-06-20 08:52:00', NULL),
(4, NULL, 6, 1, 'Flying', NULL, 'Crew', '2017-06-20 08:52:01', NULL),
(5, NULL, 9, 1, 'Flying', NULL, 'General Declaration', '2017-07-10 20:05:16', NULL),
(6, NULL, 10, 1, 'Flying', NULL, 'Residence Visa', '2017-07-10 20:05:29', NULL),
(7, NULL, 14, 5, 'Local', NULL, 'Others', '2017-07-27 17:40:08', NULL),
(8, NULL, 7, 6, 'Flying', NULL, 'Crew', '2017-08-02 17:44:29', NULL),
(9, NULL, 18, 6, 'Flying', NULL, 'Crew', '2017-08-02 17:48:42', NULL),
(10, NULL, 19, 6, 'Flying', NULL, 'Crew', '2017-08-02 17:48:51', NULL),
(11, NULL, 20, 6, 'Flying', NULL, 'Crew', '2017-08-02 17:49:03', NULL),
(12, NULL, 22, 5, 'Flying', NULL, 'Residence Visa', '2017-09-15 18:55:05', NULL),
(13, NULL, 23, 5, 'Flying', NULL, 'Residence Visa', '2017-09-15 18:55:07', NULL),
(14, NULL, 24, 5, 'Flying', NULL, 'Residence Visa', '2017-09-15 18:55:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `flight_compliance`
--

CREATE TABLE IF NOT EXISTS `flight_compliance` (
`id` bigint(20) NOT NULL,
  `load_master_id` int(11) NOT NULL,
  `flight_id` int(11) NOT NULL,
  `leg_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `pfr` enum('Pending','Completed') DEFAULT NULL,
  `pfm` enum('Pending','Completed') DEFAULT NULL,
  `pfe` enum('Pending','Completed') DEFAULT NULL,
  `notes` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flight_expenses`
--

CREATE TABLE IF NOT EXISTS `flight_expenses` (
`id` int(11) NOT NULL,
  `load_master_id` int(11) NOT NULL,
  `flight_assign_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `fx_rate` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `assign_to` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `flight_expenses`
--

INSERT INTO `flight_expenses` (`id`, `load_master_id`, `flight_assign_id`, `date`, `category`, `amount`, `currency`, `fx_rate`, `country`, `assign_to`, `created_at`, `updated_at`, `status`) VALUES
(1, 6, 9, '2017-02-08 21:00:00', 'Taxi (Hotel-AP)', 1000, 'INR', '100', NULL, 'Reimbursible', '2017-08-02 18:14:39', '2017-08-02 18:18:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `flight_legs`
--

CREATE TABLE IF NOT EXISTS `flight_legs` (
`id` int(10) unsigned NOT NULL,
  `flight_id` int(10) unsigned NOT NULL,
  `flight_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `station_source_id` int(10) unsigned NOT NULL,
  `std` datetime NOT NULL,
  `station_dest_id` int(10) unsigned NOT NULL,
  `trained` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `sta` datetime NOT NULL,
  `commercial` int(11) DEFAULT NULL,
  `reimbursable` int(11) DEFAULT NULL,
  `reimbursableClient` int(11) DEFAULT NULL,
  `etd` datetime DEFAULT NULL,
  `eta` datetime DEFAULT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `flight_legs`
--

INSERT INTO `flight_legs` (`id`, `flight_id`, `flight_no`, `station_source_id`, `std`, `station_dest_id`, `trained`, `sta`, `commercial`, `reimbursable`, `reimbursableClient`, `etd`, `eta`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'test01', 2, '2017-09-22 18:30:00', 8, '1', '2017-09-22 18:30:00', 1, 1, 9, '2017-09-22 18:30:00', '2017-09-22 18:30:00', '1', '2017-09-22 15:06:21', NULL),
(2, 2, '5Y 5201', 13, '2019-01-09 10:40:00', 28, '0', '2019-02-09 00:10:00', 0, 0, 0, '2019-01-09 10:40:00', '2019-02-09 00:10:00', '1', '2017-09-22 18:48:35', '2017-09-22 18:50:49'),
(3, 3, '9W 546', 30, '2018-10-09 15:10:00', 10, '0', '2018-10-09 19:10:00', 1, 1, 10, '2018-10-09 15:10:00', '2018-10-09 19:10:00', '1', '2017-09-22 18:59:50', '2017-09-22 19:00:04'),
(4, 4, 'RU 9394', 31, '2017-10-02 12:30:00', 13, '0', '2017-10-02 12:30:00', 0, 0, 0, '2017-10-02 18:35:00', '2017-10-02 12:30:00', '1', '2017-10-05 16:25:21', '2017-10-05 16:27:46'),
(5, 4, 'RU 9770', 13, '2017-10-02 08:35:00', 31, '1', '2017-02-10 15:30:00', 0, 0, 0, '2017-10-02 08:35:00', '2017-10-05 15:30:00', '1', '2017-10-05 16:25:21', '2017-10-05 16:27:46');

-- --------------------------------------------------------

--
-- Table structure for table `flight_movement`
--

CREATE TABLE IF NOT EXISTS `flight_movement` (
`id` bigint(20) NOT NULL,
  `load_master_id` int(11) DEFAULT NULL,
  `flight_id` int(11) DEFAULT NULL,
  `leg_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `source_etd` datetime DEFAULT NULL,
  `push_back_time` datetime DEFAULT NULL,
  `airborne_time` datetime DEFAULT NULL,
  `destination_eta` datetime DEFAULT NULL,
  `touch_down_time` datetime DEFAULT NULL,
  `chocks_on` datetime DEFAULT NULL,
  `delay_reason` text,
  `incoming_payload` varchar(255) DEFAULT NULL,
  `offload` varchar(255) DEFAULT NULL,
  `transit` varchar(255) DEFAULT NULL,
  `joining` varchar(255) DEFAULT NULL,
  `total_payload` varchar(255) DEFAULT NULL,
  `notes` text,
  `uws_received` datetime DEFAULT NULL,
  `lir_issued` datetime DEFAULT NULL,
  `loading_start` datetime DEFAULT NULL,
  `loading_finish` datetime DEFAULT NULL,
  `pfr` enum('Pending','Completed') DEFAULT NULL,
  `pfm` enum('Pending','Completed') DEFAULT NULL,
  `pfe` enum('Pending','Completed') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `assign_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `flight_movement`
--

INSERT INTO `flight_movement` (`id`, `load_master_id`, `flight_id`, `leg_id`, `date`, `source_etd`, `push_back_time`, `airborne_time`, `destination_eta`, `touch_down_time`, `chocks_on`, `delay_reason`, `incoming_payload`, `offload`, `transit`, `joining`, `total_payload`, `notes`, `uws_received`, `lir_issued`, `loading_start`, `loading_finish`, `pfr`, `pfm`, `pfe`, `created_at`, `updated_at`, `assign_id`) VALUES
(2, NULL, NULL, NULL, '2017-06-20', '2018-06-08 01:38:00', '2018-06-08 01:38:00', '2018-06-08 01:38:00', '2018-06-08 01:38:00', '2018-06-08 01:38:00', '2018-06-08 01:38:00', 'No', '2000', '100', '1900', '50', '1950', 'sdf', '2018-06-08 01:45:00', '2018-06-08 01:45:00', '2018-06-08 01:45:00', '2018-06-08 01:45:00', NULL, NULL, NULL, '2017-06-20 01:45:38', '2017-06-20 02:02:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
`id` int(11) NOT NULL,
  `iso` varchar(10) NOT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `short_name` varchar(10) DEFAULT NULL,
  `url` varchar(20) DEFAULT NULL,
  `sortorder` int(11) DEFAULT NULL,
  `primary` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `iso`, `locale`, `name`, `short_name`, `url`, `sortorder`, `primary`) VALUES
(2, 'en', 'en_EN', 'English', 'Eng', 'en', 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `load_masters`
--

CREATE TABLE IF NOT EXISTS `load_masters` (
`id` bigint(20) NOT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `middle_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `pan_number` varchar(150) DEFAULT NULL,
  `country_of_residence` varchar(150) DEFAULT NULL,
  `country_of_origin` varchar(150) DEFAULT NULL,
  `visa` varchar(150) DEFAULT NULL,
  `country` varchar(150) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `block_hour_rate` float DEFAULT NULL,
  `flight_allowance` varchar(150) DEFAULT NULL,
  `daily_allowance_intl` varchar(150) DEFAULT NULL,
  `daily_allowance_dom` varchar(150) DEFAULT NULL,
  `block_cur` varchar(10) DEFAULT NULL,
  `flight_allowance_cur` varchar(10) DEFAULT NULL,
  `allowance_intl_cur` varchar(10) DEFAULT NULL,
  `allowance_dom_cur` varchar(10) DEFAULT NULL,
  `active` enum('0','1') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `load_masters`
--

INSERT INTO `load_masters` (`id`, `first_name`, `middle_name`, `last_name`, `date_of_birth`, `pan_number`, `country_of_residence`, `country_of_origin`, `visa`, `country`, `issue_date`, `expiry_date`, `block_hour_rate`, `flight_allowance`, `daily_allowance_intl`, `daily_allowance_dom`, `block_cur`, `flight_allowance_cur`, `allowance_intl_cur`, `allowance_dom_cur`, `active`, `created_at`, `updated_at`, `admin_id`) VALUES
(1, 'Amit', 'kumar', 'Misra', '2017-06-07', 'CAHPS3434', 'India', 'India', 'INDDSAFA', 'India', '2017-06-05', '2017-06-13', 12, '12', '34', '12', NULL, NULL, NULL, NULL, '1', '2017-06-17 07:42:53', '2017-06-17 13:52:59', 0),
(3, 'Sachin', '', 'Sharma', '2000-06-01', 'CAHPS3434', 'India', 'India', 'VISA1', 'India', '2017-02-07', '2018-05-31', 12, '36', '12', '12', NULL, NULL, NULL, NULL, '1', '2017-06-18 21:59:13', NULL, 0),
(4, 'Kerry', 'kumar', 'Misra', '1998-06-01', 'PAN1234', 'India', 'India', 'INDDS', 'India', '2015-06-07', '2020-06-15', 400, '400', '200', '250', NULL, NULL, NULL, NULL, '1', '2017-06-18 22:00:34', NULL, 0),
(5, 'Rio', 'Peter Clive', 'Sura', '1992-05-01', 'XYZ', 'India', 'India', 'UAE', 'UAE', '0000-00-00', '0000-00-00', 2.5, '100', '170', '750', 'INR', 'INR', 'INR', 'INR', '1', '2017-07-10 20:09:49', '2017-07-19 11:00:02', 0),
(6, 'Jogendra', '', 'Singh', '1984-08-02', 'AAAAA1111A', 'India', 'India', 'INDIA', 'INDIA', '2017-04-04', '2020-08-16', 500, '600', '800', '400', 'INR', 'INR', 'INR', 'INR', '1', '2017-08-02 17:42:34', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `load_master_training`
--

CREATE TABLE IF NOT EXISTS `load_master_training` (
`id` int(11) NOT NULL,
  `load_master_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `training_date` date DEFAULT NULL,
  `expiry` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=125 ;

--
-- Dumping data for table `load_master_training`
--

INSERT INTO `load_master_training` (`id`, `load_master_id`, `training_id`, `training_date`, `expiry`) VALUES
(1, 3, 1, '2017-07-03', '2017-07-10'),
(2, 3, 2, '2017-07-10', '2017-07-18'),
(3, 3, 3, '2017-07-10', '0000-00-00'),
(4, 3, 4, '0000-00-00', '0000-00-00'),
(5, 3, 5, '0000-00-00', '0000-00-00'),
(6, 3, 6, '0000-00-00', '0000-00-00'),
(7, 3, 7, '0000-00-00', '0000-00-00'),
(8, 3, 8, '0000-00-00', '0000-00-00'),
(9, 3, 9, '0000-00-00', '0000-00-00'),
(10, 3, 10, '0000-00-00', '0000-00-00'),
(11, 3, 11, '0000-00-00', '0000-00-00'),
(12, 3, 12, '0000-00-00', '0000-00-00'),
(13, 3, 13, '0000-00-00', '0000-00-00'),
(14, 3, 14, '0000-00-00', '0000-00-00'),
(15, 3, 15, '0000-00-00', '0000-00-00'),
(16, 3, 16, '0000-00-00', '0000-00-00'),
(17, 3, 17, '0000-00-00', '0000-00-00'),
(18, 3, 18, '0000-00-00', '0000-00-00'),
(19, 3, 19, '0000-00-00', '0000-00-00'),
(20, 3, 20, '0000-00-00', '0000-00-00'),
(21, 3, 21, '0000-00-00', '0000-00-00'),
(22, 3, 22, '0000-00-00', '0000-00-00'),
(23, 3, 23, '0000-00-00', '0000-00-00'),
(24, 3, 24, '0000-00-00', '0000-00-00'),
(25, 3, 25, '0000-00-00', '0000-00-00'),
(26, 3, 26, '0000-00-00', '0000-00-00'),
(27, 3, 27, '0000-00-00', '0000-00-00'),
(28, 3, 28, '0000-00-00', '0000-00-00'),
(29, 3, 29, '0000-00-00', '0000-00-00'),
(30, 3, 30, '0000-00-00', '0000-00-00'),
(31, 3, 31, '0000-00-00', '0000-00-00'),
(32, 3, 32, '0000-00-00', '0000-00-00'),
(33, 3, 33, '0000-00-00', '0000-00-00'),
(34, 3, 34, '0000-00-00', '0000-00-00'),
(35, 3, 35, '0000-00-00', '0000-00-00'),
(36, 3, 36, '0000-00-00', '0000-00-00'),
(37, 3, 37, '0000-00-00', '0000-00-00'),
(38, 3, 38, '0000-00-00', '0000-00-00'),
(39, 3, 39, '0000-00-00', '0000-00-00'),
(40, 3, 40, '0000-00-00', '0000-00-00'),
(41, 3, 41, '0000-00-00', '0000-00-00'),
(42, 3, 42, '0000-00-00', '0000-00-00'),
(43, 3, 43, '0000-00-00', '0000-00-00'),
(44, 3, 44, '0000-00-00', '0000-00-00'),
(45, 3, 45, '0000-00-00', '0000-00-00'),
(46, 3, 46, '0000-00-00', '0000-00-00'),
(47, 3, 47, '0000-00-00', '0000-00-00'),
(48, 3, 48, '0000-00-00', '0000-00-00'),
(49, 3, 49, '0000-00-00', '0000-00-00'),
(50, 3, 50, '0000-00-00', '0000-00-00'),
(51, 3, 51, '0000-00-00', '0000-00-00'),
(52, 3, 52, '0000-00-00', '0000-00-00'),
(53, 3, 53, '0000-00-00', '0000-00-00'),
(54, 3, 54, '0000-00-00', '0000-00-00'),
(55, 3, 55, '0000-00-00', '0000-00-00'),
(56, 3, 56, '0000-00-00', '0000-00-00'),
(57, 3, 57, '0000-00-00', '0000-00-00'),
(58, 3, 58, '0000-00-00', '0000-00-00'),
(59, 3, 59, '0000-00-00', '0000-00-00'),
(60, 3, 60, '0000-00-00', '0000-00-00'),
(61, 3, 61, '0000-00-00', '0000-00-00'),
(62, 3, 62, '0000-00-00', '0000-00-00'),
(63, 4, 1, '2017-07-03', '2017-07-10'),
(64, 4, 2, '2017-07-03', '2017-07-10'),
(65, 4, 3, '2017-07-10', '2017-07-18'),
(66, 4, 4, '0000-00-00', '0000-00-00'),
(67, 4, 5, '0000-00-00', '0000-00-00'),
(68, 4, 6, '0000-00-00', '0000-00-00'),
(69, 4, 7, '0000-00-00', '0000-00-00'),
(70, 4, 8, '0000-00-00', '0000-00-00'),
(71, 4, 9, '0000-00-00', '0000-00-00'),
(72, 4, 10, '0000-00-00', '0000-00-00'),
(73, 4, 11, '0000-00-00', '0000-00-00'),
(74, 4, 12, '0000-00-00', '0000-00-00'),
(75, 4, 13, '0000-00-00', '0000-00-00'),
(76, 4, 14, '0000-00-00', '0000-00-00'),
(77, 4, 15, '0000-00-00', '0000-00-00'),
(78, 4, 16, '0000-00-00', '0000-00-00'),
(79, 4, 17, '0000-00-00', '0000-00-00'),
(80, 4, 18, '0000-00-00', '0000-00-00'),
(81, 4, 19, '0000-00-00', '0000-00-00'),
(82, 4, 20, '0000-00-00', '0000-00-00'),
(83, 4, 21, '0000-00-00', '0000-00-00'),
(84, 4, 22, '0000-00-00', '0000-00-00'),
(85, 4, 23, '0000-00-00', '0000-00-00'),
(86, 4, 24, '0000-00-00', '0000-00-00'),
(87, 4, 25, '0000-00-00', '0000-00-00'),
(88, 4, 26, '0000-00-00', '0000-00-00'),
(89, 4, 27, '0000-00-00', '0000-00-00'),
(90, 4, 28, '0000-00-00', '0000-00-00'),
(91, 4, 29, '0000-00-00', '0000-00-00'),
(92, 4, 30, '0000-00-00', '0000-00-00'),
(93, 4, 31, '0000-00-00', '0000-00-00'),
(94, 4, 32, '0000-00-00', '0000-00-00'),
(95, 4, 33, '0000-00-00', '0000-00-00'),
(96, 4, 34, '0000-00-00', '0000-00-00'),
(97, 4, 35, '0000-00-00', '0000-00-00'),
(98, 4, 36, '0000-00-00', '0000-00-00'),
(99, 4, 37, '0000-00-00', '0000-00-00'),
(100, 4, 38, '0000-00-00', '0000-00-00'),
(101, 4, 39, '0000-00-00', '0000-00-00'),
(102, 4, 40, '0000-00-00', '0000-00-00'),
(103, 4, 41, '0000-00-00', '0000-00-00'),
(104, 4, 42, '0000-00-00', '0000-00-00'),
(105, 4, 43, '0000-00-00', '0000-00-00'),
(106, 4, 44, '0000-00-00', '0000-00-00'),
(107, 4, 45, '0000-00-00', '0000-00-00'),
(108, 4, 46, '0000-00-00', '0000-00-00'),
(109, 4, 47, '0000-00-00', '0000-00-00'),
(110, 4, 48, '0000-00-00', '0000-00-00'),
(111, 4, 49, '0000-00-00', '0000-00-00'),
(112, 4, 50, '0000-00-00', '0000-00-00'),
(113, 4, 51, '0000-00-00', '0000-00-00'),
(114, 4, 52, '0000-00-00', '0000-00-00'),
(115, 4, 53, '0000-00-00', '0000-00-00'),
(116, 4, 54, '0000-00-00', '0000-00-00'),
(117, 4, 55, '0000-00-00', '0000-00-00'),
(118, 4, 56, '0000-00-00', '0000-00-00'),
(119, 4, 57, '0000-00-00', '0000-00-00'),
(120, 4, 58, '0000-00-00', '0000-00-00'),
(121, 4, 59, '0000-00-00', '0000-00-00'),
(122, 4, 60, '0000-00-00', '0000-00-00'),
(123, 4, 61, '0000-00-00', '0000-00-00'),
(124, 4, 62, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL,
  `role` enum('maker','checker') NOT NULL DEFAULT 'maker',
  `login` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`id` int(11) NOT NULL,
  `root` enum('top') NOT NULL DEFAULT 'top',
  `parent_id` int(11) DEFAULT NULL,
  `work_title` varchar(255) DEFAULT NULL,
  `depth` tinyint(2) NOT NULL DEFAULT '0',
  `left_key` int(11) DEFAULT NULL,
  `right_key` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_translate`
--

CREATE TABLE IF NOT EXISTS `menu_translate` (
`id` int(11) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
`id` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'index', '2014-08-03 15:18:47', '2015-06-18 16:00:29'),
(2, 'contacts', '2014-08-03 22:25:13', '2015-06-18 16:08:00'),
(3, 'About Us', '2015-11-18 01:07:22', '2015-11-18 01:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `page_translate`
--

CREATE TABLE IF NOT EXISTS `page_translate` (
`id` int(11) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `page_translate`
--

INSERT INTO `page_translate` (`id`, `foreign_id`, `lang`, `key`, `value`) VALUES
(6, 1, 'en', 'title', 'Homepage'),
(7, 1, 'en', 'meta_title', 'Homepage'),
(8, 1, 'en', 'meta_description', 'meta-description of homepage'),
(9, 1, 'en', 'meta_keywords', ''),
(10, 1, 'en', 'text', '<h1>Yona CMS</h1>\r\n<p>Yona CMS - open source content management system. Written in <a href="http://phalconphp.com/" target="_blank">Phalcon PHP Framework</a>&nbsp;(version 1.3.x).</p>\r\n<p>Has a convenient modular structure. It is intended for the design of simple sites, and major portals and web applications. Thanks to its simple configuration and architecture, can be easily modified for any task.</p>\r\n<p>The official repository on&nbsp;<a href="https://github.com/oleksandr-torosh/yona-cms" target="_blank">Github</a></p>\r\n<h2>Subtitle</h2>\r\n<p>Proin aliquet eros vel magna semper facilisis. Nunc tellus urna, bibendum vitae malesuada vel, molestie non lectus. Suspendisse sit amet ante arcu. Maecenas interdum eu neque eu dapibus. Sed maximus elementum tortor at dapibus. Phasellus rhoncus odio vel suscipit dapibus. Nullam sed luctus mauris. Nunc blandit vitae nisl at malesuada. Sed ac est ut diam hendrerit sodales quis et massa. Proin aliquet vitae massa luctus ultricies. Nullam accumsan leo nibh, non varius tortor elementum auctor. Fusce sollicitudin a dui porttitor euismod. Ut at iaculis neque, nec finibus diam. Integer pharetra vehicula urna vitae imperdiet.</p>\r\n<h3>sub-subtitle</h3>\r\n<p>List:</p>\r\n<ul>\r\n<li>First item</li>\r\n<li>Second item<br />\r\n<ul>\r\n<li>Inner level of second item</li>\r\n<li>Another one</li>\r\n</ul>\r\n</li>\r\n<li>Third item</li>\r\n</ul>\r\n<p>Table</p>\r\n<table class="table" style="width: 100%;">\r\n<tbody>\r\n<tr><th>Header</th><th>Header</th><th>Header</th></tr>\r\n<tr>\r\n<td>Text in cell</td>\r\n<td>Text in cell</td>\r\n<td>Text in cell</td>\r\n</tr>\r\n<tr>\r\n<td>Text in cell</td>\r\n<td>Text in cell</td>\r\n<td>Text in cell</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Decimal list:</p>\r\n<ol>\r\n<li>First</li>\r\n<li>Second</li>\r\n<li>Third</li>\r\n</ol>'),
(16, 2, 'en', 'title', 'Contacts'),
(17, 2, 'en', 'meta_title', 'Contacts'),
(18, 2, 'en', 'meta_description', ''),
(19, 2, 'en', 'meta_keywords', ''),
(20, 2, 'en', 'text', '<h1>Contacts</h1>\r\n<p><a href="http://yonacms.com">http://yonacms.com</a></p>'),
(31, 3, 'en', 'title', 'About Us'),
(32, 3, 'en', 'text', '<p>About Us</p>'),
(33, 3, 'en', 'meta_title', 'About Us');

-- --------------------------------------------------------

--
-- Table structure for table `pg`
--

CREATE TABLE IF NOT EXISTS `pg` (
`id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pg`
--

INSERT INTO `pg` (`id`, `category_id`, `title`, `content`, `created_at`) VALUES
(1, 3, 'Page 1', '<p>Page 1 Content</p>', '2015-11-18 11:16:26'),
(2, 2, 'My First Page', '<p>My First Page Content here updated ................</p>', '2015-11-18 02:32:30'),
(3, 1, 'About Us', '<p>About Us Content here updated&nbsp; ....................</p>', '2015-11-18 02:35:38'),
(5, 2, 'custom page', '<p><span class="c1"> This will create a route named page, which will be responsible to URLs:</span> <span class="c1">// "/pagename.html", "/en/pagename.html" with definition of current language</span> <span class="nv">$router</span><span class="o">-&gt;</span><span class="na">addML</span><span class="p">(</span><span class="s1">''/{<span class="highlighted">slug</span>:[a-zA-Z_-]+}.html''</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span> <span class="s1">''module''</span> <span class="o">=&gt;</span> <span class="s1">''page''</span><span class="p">,</span> <span class="s1">''controller''</span> <span class="o">=&gt;</span> <span class="s1">''index''</span><span class="p">,</span> <span class="s1">''action''</span> <span class="o">=&gt;</span> <span class="s1">''index''</span> <span class="p">),</span> <span class="s1">''page''</span><span class="p">);</span></p>', '2015-11-18 02:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
`id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `preview_inner` enum('1','0') DEFAULT '1',
  `preview_src` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`id`, `type_id`, `slug`, `created_at`, `updated_at`, `date`, `preview_inner`, `preview_src`) VALUES
(1, 1, 'phalcon-132-released', '2014-08-22 10:33:26', '2015-06-26 16:48:36', '2014-08-19 00:00:00', '0', 'img/original/publication/0/1.jpg'),
(2, 1, 'phalcon-community-hangout', '2014-08-22 10:42:08', '2015-06-26 16:48:44', '2014-08-21 00:00:00', '1', 'img/original/publication/0/2.jpg'),
(3, 2, 'builtwith-phalcon', '2014-11-05 18:00:20', '2015-06-26 16:48:53', '2014-11-05 00:00:00', '1', 'img/original/publication/0/3.jpg'),
(4, 2, 'vtoraya-statya', '2014-11-06 18:23:17', '2015-06-26 16:49:02', '2014-11-06 00:00:00', '0', 'img/original/publication/0/4.jpg'),
(5, 1, 'new-modular-widgets-system', '2015-04-29 10:42:49', '2015-06-30 17:12:13', '2015-06-05 14:32:44', '0', 'img/original/publication/0/5.jpg'),
(6, 3, 'sql', '2015-11-17 12:12:03', '2015-11-17 12:16:56', '2015-11-17 12:12:03', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `publication_translate`
--

CREATE TABLE IF NOT EXISTS `publication_translate` (
`id` int(11) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `publication_translate`
--

INSERT INTO `publication_translate` (`id`, `foreign_id`, `lang`, `key`, `value`) VALUES
(11, 1, 'en', 'title', 'Phalcon 1.3.2 Released'),
(12, 1, 'en', 'meta_title', 'Phalcon 1.3.2 Released'),
(13, 1, 'en', 'meta_description', ''),
(14, 1, 'en', 'meta_keywords', ''),
(15, 1, 'en', 'text', '<p>We are today releasing the much awaited 1.3.2 version.&nbsp;</p>\r\n<p>This version has a ton of contributions from our community and fixes to the framework. We thank everyone that has worked on this release, especially with their contributions both to 1.3.2 and our work in progress 2.0.0.</p>\r\n<p>Many thanks to dreamsxin, <a href="https://github.com/mruz">mruz</a>, <a href="https://github.com/kjdev">kjdev</a>, <a href="https://github.com/Cinderella-Man">Cinderella-Man</a>, <a href="https://github.com/andreadelfino">andreadelfino</a>, <a href="https://github.com/kfll">kfll</a>, <a href="https://github.com/brandonlamb">brandonlamb</a>, <a href="https://github.com/zacek">zacek</a>, <a href="https://github.com/joni">joni</a>, <a href="https://github.com/wandersonwhcr">wandersonwhcr</a>, <a href="https://github.com/kevinhatry">kevinhatry</a>, <a href="https://github.com/alkana">alkana</a> and many others that have contributed either on <a href="https://github.com/phalcon/cphalcon">Github or through discussion in our </a><a href="http://forum.phalconphp.com/">forum</a>.</p>\r\n<p>The changelog can be found <a href="https://github.com/phalcon/cphalcon/blob/master/CHANGELOG">here</a>.</p>\r\n<p>We also have a number of pull requests that have not made it to 1.3.2 but will be included to 1.3.3. We need to make sure that the fix or feature that each pull request offers are present both in 1.3.3 but also in 2.0.0</p>\r\n<p>A big thank you once again to our community! You guys are awesome!</p>\r\n<p>&lt;3 Phalcon Team</p>'),
(21, 2, 'en', 'title', 'Phalcon Community Hangout'),
(22, 2, 'en', 'meta_title', 'Phalcon Community Hangout'),
(23, 2, 'en', 'meta_description', ''),
(24, 2, 'en', 'meta_keywords', ''),
(25, 2, 'en', 'text', '<p>Yesterday (2014-04-05) we had our first Phalcon community hangout. The main purpose of the hangout was to meet the community, discuss about what Phalcon is and what our future steps are, and hear news, concerns, success stories from the community itself.</p>\r\n<p>We are excited to announce that the first Phalcon community hangout was a great success!</p>\r\n<p>We had an awesome turnout from all around the world, with members of the community filling the hangout (10 concurrent users) and more viewing online, asking questions and interacting with the team.</p>\r\n<p>We talked about where we are, where we came from and what the future steps are with Zephir and Phalcon 2.0. Contributions, bugs and NFRs were also topics in our discussion, as well as who are team and how Phalcon is funded.</p>\r\n<p>More hangouts will be scheduled in the near future, hopefully making this a regular event for our community. We will also cater for members of the community that are not English speakers, by creating hangouts for Spanish speaking, Russian etc. The goal is to engage as many members as possible!</p>\r\n<p>The love and trust you all have shown to our framework is what drives us to make it better, push performance, introduce more features and make Phalcon the best PHP framework there is.&nbsp;</p>\r\n<p>For those that missed it, the video is below.</p>'),
(41, 3, 'en', 'title', 'BuiltWith Phalcon'),
(42, 3, 'en', 'meta_title', 'BuiltWith Phalcon'),
(43, 3, 'en', 'meta_description', ''),
(44, 3, 'en', 'meta_keywords', ''),
(45, 3, 'en', 'text', '<p>Today we are launching a new site that would help us spread the word about Phalcon and show where Phalcon is used, whether this is production applications, hobby projects or tutorials.</p>\r\n<p>Introducing <a href="http://builtwith.phalconphp.com/">builtwith.phalconphp.com</a></p>\r\n<p>Taking the example from our friends at <a href="http://www.angularjs.org/">AngularJS</a> we have cloned their <a href="https://github.com/angular/builtwith.angularjs.org">repository</a> and we have Phalcon-ized it. Special thanks to the <a href="http://en.wikipedia.org/wiki/AngularJS">AngularJS</a>team as well as <a href="https://github.com/oaass">Ole Aass</a> (<a href="http://oleaass.com/">website</a>) who is leading the project.</p>\r\n<p>The new site has a very easy interface that users can navigate to and even search for projects with tags.&nbsp;</p>\r\n<p>You can add your own project by simply cloning our <a href="https://github.com/phalcon/builtwith">repository</a> and adding your project as well as a logo and screenshots and then issue a pull request for it to appear in the live site.</p>\r\n<p>Looking forward to seeing your projects listed up there!</p>\r\n<p>&lt;3 The Phalcon Team</p>'),
(51, 4, 'en', 'title', 'Second article'),
(52, 4, 'en', 'meta_title', 'Second article'),
(53, 4, 'en', 'meta_description', ''),
(54, 4, 'en', 'meta_keywords', ''),
(55, 4, 'en', 'text', '<p>Second article text</p>'),
(61, 5, 'en', 'title', 'New modular widgets system'),
(62, 5, 'en', 'meta_title', 'New widgets system'),
(63, 5, 'en', 'meta_description', ''),
(64, 5, 'en', 'meta_keywords', ''),
(65, 5, 'en', 'text', '<p>Here is the new features of YonaCMS - "System of modular widgets".</p>\r\n<p>Now, in any of your modules, you can create dynamic widgets with their business logic and templates. Forget about dozens of separate helper and the need to do the same routine operations! Also, this scheme will maintain cleanliness and order in the code for your project.</p>\r\n<p>Call each widget can be produced directly from the template Volt with the transfer set of parameters. Each widget is automatically cached and does not lead to additional load on the database. Caching can be disabled in the administrative panel, see Admin -&gt; Settings, option "Widgets caching". Automatic regeneration of the cache is carried out after 60 seconds.</p>\r\n<p>As an example of such a call is made to the widget template''s main page /app/modules/Index/views/index.volt</p>\r\n<pre>{{Helper.widget (''Publication''). LastNews ()}}</pre>\r\n<p><br />Files widget:<br />/app/modules/Publication/Widget/PublicationWidget.php - inherits \\ Application \\ Widget \\ AbstractWidget<br />/app/modules/Publication/views/widget/last-news.volt - template output</p>\r\n<p>The main class of the widget - \\ Application \\ Widget \\ Proxy<br />It is possible to set the default value for time caching.</p>\r\n<p>This system will be very useful for developers who have a lot of individual information units, as well as those who want to keep your code clean and easy tool to use.</p>'),
(76, 6, 'en', 'title', 'sql'),
(77, 6, 'en', 'text', '<div>\r\n<div class="lc">\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n<div class="rc">\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>\r\n</div>\r\n<p>&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32</p>'),
(78, 6, 'en', 'meta_title', 'My First page');

-- --------------------------------------------------------

--
-- Table structure for table `publication_type`
--

CREATE TABLE IF NOT EXISTS `publication_type` (
`id` int(11) NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `limit` int(4) DEFAULT NULL,
  `format` enum('list','grid') DEFAULT NULL,
  `display_date` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `publication_type`
--

INSERT INTO `publication_type` (`id`, `slug`, `limit`, `format`, `display_date`) VALUES
(1, 'news', 10, 'grid', '1'),
(2, 'articles', 10, 'list', '0'),
(3, 'category', 10, 'list', '1'),
(4, 'erte', 10, 'list', '0');

-- --------------------------------------------------------

--
-- Table structure for table `publication_type_translate`
--

CREATE TABLE IF NOT EXISTS `publication_type_translate` (
`id` int(11) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `publication_type_translate`
--

INSERT INTO `publication_type_translate` (`id`, `foreign_id`, `lang`, `key`, `value`) VALUES
(54, 1, 'en', 'title', 'News'),
(55, 1, 'en', 'head_title', 'News'),
(56, 1, 'en', 'meta_description', ''),
(57, 1, 'en', 'meta_keywords', ''),
(58, 1, 'en', 'seo_text', ''),
(70, 2, 'en', 'title', 'Articles'),
(71, 2, 'en', 'head_title', 'Articles'),
(72, 2, 'en', 'meta_description', ''),
(73, 2, 'en', 'meta_keywords', ''),
(74, 2, 'en', 'seo_text', ''),
(80, 3, 'en', 'title', 'Category'),
(81, 3, 'en', 'head_title', 'Category'),
(82, 4, 'en', 'title', 'etrerw'),
(83, 4, 'en', 'head_title', 'etrerw');

-- --------------------------------------------------------

--
-- Table structure for table `seo_manager`
--

CREATE TABLE IF NOT EXISTS `seo_manager` (
`id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `head_title` varchar(500) DEFAULT NULL,
  `meta_description` varchar(500) DEFAULT NULL,
  `meta_keywords` varchar(500) DEFAULT NULL,
  `seo_text` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `seo_manager`
--

INSERT INTO `seo_manager` (`id`, `url`, `head_title`, `meta_description`, `meta_keywords`, `seo_text`, `created_at`, `updated_at`) VALUES
(1, '/news', 'Latest News', 'Greate latest and fresh news!', 'news, latest news, fresh news', '<p>Presenting your attention the latest news!</p>', '2014-09-30 10:39:23', '2015-07-02 11:28:57'),
(2, '/contacts.html', 'Yona CMS Contacts', '', '', '', '2015-05-21 16:33:14', '2015-07-02 11:19:40');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
`id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state_name`) VALUES
(1, 'Andhra Pradesh'),
(2, 'Arunachal Pradesh'),
(3, 'Assam'),
(4, 'Bihar'),
(5, 'Chattisgarh'),
(6, 'Goa'),
(7, 'Gujarat'),
(8, 'Haryana'),
(9, 'Himachal Pradesh'),
(10, 'Jammu and Kashmir'),
(11, 'Jharkhand'),
(12, 'Karnataka'),
(13, 'Kerala'),
(14, 'Madhya Pradesh'),
(15, 'Maharashtra'),
(16, 'Manipur'),
(17, 'Meghalaya'),
(18, 'Mizoram'),
(19, 'Nagaland'),
(20, 'Odisha'),
(21, 'Punjab'),
(22, 'Rajasthan'),
(23, 'Sikkim'),
(24, 'Tamil Nadu'),
(25, 'Telangana'),
(26, 'Tripura'),
(27, 'Uttar Pradesh'),
(28, 'Uttarakhand'),
(29, 'West Bengal'),
(31, 'Delhi');

-- --------------------------------------------------------

--
-- Table structure for table `stations`
--

CREATE TABLE IF NOT EXISTS `stations` (
`id` int(10) unsigned NOT NULL,
  `airport_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `station_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `stations`
--

INSERT INTO `stations` (`id`, `airport_name`, `station_code`, `country`, `state`, `city`, `active`, `created_at`, `updated_at`) VALUES
(2, 'JAMMU AIRPORT', 'JAM001', 'India', 'J&K', 'Jammu', '0', '2017-06-16 14:45:59', '2017-06-20 08:23:34'),
(6, 'Al Maktoum International Airport', 'DWC', 'UAE', 'Dubai', 'Dubai', '1', '2017-06-20 08:19:55', '2017-06-20 08:23:07'),
(7, 'Addis Ababa Bole International Airport', 'ADD', 'Ethiopia', 'Addis Ababa', 'Addis Ababa', '1', '2017-06-20 08:20:46', '2017-06-20 08:22:59'),
(8, 'Houari Boumediene Airport', 'ALG', 'Algeria', 'Algiers', 'Dar El Beida', '1', '2017-06-20 08:21:54', '2017-06-20 08:22:53'),
(9, 'Sardar Vallabhbhai Patel International Airport', 'AMD', 'India', 'Gujrat', 'Ahmadabad', '1', '2017-06-20 08:22:31', '2017-06-20 08:22:45'),
(10, 'Chhatrapati Shivaji International Airport ', 'BOM', 'India', 'Maharashtra', 'Mumbai', '1', '2017-06-20 08:24:20', NULL),
(11, 'Brussels Airport', 'BRU', 'Belgium', 'Zaventem', 'Brussels', '1', '2017-06-20 08:25:42', '2017-06-20 08:25:51'),
(12, 'Guangzhou Baiyun International Airport', 'CAN', 'China', 'Guangdong', 'Baiyun', '1', '2017-06-20 08:26:54', NULL),
(13, 'Indira Gandhi International Airport', 'DEL', 'India', 'Delhi', 'New Delhi', '1', '2017-06-20 08:27:34', NULL),
(14, 'King Fahd International Airport', 'DMM', 'Saudi Arabia', 'Dammam', 'Dammam', '1', '2017-06-20 08:28:34', NULL),
(15, 'Eldoret International Airport', 'EDL', 'Kenya', 'Eldoret ', 'Eldoret ', '1', '2017-06-20 08:29:39', NULL),
(16, 'Frankfurt Airport', 'FRA', 'Germany', 'Frankfurt', 'Frankfurt', '1', '2017-06-20 08:30:20', '2017-06-20 08:30:30'),
(17, 'Djibouti–Ambouli International Airport', 'JIB', 'Djibouti', 'Djibouti', 'Ambouli', '1', '2017-06-20 08:32:24', NULL),
(18, 'Khartoum International Airport', 'KRT', 'Sudan', 'Khartoum', 'Khartoum', '1', '2017-06-20 08:33:27', NULL),
(19, 'Lilongwe International Airport', 'LLW', 'Malawi', 'Lumbadzi', 'Lumbadzi', '1', '2017-06-20 08:34:26', NULL),
(20, 'Jomo Kenyatta International Airport', 'NBO', 'Kenya', 'Nairobi', 'Nairobi', '1', '2017-06-20 08:35:16', NULL),
(21, 'Shanghai Pudong International Airport', 'PVG', 'HongKong', 'Shanghai', 'Shanghai', '1', '2017-06-20 08:35:59', NULL),
(22, 'Zaragoza Airport', 'ZAZ', 'Spain', 'Zaragoza', 'Zaragoza', '1', '2017-06-20 08:36:53', NULL),
(23, 'Entebbe International Airport ', 'EBB', 'Uganda', 'Entebbe ', 'Entebbe ', '1', '2017-06-20 08:50:12', NULL),
(24, 'Amsterdam Airport Schiphol', 'AMS', 'Netherlands', 'Amsterdam', 'Amsterdam', '1', '2017-06-20 08:50:46', NULL),
(25, 'King Khalid International Airport', 'RUH', 'Saudi Arabia', 'Riyadh', 'Riyadh', '1', '2017-06-20 08:51:19', NULL),
(26, 'Henri Coandă International Airport', 'OTP', 'Romania', 'Otopeni', 'Bucharest', '1', '2017-07-10 20:02:02', NULL),
(27, 'Kempegowda International Airport', 'BLR', 'India', 'Karnataka', 'Bangalore', '1', '2017-08-01 11:20:56', NULL),
(28, 'Hong Kong Internal Airport', 'HKG', 'Hong Kong', 'Chek Lap Kok', 'Hong Kong', '1', '2017-08-01 17:02:27', NULL),
(29, 'Queen Alia International Airport', 'AMM', 'Jordon', 'Amman', 'Zizya', '1', '2017-09-15 18:52:27', NULL),
(30, 'Dubai International Airport', 'DXB', 'UAE', 'NA', 'Dubai', '1', '2017-09-22 18:58:18', NULL),
(31, 'Sheremetyevo International Airport', 'SVO', 'Russia', 'Moscow', 'Khimki', '1', '2017-10-05 16:18:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `system_emails_log`
--

CREATE TABLE IF NOT EXISTS `system_emails_log` (
`id` int(11) NOT NULL,
  `calledFrom` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `addedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE IF NOT EXISTS `training` (
`id` bigint(20) NOT NULL,
  `category` varchar(255) NOT NULL,
  `training` varchar(255) DEFAULT NULL,
  `duration` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`id`, `category`, `training`, `duration`, `created_at`, `active`) VALUES
(1, 'Atlas', 'Air Condition & Heat Cart', '', '2017-07-10 07:39:50', 1),
(2, 'Atlas', 'Aviation Security Awareness Training ', '', '2017-07-10 07:47:30', 1),
(3, 'Atlas', 'Air Start Unit', '', '2017-07-10 07:47:45', 1),
(4, 'Atlas', 'Aircraft Fueling Training', '', '2017-07-10 07:47:57', 1),
(5, 'Atlas', 'B747 Configurations', '', '2017-07-10 07:48:37', 1),
(6, 'Atlas', 'B747-400SF (Special Freighter) Training', '', '2017-07-10 07:48:50', 1),
(7, 'Atlas', 'B747-8 Introduction', '', '2017-07-10 07:49:08', 1),
(8, 'Atlas', 'B767 Cargo Ramp Handling Training', '', '2017-07-10 07:49:21', 1),
(9, 'Atlas', 'Baggage Tractor', '', '2017-07-10 07:49:33', 1),
(10, 'Atlas', 'Carry on Baggage', '', '2017-07-10 07:49:48', 1),
(11, 'Atlas', 'Communications', '', '2017-07-10 07:50:05', 1),
(12, 'Atlas', 'Complaints Resolution Officals', '', '2017-07-10 07:50:21', 1),
(13, 'Atlas', 'Dangerous Goods 1-6', '', '2017-07-10 07:50:37', 1),
(14, 'Atlas', 'Dangerous Goods 7-8, 10', '', '2017-07-10 07:50:54', 1),
(15, 'Atlas', 'Deice Ground Operations', '', '2017-07-10 07:51:09', 1),
(16, 'Atlas', 'FACAOSSP', '', '2017-07-10 07:51:21', 1),
(17, 'Atlas', 'Exit Row Seating', '', '2017-07-10 07:51:38', 1),
(18, 'Atlas', 'Fire Extingusher Awareness Training', '', '2017-07-10 07:51:54', 1),
(19, 'Atlas', 'Ground Operations Crew Resources Mgmt', '', '2017-07-10 07:52:13', 1),
(20, 'Atlas', 'Ground Operations Alerts', '', '2017-07-10 07:52:40', 1),
(21, 'Atlas', 'Ground Power Unit', '', '2017-07-10 07:52:54', 1),
(22, 'Atlas', 'GT 110 PUSHBACK TRACTOR', '', '2017-07-10 07:53:18', 1),
(23, 'Atlas', 'Human Factors', '', '2017-07-10 07:53:31', 1),
(24, 'Atlas', 'LAV Truck', '', '2017-07-10 07:53:43', 1),
(25, 'Atlas', 'Pipe Training', '', '2017-07-10 07:53:58', 1),
(26, 'Atlas', 'Radio Awareness Training', '', '2017-07-10 07:54:10', 1),
(27, 'Atlas', 'Ramp Handling', '', '2017-07-10 07:54:22', 1),
(28, 'Atlas', 'Regulation Update 2012', '', '2017-07-10 07:54:36', 1),
(29, 'Atlas', 'Regulation Update 2013', '', '2017-07-10 07:54:50', 1),
(30, 'Atlas', 'Sable Update 2012', '', '2017-07-10 07:55:01', 1),
(31, 'Atlas', 'Strap to Pallet Training', '', '2017-07-10 07:55:12', 1),
(32, 'Atlas', 'Tiedown Procedures', '', '2017-07-10 07:55:33', 1),
(33, 'Atlas', 'Towing Procedures', '', '2017-07-10 07:55:45', 1),
(34, 'Atlas', 'Unit Load Devices & Equipment', '', '2017-07-10 07:56:05', 1),
(35, 'Atlas', 'W/B CAT 1', '', '2017-07-10 07:56:17', 1),
(36, 'Atlas', 'W/B CAT 2', '', '2017-07-10 07:56:27', 1),
(37, 'Atlas', 'Warehouse Procedures', '', '2017-07-10 07:56:36', 1),
(38, 'Atlas', '859 Diffrences Training', '', '2017-07-10 07:56:48', 1),
(39, 'TNT', 'W/B', '', '2017-07-10 07:57:22', 1),
(40, 'Emirates', 'W/B', '', '2017-07-10 07:57:35', 1),
(41, 'Emirates', 'Airside Safety Certificate', '', '2017-07-10 07:57:50', 1),
(42, 'Emirates', 'Aviation Security CertifIcate', '', '2017-07-10 07:58:03', 1),
(43, 'Emirates', 'AVSEC Certificate', '', '2017-07-10 07:58:19', 1),
(44, 'Emirates', 'Human Factors Traning for Airports Operation', '', '2017-07-10 07:58:30', 1),
(45, 'Emirates', 'SMS CONTRACTOR', '', '2017-07-10 07:58:47', 1),
(46, 'Emirates', 'Safe ULD Handling', '', '2017-07-10 08:02:01', 1),
(47, 'Emirates', 'Freighter & Specialzied Load Handling', '', '2017-07-10 08:02:15', 1),
(48, 'Emirates', 'B777F Loading Supervision', '', '2017-07-10 08:02:28', 1),
(49, 'Emirates', 'Safety and Emergency Procedures', '', '2017-07-10 08:02:41', 1),
(50, 'Emirates', 'Profciency Review', '', '2017-07-10 08:02:54', 1),
(51, 'LOCAL AIRPORT ', 'Aviation Security Awareness Training ', '', '2017-07-10 08:03:10', 1),
(52, 'Personal', 'Visa Details', '', '2017-07-10 08:03:43', 1),
(53, 'Personal', 'Visa - DAC', '', '2017-07-10 08:03:58', 1),
(54, 'Personal', 'Visa - DXB', '', '2017-07-10 08:04:12', 1),
(55, 'Personal', 'ID Detail', '', '2017-07-10 08:04:27', 1),
(56, 'Personal', 'UAE RES ID', '', '2017-07-10 08:04:48', 1),
(57, 'Personal', 'Passport', '', '2017-07-10 08:05:01', 1),
(58, 'Personal', 'Atlas', '', '2017-07-10 08:05:13', 1),
(59, 'Personal', 'Emirates', '', '2017-07-10 08:05:26', 1),
(60, 'Personal', 'ASL ID', '', '2017-07-10 08:05:38', 1),
(61, 'Personal', 'TNT', '', '2017-07-10 08:05:51', 1),
(62, 'Personal', 'Emirates - Contingent ID', '', '2017-07-10 08:06:31', 1),
(64, 'W&B Cat1', 'Atlas', '730', '2017-08-01 17:17:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `translate`
--

CREATE TABLE IF NOT EXISTS `translate` (
`id` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `phrase` varchar(500) DEFAULT NULL,
  `translation` varchar(500) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `translate`
--

INSERT INTO `translate` (`id`, `lang`, `phrase`, `translation`) VALUES
(8, 'en', 'Ошибка валидации формы', 'Form validation fails'),
(9, 'en', 'Подробнее', 'Read more'),
(10, 'en', 'Назад к перечню публикаций', 'Back to the publications list'),
(11, 'en', 'SITE NAME', 'Yona CMS'),
(12, 'en', 'Главная', 'Home'),
(13, 'en', 'Новости', 'News'),
(14, 'en', 'Контакты', 'Contacts'),
(23, 'en', 'Статьи', 'Articles'),
(25, 'en', 'Home', 'Home'),
(26, 'en', 'News', 'News'),
(27, 'en', 'Articles', 'Articles'),
(28, 'en', 'Contacts', 'Contacts'),
(29, 'en', 'Admin', 'Admin'),
(30, 'en', 'YonaCms Admin Panel', 'YonaCms Admin Panel'),
(31, 'en', 'Back к перечню публикаций', 'Back to publications list'),
(32, 'en', 'Страница №', 'Page num.'),
(49, 'en', 'Полная версия', 'Full version'),
(50, 'en', 'Мобильная версия', 'Mobile version'),
(51, 'en', 'Services', 'Services'),
(52, 'en', 'Printing', 'Printing'),
(53, 'en', 'Design', 'Design'),
(64, 'en', 'Latest news', 'Latest news'),
(67, 'en', 'Entries not found', 'Entries not found'),
(68, 'en', 'Back to publications list', 'Back to publications list');

-- --------------------------------------------------------

--
-- Table structure for table `tree_category`
--

CREATE TABLE IF NOT EXISTS `tree_category` (
`id` int(11) NOT NULL,
  `root` enum('articles','news','category') NOT NULL DEFAULT 'articles',
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `depth` tinyint(2) NOT NULL DEFAULT '0',
  `left_key` int(11) DEFAULT NULL,
  `right_key` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `tree_category`
--

INSERT INTO `tree_category` (`id`, `root`, `parent_id`, `slug`, `depth`, `left_key`, `right_key`, `created_at`, `updated_at`) VALUES
(15, 'articles', NULL, 'computers', 1, 2, 7, '2015-05-19 16:46:38', '2015-11-18 23:35:56'),
(16, 'articles', NULL, 'software', 1, 14, 21, '2015-05-19 16:47:32', '2015-11-18 23:35:57'),
(17, 'articles', NULL, 'gadgets', 1, 8, 13, '2015-05-19 16:47:45', '2015-11-18 23:35:56'),
(18, 'articles', 16, 'microsoft', 2, 17, 18, '2015-05-19 17:23:44', '2015-11-18 23:35:57'),
(19, 'articles', 16, 'oracle', 2, 19, 20, '2015-05-19 17:24:00', '2015-11-18 23:35:57'),
(20, 'articles', 16, 'google', 2, 15, 16, '2015-05-19 17:24:24', '2015-11-18 23:35:57'),
(21, 'articles', 15, 'netbooks', 2, 3, 4, '2015-05-19 17:24:49', '2015-11-18 23:35:56'),
(22, 'articles', 15, 'laptops test', 2, 5, 6, '2015-05-19 17:30:49', '2015-11-18 23:35:56'),
(23, 'articles', 17, 'smartpfone', 2, 9, 10, '2015-05-19 17:32:06', '2015-11-18 23:35:56'),
(24, 'articles', 17, 'tablet', 2, 11, 12, '2015-05-19 17:32:53', '2015-11-18 23:35:57'),
(25, 'news', NULL, 'world', 1, 2, 3, '2015-05-19 17:33:04', '2015-05-20 15:24:45'),
(26, 'news', NULL, 'business', 1, 6, 11, '2015-05-19 17:33:11', '2015-05-20 15:24:45'),
(27, 'news', NULL, 'politics', 1, 4, 5, '2015-05-19 17:33:16', '2015-05-20 15:24:45'),
(28, 'news', 26, 'real-estate', 2, 7, 8, '2015-05-19 17:33:30', '2015-05-20 15:24:45'),
(29, 'news', 26, 'investitions', 2, 9, 10, '2015-05-19 17:33:54', '2015-05-20 15:24:45'),
(30, 'news', NULL, 'life', 1, 12, 17, '2015-05-20 15:24:05', '2015-05-20 15:24:45'),
(31, 'news', 30, 'health', 2, 13, 14, '2015-05-20 15:24:22', '2015-05-20 15:24:45'),
(32, 'news', 30, 'family', 2, 15, 16, '2015-05-20 15:24:42', '2015-05-20 15:24:45'),
(33, 'category', NULL, 'database', 1, 22, 27, '2015-11-17 12:00:18', '2015-11-17 12:03:53'),
(35, 'category', 33, 'mysql', 2, 23, 24, '2015-11-17 12:03:16', '2015-11-17 12:03:53'),
(36, 'category', 33, 'mongodb', 2, 25, 26, '2015-11-17 12:03:43', '2015-11-17 12:03:54'),
(37, 'articles', NULL, 'testing', 1, 22, 23, '2015-11-18 23:35:56', '2015-11-18 23:35:57');

-- --------------------------------------------------------

--
-- Table structure for table `tree_category_translate`
--

CREATE TABLE IF NOT EXISTS `tree_category_translate` (
`id` int(11) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `tree_category_translate`
--

INSERT INTO `tree_category_translate` (`id`, `foreign_id`, `lang`, `key`, `value`) VALUES
(14, 15, 'en', 'title', 'Computers test'),
(15, 16, 'en', 'title', 'Software'),
(16, 17, 'en', 'title', 'Gadgets'),
(17, 18, 'en', 'title', 'Microsoft'),
(18, 19, 'en', 'title', 'Oracle'),
(19, 20, 'en', 'title', 'Google'),
(20, 21, 'en', 'title', 'Netbooks'),
(21, 22, 'en', 'title', 'Laptops'),
(22, 23, 'en', 'title', 'Smartpfone'),
(23, 24, 'en', 'title', 'Tablet'),
(24, 25, 'en', 'title', 'World'),
(25, 26, 'en', 'title', 'Business'),
(26, 27, 'en', 'title', 'Politics'),
(27, 28, 'en', 'title', 'Real estate'),
(28, 29, 'en', 'title', 'Investitions'),
(29, 30, 'en', 'title', 'Life'),
(30, 31, 'en', 'title', 'Health'),
(31, 32, 'en', 'title', 'Family'),
(32, 33, 'en', 'title', 'database'),
(34, 35, 'en', 'title', 'mysql'),
(35, 36, 'en', 'title', 'mongodb'),
(36, 37, 'en', 'title', 'testing');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `role` enum('maker','checker','bank') NOT NULL DEFAULT 'maker',
  `login` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile_no` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `parent_id` varchar(50) DEFAULT '0',
  `request_token` text,
  `state` text,
  `automailer` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role`, `login`, `email`, `name`, `mobile_no`, `password`, `active`, `parent_id`, `request_token`, `state`, `automailer`, `created_at`, `updated_at`) VALUES
(1, 'checker', 'checker', 'checker@yopmail.com', 'Checker', '9990058050', '$2y$10$Py5VV4EaljELjsR2.HIYxee.sbmJICfyUbloRO8zz8bIbL7I4fSgm', 1, '0', NULL, NULL, '1', '2017-05-22 11:00:20', NULL),
(2, 'maker', 'maker', 'maker@yopmail.com', 'Maker', '9999999999', '$2y$10$uHGUMyju5xyvmx.WF8/afepItGFOk3TpZ//4M9jqad0ysxc9DcBta', 1, '1', NULL, NULL, '1', '2017-05-22 11:01:04', NULL),
(3, 'bank', 'yesbank', 'yesbank@yopmail.com', 'Yes Bank', '9999999999', '$2y$10$d9cxmlhYCR4nR4PNdSkl1uIokOMtCzo8312aby7dXOrnbOvnh33sq', 1, '0', NULL, 'Andhra Pradesh,Arunachal Pradesh,Assam,Bihar,Chattisgarh,Goa,Gujarat,Haryana,Himachal Pradesh,Jammu and Kashmir,Jharkhand,Karnataka,Kerala,Madhya Pradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,Odisha,Punjab,Rajasthan,Sikkim,Tamil Nadu,Telangana,Tripura,Uttar Pradesh,Uttarakhand,West Bengal,Delhi', '1', '2017-05-22 11:02:51', '2017-05-22 11:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `widget`
--

CREATE TABLE IF NOT EXISTS `widget` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `html` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `widget`
--

INSERT INTO `widget` (`id`, `title`, `html`) VALUES
('phone', 'Phone in header', '<div class="phone">+1 (001) 555-44-33</div>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_log`
--
ALTER TABLE `access_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `login` (`login`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_configuration`
--
ALTER TABLE `cms_configuration`
 ADD PRIMARY KEY (`key`);

--
-- Indexes for table `cms_javascript`
--
ALTER TABLE `cms_javascript`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flights`
--
ALTER TABLE `flights`
 ADD PRIMARY KEY (`id`), ADD KEY `flights_client_id_index` (`client_id`);

--
-- Indexes for table `flight_assign`
--
ALTER TABLE `flight_assign`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flight_compliance`
--
ALTER TABLE `flight_compliance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flight_expenses`
--
ALTER TABLE `flight_expenses`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flight_legs`
--
ALTER TABLE `flight_legs`
 ADD PRIMARY KEY (`id`), ADD KEY `flight_legs_flight_id_index` (`flight_id`), ADD KEY `flight_legs_station_source_id_index` (`station_source_id`), ADD KEY `flight_legs_station_dest_id_index` (`station_dest_id`);

--
-- Indexes for table `flight_movement`
--
ALTER TABLE `flight_movement`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `iso` (`iso`);

--
-- Indexes for table `load_masters`
--
ALTER TABLE `load_masters`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `load_master_training`
--
ALTER TABLE `load_master_training`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug` (`work_title`), ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `menu_translate`
--
ALTER TABLE `menu_translate`
 ADD PRIMARY KEY (`id`), ADD KEY `foreign_id` (`foreign_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `page_translate`
--
ALTER TABLE `page_translate`
 ADD PRIMARY KEY (`id`), ADD KEY `foreign_id` (`foreign_id`), ADD KEY `lang` (`lang`);

--
-- Indexes for table `pg`
--
ALTER TABLE `pg`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
 ADD PRIMARY KEY (`id`), ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `publication_translate`
--
ALTER TABLE `publication_translate`
 ADD PRIMARY KEY (`id`), ADD KEY `foreign_id` (`foreign_id`), ADD KEY `lang` (`lang`);

--
-- Indexes for table `publication_type`
--
ALTER TABLE `publication_type`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `publication_type_translate`
--
ALTER TABLE `publication_type_translate`
 ADD PRIMARY KEY (`id`), ADD KEY `foreign_id` (`foreign_id`), ADD KEY `lang` (`lang`);

--
-- Indexes for table `seo_manager`
--
ALTER TABLE `seo_manager`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `url` (`url`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stations`
--
ALTER TABLE `stations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_emails_log`
--
ALTER TABLE `system_emails_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translate`
--
ALTER TABLE `translate`
 ADD PRIMARY KEY (`id`), ADD KEY `lang` (`lang`);

--
-- Indexes for table `tree_category`
--
ALTER TABLE `tree_category`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug` (`slug`), ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `tree_category_translate`
--
ALTER TABLE `tree_category_translate`
 ADD PRIMARY KEY (`id`), ADD KEY `foreign_id` (`foreign_id`), ADD KEY `lang` (`lang`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `login` (`login`), ADD KEY `email` (`email`), ADD KEY `parent_id` (`parent_id`), ADD KEY `role` (`role`), ADD KEY `active` (`active`);

--
-- Indexes for table `widget`
--
ALTER TABLE `widget`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_log`
--
ALTER TABLE `access_log`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `flights`
--
ALTER TABLE `flights`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `flight_assign`
--
ALTER TABLE `flight_assign`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `flight_compliance`
--
ALTER TABLE `flight_compliance`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `flight_expenses`
--
ALTER TABLE `flight_expenses`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `flight_legs`
--
ALTER TABLE `flight_legs`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `flight_movement`
--
ALTER TABLE `flight_movement`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `load_masters`
--
ALTER TABLE `load_masters`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `load_master_training`
--
ALTER TABLE `load_master_training`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu_translate`
--
ALTER TABLE `menu_translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `page_translate`
--
ALTER TABLE `page_translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `pg`
--
ALTER TABLE `pg`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `publication_translate`
--
ALTER TABLE `publication_translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `publication_type`
--
ALTER TABLE `publication_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `publication_type_translate`
--
ALTER TABLE `publication_type_translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `seo_manager`
--
ALTER TABLE `seo_manager`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `stations`
--
ALTER TABLE `stations`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `system_emails_log`
--
ALTER TABLE `system_emails_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `translate`
--
ALTER TABLE `translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `tree_category`
--
ALTER TABLE `tree_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tree_category_translate`
--
ALTER TABLE `tree_category_translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `flights`
--
ALTER TABLE `flights`
ADD CONSTRAINT `flights_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Constraints for table `flight_legs`
--
ALTER TABLE `flight_legs`
ADD CONSTRAINT `flight_legs_station_dest_id_foreign` FOREIGN KEY (`station_dest_id`) REFERENCES `stations` (`id`),
ADD CONSTRAINT `flight_legs_station_source_id_foreign` FOREIGN KEY (`station_source_id`) REFERENCES `stations` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
