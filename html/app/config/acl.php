<?php

return [

    // frontend
    'guest'      => [
        'admin/index'       => '*',
        'index/index'       => '*',
        'index/error'       => '*',
        'page/index'        => '*',
        'publication/index' => '*',
        'sitemap/index'     => '*',
        'user/index'        => '*',
        'audit/reminder'    => ['index','escalationreminder']
       ],
    'member'     => [
        'admin/index'       => '*',
        'index/index' => '*',
     ],
    
    'maker' => [
                'admin/index'=> '*',
                'user/user' => '*', 
		'user/index' => '*',
                'audit/user'    =>['index','add','edit','delete','list','save','view','approve','display','upload','quantity','imageupload','delimage','quality']
                
	],
	
	'checker' => [
            'admin/index'       => '*',
                'user/user' => '*', 
		'user/index' => '*',
                'audit/user' =>['index','edit','view','addcomment','confirmapprove','quantity','check','downloadQuantity','imageupload','delimage','downloadword','quality']
		
	],
             

    'loadmaster'     => [
        'admin/admin-user'   => '*',
        'flight/admin'       => '*', 
        'audit/admin'        =>  '*',
        'admin/index'        => '*',
        'load-master/admin'  => ['expenses','flightexpenses','expdelete'],
    ],
    'admin'      => [
        'admin/admin-user'   => '*',
        'user/user'          => '*',
        'cms/configuration'  => '*',
        'cms/translate'      => '*',
        'cms/language'       => '*',
        'cms/javascript'     => '*',
        'widget/admin'       => '*',
        'file-manager/index' => '*',
        'page/admin'         => '*',
        'publication/admin'  => '*',
        'publication/type'   => '*',
        'seo/robots'         => '*',
        'seo/sitemap'        => '*',
        'seo/manager'        => '*',
        'tree/admin'         => '*',
        'station/admin'      => '*',
        'client/admin'       => '*',
        'flight/admin'       => '*', 
        'audit/admin'       =>  '*',
        'load-master/admin'  => '*',
        'admin/index'       =>  '*',
        'training/admin'    =>  '*',      
    ],
];