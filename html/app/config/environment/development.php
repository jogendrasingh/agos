<?php

return [
    'base_path' => '/',
  
    'database'  => [
        'adapter'  => 'Mysql',
        'host'     => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname'   => 'agos',
        'charset'  => 'utf8',
    ],

    'memcache'  => [
        'host' => 'localhost',
        'port' => 11211,
    ],
    
    'cache'     => 'file',
    'mail'  => [
        'driver'     => 'smtp',
        'host'     => 'mail.formzero.in',
        'port'     => '25',
        'username'     => 'demo@formzero.co.in',
        'password'     => 'Reset321$$',
        'from'       => [
            'email' => 'arya@formzero.co.in',
            'name'  => 'Arya Warehouse (YBL)'
        ]
    ],
    
    'fileType' => [
        'image' => ['jpeg' , 'jpg' ,'png'],
        'document' => ['png','pdf' , 'rtf' , 'doc' , 'docx'],
        'img+doc' =>['jpeg' , 'jpg' , 'png','pdf' , 'rtf' , 'doc' , 'docx'],
    ],

    'fileSize' => [
        'upload' => '200000',
    ],

];