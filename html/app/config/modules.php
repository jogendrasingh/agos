<?php

/**
 * Modules "Application" and "Cms" loads automatically
 * */

return array(
    'Image',
    'Index',
    'Admin',
    'Widget',
    'FileManager',
    'Page',
    'Publication',
    'Seo',
    'Menu',
    'Tree',
    'Sitemap',
    'User',
    'Station',
    'Client',
    'Flight',
    'Audit',
    'LoadMaster',
    'Training'
);