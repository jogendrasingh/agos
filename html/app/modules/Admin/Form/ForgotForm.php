<?php

/**
 * LoginForm
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace Admin\Form;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
//use Phalcon\Validation\Validator\PresenceOf;

class ForgotForm extends \Phalcon\Forms\Form
{

    public function initialize()
    {
        $login = new Text('login', array(
            'required' => true,
            'placeholder' => 'Enter login',
        ));
       // $login->addValidator(new PresenceOf(array('message' => 'Login is required')));
        $this->add($login);

       $this->add(new Text("request_token"));
       $this->add(new Text("password"));
    }

}
