<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login</title>

        <link href="{{ url.path() }}vendor/semantic-2.1/semantic.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url.path() }}vendor/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="{{ url.path() }}vendor/js/html5shiv.js"></script>
            <script src="{{ url.path() }}vendor/js/respond.min.js"></script>
        <![endif]-->
        <style>
            .container {
                width: 400px;
                margin: 100px auto 0;
            }
            .error{
                    color:red;
                    margin:0 0 0 1px;
                }
        </style>
    </head>
    <body>
        <!--login modal-->
        <div class="container text-center">
            {{ flash.output() }}
            <?php if(!empty($_GET['rt'])){ $param="?rt=".$_GET['rt']; }else{$param="";}?>
            <form  class="ui form segment" name="reserve" AUTOCOMPLETE = "off" id="reserve" method="post" onsubmit="return ValidateForm();" action="{{ url.get() }}admin/index/forgot<?=$param?>">                           
                <h1 align="center">AGOS</h1>
                <div>{{ flash.output() }}</div>
                <div class="errorClass error"></div>
                <?php if(!empty($_GET['rt'])){?>
                <div class="form-group field-btm">
                    <input type="password" class="form-control my-form-control input required inputData" name="password" id="password" value="" placeholder="Enter your Password"><span  id="error_username" ></span> 
                    <input type='hidden' name='login' value='<?=$_GET["login"]?>'>
                </div><br/>
                <div class="form-group field-btm">
                    <input type="password" class="form-control my-form-control input required inputData" name="confirmPassword" id="cnfPassword" value="" placeholder="Confirm your login"><span  id="error_username" ></span> 
                </div>
                <?php }else{?>
                <div class="form-group field-btm">
                    <input type="text" class="form-control my-form-control input required inputData" name="login" id="login" value="" placeholder="Enter your login"><span  id="error_username" ></span> 
                </div>
                <?php }?>
                    <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}"/>  
                <div class="col-md-12 col-md-offset-1"><br/>
                    <button class="ui blue submit button" type="submit" value="Submit" name="submit" style="margin-left:38%;" id="submit" >Submit</button>
                </div>
                <div class="col-md-12" style="margin-left:44%;"><br/>
                    <a href="{{ url.get()}}admin/index/login"><p>login?</p></a>
                </div>	
            </form>
        </div>

        <!-- script references -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>

<?php if(!empty($_GET['rt'])){?>
                        <script>
                            function ValidateForm(){
                                var password = $("#password").val();
                                var cnfPassword = $("#cnfPassword").val();
                                if(password==''){
                                   //alert("Password can't be empty");
                                   $(".errorClass").html("Password can't be empty");
                                    return false; 
                                }else if(cnfPassword==''){
                                    //alert("Confirm Password can't be empty");
                                    $(".errorClass").html("Confirm Password can't be empty");                        
                                    return false; 
                                }else if(password!==cnfPassword){
                                    //alert("Password not Match");
                                    $(".errorClass").html("Password not Match");
                                    return false;
                                }else{
                                    return true;
                                }
                            }
                        </script>
<?php } ?>



