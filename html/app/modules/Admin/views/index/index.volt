<link href='{{url.path()}}static/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='{{url.path()}}static/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script>
	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			defaultDate: new Date(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			ignoreTimezone: false,
			eventMouseover: function (data, event, view) {
				start = $.fullCalendar.formatDate(data.start, "YYYY-MM-DD HH:mm:ss");
				//alert(data.start);
	            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#feb811;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'title: ' + ': ' + data.title + '</br>' + 'start: ' + start + '</div>';


	            $("body").append(tooltip);
	            $(this).mouseover(function (e) {
	                $(this).css('z-index', 10000);
	                $('.tooltiptopicevent').fadeIn('500');
	                $('.tooltiptopicevent').fadeTo('10', 1.9);
	            }).mousemove(function (e) {
	                $('.tooltiptopicevent').css('top', e.pageY + 10);
	                $('.tooltiptopicevent').css('left', e.pageX + 20);
	            });


	        },
	        eventMouseout: function (data, event, view) {
	            $(this).css('z-index', 8);

	            $('.tooltiptopicevent').remove();

	        },
	        dayClick: function () {
	            tooltip.hide()
	        },
	        eventResizeStart: function () {
	            tooltip.hide()
	        },
	        eventDragStart: function () {
	            tooltip.hide()
	        },
	        viewDisplay: function () {
	            tooltip.hide()
	        },
			events: "{{ url.get() }}flight/admin/calender" 
			
		});
		
	});

</script>

	<div id='calendar'></div>
<!-- {{ javascript_include("static/js/fullcalendar/lib/jquery.min.js")}} -->
{{ javascript_include("static/js/fullcalendar/lib/moment.min.js")}}

{{ javascript_include("static/js/fullcalendar/fullcalendar.min.js")}}

