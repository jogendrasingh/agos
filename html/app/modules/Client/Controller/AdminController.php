<?php
namespace Client\Controller;
use YonaCMS\Plugin\Mail;
use Application\Mvc\Controller;
use Client\Model\Client;
use Client\Form\ClientForm;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AdminController extends Controller {

    public function initialize() {
        $this->setAdminEnvironment();
        $this->helper->activeMenu()->setActive('client');

    }

    public function indexAction() {

        $this->view->entries = Client::find([
            "order" => "id DESC"
        ]);
        
        $this->helper->title($this->helper->at('Manage Client'), true);
    }

    public function addAction() {

        $this->view->pick(['admin/edit']);
        $model = new Client();
        $form = new ClientForm();
        
        if ($this->request->isPost()) {
            $model = new Client();
            $post = $this->request->getPost();
        
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $this->flash->success($this->helper->at('Client created', ['name' => $model->name]));
                    $this->redirect($this->url->get() . 'client/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->submitButton = $this->helper->at('Add New');

        $this->helper->title($this->helper->at('Manage Client'), true);
    
    }

    public function editAction($id) {
        $model = Client::findFirst($id);
        if (!$model) {
            $this->redirect($this->url->get() . 'client/admin');
        }

        $form = new ClientForm();

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save() == true) {
                    $this->flash->success('Client <b>' . $model->name . '</b> has been saved');
                    return $this->redirect($this->url->get() . 'client/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        } else {
            $form->setEntity($model);
        }

        $this->view->submitButton = $this->helper->at('Save');
        $this->view->form = $form;
        $this->view->model = $model;

        $this->helper->title($this->helper->at('Manage Clients'), true);
    }

    
    public function deleteAction($id) {
        $model = Client::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'client/admin');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('Deleting client <b>' . $model->name . '</b>');
            return $this->redirect($this->url->get() . 'client/admin');
        }

        $this->view->model = $model;

        $this->helper->title($this->helper->at('Delete Client'), true);
    }

}
