<?php

namespace Client\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;

class ClientForm extends Form
{
    public function initialize()
    {
     
        $this->add(
            (new Text('name', [
                'required' => true,
            ]))->setLabel('Client Name')
        );

        $this->add(
            (new Text('client_code', [
                'required' => true,
            ]))->setLabel('Client Code')
        );

        $this->add(
            (new Check('active'))
                ->setLabel('Active')
        );     

    }

}   