<?php

/**
 * Routes
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace Client;

use Application\Mvc\Router\DefaultRouter;
//use Category\Model\Category;

class Routes
{

     public function init($router)
    {    
    $router->add('/client', array(
            'module'     => 'client',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('client');
        
        $router->add('/client/', array(
            'module'     => 'client',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('client');
        
        $router->add('/client/admin/', array(
            'module'     => 'client',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('client');
        
        
        return $router;
    }     

}