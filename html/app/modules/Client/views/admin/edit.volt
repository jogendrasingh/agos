<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}client/admin" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
        {% if(not(model.id is empty)) %}
            <a href="{{ url.get() }}client/admin/delete/{{ model.id }}" class="ui button red">
                <i class="icon trash"></i> Delete
            </a>
        {% endif %}
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <div class="two fields">
            <div class="field">
                {{ form.renderDecorated('name') }}
                {{ form.renderDecorated('client_code') }}
                {{ form.renderDecorated('active') }}
            </div>
            <div class="field">
            </div>
        </div>
    </div>

</form>

<script>
    $('.ui.form').form({
           fields: {
            name: {
                identifier: 'name',
                rules: [
                    {type: 'empty'}
                ]
            },
            client_code: {
                identifier: 'client_code',
                rules: [
                    {type: 'empty'}
                ]
            },
    }
 });
          
</script>