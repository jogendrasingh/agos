<?php
namespace Flight\Controller;
use YonaCMS\Plugin\Mail;
use Application\Mvc\Controller;
use Flight\Model\Flight;
use Flight\Form\FlightForm;
use Station\Model\Station;
use Flight\Model\FlightLeg;
use Flight\Form\FlightLegForm;
use Flight\Model\FlightAssign;
use Flight\Form\FlightAssignForm;
use LoadMaster\Model\LoadMaster;
use Flight\Model\FlightMovement;
use Flight\Form\FlightMovementForm;
use Client\Model\Client; 


use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AdminController extends Controller {

    public function initialize() {
        $this->setAdminEnvironment();
        $this->helper->activeMenu()->setActive('flight');

    }

    public function indexAction() {


       $flight = Flight::find(['status'=>'1','columns'=>['id','support_type','client_id','active']]);
         
        $fresult = [];
        if(count($flight)>0){
        
            foreach ($flight as $key => $res) {
                try{
                
                $flightlegArr = FlightLeg::query()
                ->where("flight_id='".$res['id']."'")
                ->execute()->toArray();
                }catch(\Exception $e){
                }
                
                if(count($flightlegArr)>0){
                    $j=0;
                    $finalcommercial = "";
                    $fligtNo ="";
                    $route=[];
                    $fresult[$key]['date'] = "";
                    foreach($flightlegArr as $k=>$val){
                        $commercial = "";
                         
                        if($j=="0"){
                            if($res['support_type']=='local'){
                                $fresult[$key]['date'] = $val['eta'];
                            }else{
                                $fresult[$key]['date'] = $val['etd'];
                            }    
                        }

                        /* ROUTING*/

                        $route[$k]['station'] = $this->stationName($val['station_source_id']);
                        $route[$k]['std'] = date('d/m/Y H:i',strtotime($val['std']));
                        
                        /* END ROUTING*/



                        /* COMMERCIAL STEP */
                        if($val['commercial']=='1'){
                            $commercial = "Yes";
                            if($val['reimbursable']=="1"){
                                $commercial .="->Yes";
                                if(!empty($val['reimbursableClient']))
                                    $commercial .="->".Client::getName($val['reimbursableClient']);
                            }else{
                                $commercial .="->No";
                            }    

                        }else{
                        $commercial = 'No';    
                        }
                        $commercial .=", ";  
                        $finalcommercial .=$commercial;
                        /* END COMMERCIAL STEP */
                        
                        /* FLIGHT NO*/
                        $fligtNo .= $fligtNo == ""?$val['flight_no']:" ->".$val['flight_no'];
                        /* END FLIGHT No*/

                    $j++;                         
                    }
                        $fresult[$key]['id'] =          $res['id'];
                        $fresult[$key]['client_name'] = Client::getName($res['client_id']);
                        $fresult[$key]['support_type']= $res['support_type'];
                        $fresult[$key]['commercial']  = $finalcommercial;
                        $fresult[$key]['flight_no']   = $fligtNo;
                        $fresult[$key]['routing']     = $route;
                        $fresult[$key]['active']      = $res['active'];
                

                }
                
            }
              
             

            $this->view->entries = $fresult;

        }
        $this->helper->title($this->helper->at('Manage Flight'), true);
    }

    public function addAction() {

        $this->view->pick(['admin/edit']);
        $model = new Flight();
        $form = new FlightForm();

        $stationArr = Station::find(["columns" => "id, airport_name, station_code","active"=>'1',"orderby"=>"airport_name"]);

        $this->view->station = $stationArr;
        $this->view->month = $this->config->month;

        $reimbursClient = Client::getClient();
        $this->view->reimbursClient = $reimbursClient;

        $editModel = new FlightLeg();

        $this->view->legmodel = ['0'=>$editModel->toArray(),'1'=>$editModel->toArray(),'2'=>$editModel->toArray(),'3'=>$editModel->toArray(),'4'=>$editModel->toArray(),'5'=>$editModel->toArray()];

        if ($this->request->isPost()) {
            $model = new Flight();
            $post = $this->request->getPost();

            //echo "<pre>";
            //print_r($post);
            //exit;

            $flightpost['client_id'] = $post['client_id'];
            //$flightpost['aircraft_registration_no'] = $post['aircraft_registration_no'];
            $flightpost['support_type'] = $post['support_type'];
            $form->bind($flightpost, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $flight_id = $model->id;
                    if(isset($post['flight_no']) && count($post['flight_no'])>0){
                        $leg = array();
                        $modelForm = new FlightLegForm();
                        

                        foreach($post['flight_no'] as $k=>$data){
                            $leg['flight_id'] = $flight_id;
                            $leg['flight_no'] = $post['flight_no'][$k];
                            $leg['station_source_id'] = $post['station_source_id'][$k];
                            
                            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['std'][$k]);
                            $leg['std'] = $d->format('Y-m-d H:i:s');
                            $da= \DateTime::createFromFormat('m/d/Y H:i', $post['etd'][$k]);
                            $leg['etd'] = $da->format('Y-m-d H:i:s');
                            $leg['trained'] = $post['trained'][$k];
                            $leg['station_dest_id'] = $post['station_dest_id'][$k];
                            
                            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['sta'][$k]);
                            $leg['sta'] = $d->format('Y-m-d H:i:s');
                            $ds = \DateTime::createFromFormat('m/d/Y H:i', $post['eta'][$k]);
                            $leg['eta'] = $d->format('Y-m-d H:i:s');
                            //$leg['sta'] = date_format(new \Datetime($post['sta'][$k]), 'Y-m-d h:i:s');
                            $leg['active'] ='1';

                            $leg['commercial'] = $post['commercial'][$k];
                            if($post['commercial'][$k]=="1"){
                                $leg['reimbursable'] = $post['reimbursable'][$k];
                                $leg['reimbursableClient'] = $post['reimbursableClient'][$k];    
                            }

                                                     
                            $modelleg = new FlightLeg();
                            $modelForm->bind($leg, $modelleg);
                            if ($modelForm->isValid()) {
                                $modelleg->save();
                            }
                        }
                    }

                    $this->flash->success($this->helper->at('Flight created', ['name' => $modelodel->name]));
                    $this->redirect($this->url->get() . 'flight/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->submitButton = $this->helper->at('Add New');

        $this->helper->title($this->helper->at('Manage Flight'), true);
    
    }

    public function editAction($id) {
        $model = Flight::findFirst($id);
        if (!$model) {
            $this->redirect($this->url->get() . 'flight/admin');
        }

        $this->view->pick(['admin/edit_old']);
        $form = new FlightForm();
        
        $stationArr = Station::find(["columns" => "id, station_code","active"=>'1',"orderby"=>"airport_name"]);
        $this->view->station = $stationArr;

        
        if($model->id){
            $legModel = FlightLeg::query()->columns(array('id','flight_no',"date_format(std,'%m/%d/%Y %k:%i') as std","date_format(etd,'%m/%d/%Y %k:%i') as etd",'station_source_id','station_dest_id','trained',"date_format(sta,'%m/%d/%Y %k:%i') as sta","date_format(eta,'%m/%d/%Y %k:%i') as eta","commercial","reimbursable","reimbursableClient"))->where("flight_id='".$model->id."'")->execute()->toArray();

            $totalleg = count($legModel);
            $editModel = new FlightLeg();
            for($i=$totalleg;$i<=6;$i++){
                $legModel[$i] = $editModel->toArray();
            }
            
            //echo "<pre>";
            //print_r($legModel);
            //exit;
            // echo "<pre>";
            // print_r($legModel);
            // print_R($stationArr->toArray());
            // exit;
            $reimbursClient = Client::getClient();
            $this->view->reimbursClient = $reimbursClient;
            
            $this->view->legmodel = $legModel;
            $this->view->totalleg =$totalleg; 
        }

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $flightpost['client_id'] = $post['client_id'];
            $flightpost['aircraft_registration_no'] = $post['aircraft_registration_no'];
            $form->bind($flightpost, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save() == true) {
                    if(isset($post['flight_no']) && count($post['flight_no'])>0){
                        $leg = array();
                        $modelForm = new FlightLegForm();
                        foreach($post['flight_no'] as $k=>$data){
                            if(!empty($data)){
                            $leg['flight_id'] = $model->id;
                            $leg['flight_no'] = $post['flight_no'][$k];
                            $leg['station_source_id'] = $post['station_source_id'][$k];
                            
                            $d1 = \DateTime::createFromFormat('d/m/Y H:i',$post['std'][$k]);
                            $leg['std'] = $d1->format('Y-m-d H:i:s');

                            $d2 = \DateTime::createFromFormat('d/m/Y H:i',$post['etd'][$k]);
                            $leg['etd'] = $d2->format('Y-m-d H:i:s');

                            $leg['trained'] = $post['trained'][$k];
                            $leg['station_dest_id'] = $post['station_dest_id'][$k];
                            
                            $d = \DateTime::createFromFormat('d/m/Y H:i', $post['sta'][$k]);
                            $leg['sta'] = $d->format('Y-m-d H:i:s');

                            $ds = \DateTime::createFromFormat('d/m/Y H:i', $post['eta'][$k]);
                            $leg['eta'] = $ds->format('Y-m-d H:i:s');

                            $leg['active'] ='1';

                            $leg['commercial'] = $post['commercial'][$k];
                            if($post['commercial'][$k]=="1"){
                                $leg['reimbursable'] = $post['reimbursable'][$k];
                                $leg['reimbursableClient'] = $post['reimbursableClient'][$k];    
                            }else{
                                $leg['reimbursable'] = "";
                                $leg['reimbursableClient'] = "";
                            }
                            if($post['id'][$k]){
                            $modelleg = FlightLeg::findFirst($post['id'][$k]);
                            }else{
                            $modelleg = new FlightLeg();
                            }
                          
                            $modelForm->bind($leg, $modelleg);
                            if ($modelForm->isValid()) {
                                $modelleg->save();
                            }

                            }
                            
                        }
                        
    
                    }
                    $this->flash->success('Flight <b>' . $model->name . '</b> has been saved');
                    return $this->redirect($this->url->get() . 'flight/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        } else {
            $form->setEntity($model);
        }

        $this->view->submitButton = $this->helper->at('Save');
        $this->view->form = $form;
        $this->view->model = $model;

        $this->helper->title($this->helper->at('Manage Flights'), true);
    }

    
    public function deleteAction($id) {
        $model = Flight::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'flight/admin');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('Deleting Flight <b>' . $model->name . '</b>');
            return $this->redirect($this->url->get() . 'flight/admin');
        }

        $this->view->model = $model;

        $this->helper->title($this->helper->at('Delete Flight'), true);
    }


    public function flightAction($id){
        if($id){
             $flightleg = FlightLeg::query()->columns(array('Flight\Model\FlightLeg.id','Flight\Model\FlightLeg.sta','SD.station_code','Flight\Model\FlightLeg.std','FA.support','FA.visa','LM.first_name','FA.id as assign_id'))
            ->leftJoin("Station\Model\Station","Flight\Model\FlightLeg.station_dest_id=SD.id","SD")    
            ->leftJoin("Flight\Model\FlightAssign","FA.leg_id=Flight\Model\FlightLeg.id","FA")
            ->leftJoin('LoadMaster\Model\LoadMaster','FA.load_master_id=LM.id', 'LM')

            ->where("Flight\Model\FlightLeg.flight_id='".$id."'")
            ->orderBy('Flight\Model\FlightLeg.id asc')->execute()->toArray();
            
            

            //echo "<pre>";
            //print_r($flightleg);
            //exit;
            $finalflight = [];
            if(count($flightleg)>0){
                foreach($flightleg as $k=>$val){
                  if(isset($flightleg[$k+1]['std'])){
                    $flightleg[$k]['std'] =  $flightleg[$k+1]['std'];  
                  }else{
                    $flightleg[$k]['std'] = "";
                  }
                }
            }
            
            $this->view->flightleg = $flightleg;

            $loadmaster = LoadMaster::query()->columns(array('id','first_name'))->where("active='1'")->orderBy("first_name")->execute();
            $this->view->loadmaster = $loadmaster;
   
       // $this->view->form = $form;
        //$this->view->model = $model;
        //$this->view->assignflight = $assignFlight; 
}
        $this->helper->title($this->helper->at('Assign Flights'), true); 




    }


    public function assignflightAction(){
        if ($this->request->isPost()) {
            $model = new FlightAssign();
            $form  = new FlightAssignForm();
            $post = $this->request->getPost();
            //$post['flight_id'] = $id; 
            $form->bind($post, $model);
            if ($form->isValid()) {
                if ($model->save()) {
                    $assign_id = $model->id;
                    $support = $post['support'];
                    $visa = $post['visa'];
                    $load_master = LoadMaster::query()->columns('first_name')->where("id = '".$post['load_master_id']."'")->execute()->toArray();
                    echo json_encode(array('value'=>'success','res'=>array('support'=>$support,'visa'=>$visa,'load_master'=>$load_master[0]['first_name'],'assign_id'=>$assign_id)));   
                } else {
                echo json_encode(array('value' => 'error'));  
                }
                }
            }
            exit;
    }

    public function movementAction($id){
        if($id){
            //$movementmodel = new FlightMovement();
            $movementform = new FlightMovementForm();

            $aF = FlightAssign::query()->columns(array('FL.flight_id'))->leftJoin("Flight\Model\FlightLeg","FL.id=Flight\Model\FlightAssign.leg_id","FL")->execute()->toArray();
            $this->view->flightId = $aF[0]['flight_id'];
            
            $fm = FlightMovement::query()->columns('id')->where("assign_id='".$id."'")->execute()->toArray();
            if(count($fm)>0){
                $movementmodel = FlightMovement::findFirst($fm[0]['id']);
            }else{
                $movementmodel = new FlightMovement();
                
            }
            //exit;
            if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $post['assign_id'] = $id;
            
            $post['date'] = date_format(new \Datetime($post['date']), 'Y-m-d');
            
            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['source_etd']);
            $post['source_etd'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['push_back_time']);
            $post['push_back_time'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['airborne_time']);
            $post['airborne_time'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['destination_eta']);
            $post['destination_eta'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['touch_down_time']);
            $post['touch_down_time'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['chocks_on']);
            $post['chocks_on'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['uws_received']);
            $post['uws_received'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['lir_issued']);
            $post['lir_issued'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['loading_start']);
            $post['loading_start'] = $d->format('Y-m-d H:i:s');

            $d = \DateTime::createFromFormat('m/d/Y H:i', $post['loading_finish']);
            $post['loading_finish'] = $d->format('Y-m-d H:i:s');

            if(count($fm)>0){
                $movementmodel = FlightMovement::findFirst($fm[0]['id']);
            }else{
                $movementmodel = new FlightMovement();
            }
            $movementform->bind($post, $movementmodel);
            if ($movementform->isValid()) {
                if ($movementmodel->save() == true) {

                    }
                }
            }else {
            $movementform->setEntity($movementmodel);
        }
            $this->view->form = $movementform;
             $this->view->model = $movementmodel;
            $this->view->submitButton = $this->helper->at('Add');
            $this->helper->title($this->helper->at('Add/Edit Movement'), true);
        }
    }

    public function reportAction($id){
        if($id){


            $result = FlightMovement::query()->columns(array('Flight.id as flight_id','FL.id as leg_id','ST.station_code','FL.flight_no','Flight.aircraft_registration_no','FL.sta','LM.first_name','Flight\Model\FlightMovement.date','Flight\Model\FlightMovement.delay_reason','Flight\Model\FlightMovement.uws_received','Flight\Model\FlightMovement.lir_issued','Flight\Model\FlightMovement.loading_start', 'Flight\Model\FlightMovement.loading_finish','Flight\Model\FlightMovement.offload','Flight\Model\FlightMovement.transit','joining'))
            ->leftJoin("Flight\Model\FlightAssign","FA.id=Flight\Model\FlightMovement.assign_id","FA")
            ->leftJoin("Flight\Model\FlightLeg","FL.id=FA.leg_id","FL")
            ->leftJoin("Flight\Model\Flight","Flight.id=FL.flight_id","Flight")
            ->leftJoin("Station\Model\Station","ST.id=FL.station_dest_id","ST")
            ->leftJoin("LoadMaster\Model\LoadMaster","LM.id = FA.load_master_id","LM")
            ->execute()->toArray();

            $report = Array();
            if(count($result)>0){
                foreach ($result as $req) {
                    $report = $req;
                }

                if($report['flight_id']){
                    $report['routing'] = $this->getFlightRounting($report['flight_id']);
                }

            }

            /*echo "<pre>";
            print_r($report);
            exit;*/

            $this->view->report =$report; 
        }else{
            $this->redirect($this->url->get() . 'flight/admin');

        }


        $this->helper->title($this->helper->at('Report'), true);


    }

    private function getFlightRounting($id){
        $flightleg = FlightLeg::query()->columns(array('station_source_id','station_dest_id'))
        ->where("flight_id='".$id."'")->execute()->toArray();
        //print_r()
        $i=0; $station='';
        foreach($flightleg as $flg){
            if($i==0){
            $station .= $this->stationName($flg['station_source_id']);
            $station .= "-".$this->stationName($flg['station_dest_id']);    
        }else{
            $station .= "-".$this->stationName($flg['station_dest_id']);
        }
            $i++;
        }
        return $station; 
    }

    private function stationName($id){
        $fname = Station::query()->columns('station_code')->where("id='".$id."'")->execute()->toArray();
        return $fname[0]['station_code'];
    }



    

/*    public function listAction(){
        
        $listAssign = FlightAssign::query()->columns(array('Flight\Model\FlightAssign.id','LM.first_name','F.aircraft_registration_no','FL.flight_no'))
            ->leftJoin('LoadMaster\Model\LoadMaster','Flight\Model\FlightAssign.load_master_id=LM.id', 'LM')
            ->leftJoin('Flight\Model\Flight', 'Flight\Model\FlightAssign.flight_id=F.id','F')
            ->leftJoin('Flight\Model\FlightLeg','Flight\Model\FlightAssign.leg_id=FL.id','FL')
            ->where("LM.active='1'")
            ->where("F.active='1'")
            ->orderBy("Flight\Model\FlightAssign.id desc")
            ->execute();
        $this->view->listAssign = $listAssign;
        $this->helper->title($this->helper->at('Manage Movement'), true);        
    }*/



    public function calenderAction(){
            $auth = $this->session->get('auth');
            $condition ="1=1";
            if($auth->role=='loadmaster'){
            $condition ="LM.admin_id='".$auth->id."'";
            }
             
             $flightleg = FlightLeg::query()->columns(array('Flight\Model\FlightLeg.id','Flight\Model\FlightLeg.sta','SD.station_code','Flight\Model\FlightLeg.std','LM.first_name','Flight\Model\FlightLeg.flight_no'))
            ->leftJoin("Station\Model\Station","Flight\Model\FlightLeg.station_dest_id=SD.id","SD")    
            ->leftJoin("Flight\Model\FlightAssign","FA.leg_id=Flight\Model\FlightLeg.id","FA")
            ->leftJoin('LoadMaster\Model\LoadMaster','FA.load_master_id=LM.id', 'LM')
            ->where($condition)
            ->orderBy('Flight\Model\FlightLeg.id asc')->execute()->toArray();
            
           
            $finalflight = [];
            if(count($flightleg)>0){
                foreach($flightleg as $k=>$val){
                  $finalflight[$k]['title'] = $flightleg[$k]['flight_no']." - ".$flightleg[$k]['first_name'];
                  $finalflight[$k]['start'] = $flightleg[$k]['sta'];
                  //$finalflight[$k]['allDay'] = false;
                  if(isset($flightleg[$k+1]['std'])){
                    //$finalflight[$k]['end'] =  $flightleg[$k+1]['std'];  
                  }else{
                    //$finalflight[$k]['end'] = "";
                  }
                }

            }
            echo json_encode($finalflight);
            exit;
            //echo "<pre>";
            //print_r($finalflight);
            //exit;
            
           // $this->view->flightleg = $flightleg;

           
    }
}
