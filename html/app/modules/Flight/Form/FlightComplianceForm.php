<?php

namespace Flight\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Client\Model\Client;

class FlightComplianceForm extends Form
{
    public function initialize()
    {
     
        $this->add(new Text("load_master_id"));   
        $this->add(new Text("flight_id"));   
        $this->add(new Text("leg_id"));   
        $this->add(new Text("date"));   
        $this->add(new Text("pfr"));   
        $this->add(new Text("pfm"));   
        $this->add(new Text("pfe"));   
        $this->add(new Text("notes"));
        $this->add(new Text("created_at"));
        $this->add(new Text("updated_at"));
    }

}   