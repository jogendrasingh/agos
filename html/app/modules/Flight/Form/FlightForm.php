<?php

namespace Flight\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Client\Model\Client;

class FlightForm extends Form
{
    public function initialize()
    {
     
        $this->add((new Select('client_id', Client::getClient(),
            array(
            'useEmpty'  =>  true,
            'emptyText' =>  'Select',
            'using'     => array('id', 'name'),
            'required' => true)))
            ->addValidator(new PresenceOf([
            'message' => 'Client can not be empty'
        ]))->setLabel('Select Client'));

        $this->add(
            (new Text('aircraft_registration_no', [
                'required' => true,
            ]))->setLabel('Aircraft Registration No')
        );

        $this->add(
            (new select('support_type', [''=>'Select','local'=>'Local','flying'=>'Flying'],[
                'required' => true,
                'onChange' => "changelocalleg(this.value)",
            ]))->setLabel('Support Type')
        );

        $this->add(
            (new Check('active'))
                ->setLabel('Active')
        );     

    }

}   