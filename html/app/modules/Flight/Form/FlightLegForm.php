<?php

namespace Flight\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Client\Model\Client;

class FlightLegForm extends Form
{
    public function initialize()
    {
     
        $this->add(new Text("flight_id"));   
        $this->add(new Text("flight_no"));   
        $this->add(new Text("station_source_id"));   
        $this->add(new Text("std"));   
        $this->add(new Text("station_dest_id"));   
        $this->add(new Text("trained"));   
        $this->add(new Text("sta"));
        $this->add(new Text("active"));
        $this->add(new Text("created_at"));
        $this->add(new Text("updated_at"));

        $this->add(new Text("commercial"));   
        $this->add(new Text("reimbursable"));   
        $this->add(new Text("reimbursableClient"));   
        $this->add(new Text("etd"));
        $this->add(new Text("eta"));
    }

}   