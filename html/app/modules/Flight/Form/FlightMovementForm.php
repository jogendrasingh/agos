<?php

namespace Flight\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Client\Model\Client;

class FlightMovementForm extends Form
{
    public function initialize()
    {
     
        $this->add(new Text("load_master_id"));   
        $this->add(new Text("flight_id"));   
        $this->add(new Text("leg_id"));   
        $this->add(new Text("assign_id"));   
        
        $this->add(
            (new Text('date', [
                'required' => true,
            ]))->setLabel('Date')
        );

        $this->add(
            (new Text('source_etd', [
                'required' => true,
            ]))->setLabel('Source ETD')
        );
        $this->add(
            (new Text('push_back_time', [
                'required' => true,
            ]))->setLabel('Push Back Time /Chocks Off')
        );
        $this->add(
            (new Text('airborne_time', [
                'required' => true,
            ]))->setLabel('Airborne Time')
        );
        $this->add(
            (new Text('destination_eta', [
                'required' => true,
            ]))->setLabel('Destination ETA')
        );
        $this->add(
            (new Text('touch_down_time', [
                'required' => true,
            ]))->setLabel('Touch Down Time')
        );
        $this->add(
            (new Text('chocks_on', [
                'required' => true,
            ]))->setLabel('Chocks On')
        );
        $this->add(
            (new Text('delay_reason'))->setLabel('Delay Reason')
        );
        $this->add(
            (new Text('incoming_payload', [
                'required' => true,
            ]))->setLabel('Incoming Payload (in Kgs)')
        );
        $this->add(
            (new Text('offload', [
                'required' => true,

            ]))->setLabel('Offload (in Kgs)')
        );
        $this->add(
            (new Text('transit', [
                'required' => true,
                'readonly' => readonly,
            ]))->setLabel('Transit (in Kgs)')
        );
        $this->add(
            (new Text('joining', [
                'required' => true,
            ]))->setLabel('Joining (in Kgs)')
        );
        $this->add(
            (new Text('total_payload', [
                'required' => true,
                'readonly' => readonly,
            ]))->setLabel('Total Payload (in Kgs)')
        );

        $this->add(
            (new TextArea('notes'))->setLabel('Notes')
        );
        $this->add(new Text("created_at"));
        $this->add(new Text("updated_at"));
        $this->add(new Text("pfr"));
        $this->add(new Text("pfm"));
        $this->add(new Text("pfe"));
        $this->add(new Text("uws_received"));
        $this->add(new Text("lir_issued"));
        $this->add(new Text("loading_start"));
        $this->add(new Text("loading_finish"));
    }

}   