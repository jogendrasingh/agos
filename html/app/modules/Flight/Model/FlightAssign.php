<?php

namespace Flight\Model;

use Application\Mvc\Model\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\PresenceOf;
use Application\Localization\Transliterator;

class FlightAssign extends Model
{

    public function getSource()
    {
        return "flight_assign";
    }

  
    public $id;
    public $flight_id;
    public $leg_id;
    public $load_master_id;
    public $support;
    public $commercial;
    public $visa;
    public $created_at;
    public $updated_at;

    
    public function initialize()
    {
    }
    
 
    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }
  /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
/*    public function getActive()
    {
        return $this->active;
    }
    

    public function isActive()
    {
        if ($this->active) {
            return true;
        }
    }
    
     public function setActive($active)
    {
        $this->active = $active;
    }
    
    public function setCheckboxes($post)
    {
        $this->setActive(isset($post['active']) ? 1 : 0);
    }*/

    
}
