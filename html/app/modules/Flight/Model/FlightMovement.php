<?php

namespace Flight\Model;

use Application\Mvc\Model\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\PresenceOf;
use Application\Localization\Transliterator;

class FlightMovement extends Model
{

    public function getSource()
    {
        return "flight_movement";
    }

  
    public $id;
    public $assign_id;
    public $load_master_id;
    public $flight_id;
    public $leg_id;
    public $date;
    public $source_etd;
    public $push_back_time;
    public $airborne_time;
    public $destination_eta;
    public $touch_down_time;
    public $chocks_on;
    public $delay_reason;
    public $incoming_payload;
    public $offload;
    public $transit;
    public $joining;
    public $total_payload;
    public $notes;
    public $created_at;
    public $updated_at;
    public $pfr;
    public $pfm;
    public $pfe;
    public $uws_received;
    public $lir_issued;
    public $loading_start;
    public $loading_finish;

    
    public function initialize()
    {
    }
    
 
    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }
  /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setSourceEtd($source_etd){
        $this->source_etd = $source_etd;
    }

    public function getSourceEtd(){
        return  $this->source_etd;
    }
    
}
