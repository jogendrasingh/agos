<?php

/**
 * Routes
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace Flight;

use Application\Mvc\Router\DefaultRouter;
//use Category\Model\Category;

class Routes
{

     public function init($router)
    {    
    $router->add('/flight', array(
            'module'     => 'flight',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('client');
        
        $router->add('/flight/', array(
            'module'     => 'flight',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('flight');
        
        $router->add('/flight/admin/', array(
            'module'     => 'flight',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('flight');
        
        
        return $router;
    }     

}