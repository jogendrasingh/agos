<style>.error{ color: red; }</style>
<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}flight/admin" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
        {% if(not(model.id is empty)) %}
            <a href="{{ url.get() }}flight/admin/delete/{{ model.id }}" class="ui button red">
                <i class="icon trash"></i> Delete
            </a>
        {% endif %}
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <div class="two fields">
            <div class="field">
                {{ form.renderDecorated('client_id') }}
                {{ form.renderDecorated('support_type') }}
                <!-- {{ form.renderDecorated('aircraft_registration_no') }} -->
                {{ form.renderDecorated('active') }}
            </div>
            <div class="field">
            </div>
        </div>

        <div class="two fields" id="maintable">
            <div class="field">
                    <h4>Leg 1</h4>
                    <div class="field">
                        <label for="flight_no">Flight No</label>
                        <input type="text" name="flight_no[leg1]" id="flight_no-1" value="{% if legmodel[0]['flight_no'] %}{{legmodel[0]['flight_no']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_source">Source Station</label>
                        <select name="station_source_id[leg1]" id="station_source_id-1">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[0]['station_source_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div class="field">
                        <label for="std">STD</label>
                        <input type="text" name="std[leg1]" id="std-1" value="{% if legmodel[0]['std'] %}{{legmodel[0]['std']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="std">ETD</label>
                        <input type="text" name="etd[leg1]" id="etd-1" value="{% if legmodel[0]['etd'] %}{{legmodel[0]['etd']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_dest">Destination Station</label>
                        <select name="station_dest_id[leg1]" id="station_dest_id-1" onchange="checkStation(this.id,this.value)">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[0]['station_dest_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                        <span id="error_1" class="error"></span>
                    </div>
                    <div class="field">
                        <label for="trained">Trained</label>
                        <select name="trained[leg1]" id="trained-1">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[0]['trained']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[0]['trained']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field">
                        <label for="sta">STA</label>
                        <input type="text" name="sta[leg1]" id="sta-1"  value="{% if legmodel[0]['sta'] %}{{legmodel[0]['sta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="eta">ETA</label>
                        <input type="text" name="eta[leg1]" id="eta-1"  value="{% if legmodel[0]['eta'] %}{{legmodel[0]['eta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="commercial">Commercial</label>
                        <select name="commercial[leg1]" id="commercial-1" onchange="commercial(this.value,'1')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[0]['commercial']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[0]['commercial']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimburs-1">
                        <label for="reimbursable">Reimbursable</label>
                        <select name="reimbursable[leg1]" id="reimbursable-1" onchange = reimbursment(this.value,'1')>
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[0]['reimbursable']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[0]['reimbursable']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimbursClient-1">
                        <label for="reimbursable">Reimbursable Client</label>
                        <select name="reimbursableClient[leg1]" id="reimbursableClient-1">
                            <option value="">Select</option>
                            {% for key,rClient in reimbursClient %}
                            <option value="{{key}}">{{rClient}}</option>
                            {% endfor%}

                        </select>
                    </div>
                    <input type="hidden" name="id[leg1]" value="{% if legmodel[0]['id'] %}{{legmodel[0]['id']}}{% endif %}">
            </div>
            
            <div class="field">
                    <h4>Leg 2</h4>
                    <div class="field">
                            <label for="flight_no">Flight No</label>
                        <input type="text" name="flight_no[leg2]" id="flight_no-2" value="{% if legmodel[1]['flight_no'] %}{{legmodel[1]['flight_no']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_source">Source Station</label>
                        <select name="station_source_id[leg2]" id="station_source_id-2">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[1]['station_source_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div class="field">
                        <label for="std">STD</label>
                        <input type="text" name="std[leg2]" id="std-2" value="{% if legmodel[1]['std'] %}{{legmodel[1]['std']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="etd">ETD</label>
                        <input type="text" name="etd[leg2]" id="etd-2" value="{% if legmodel[0]['etd'] %}{{legmodel[0]['etd']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_dest">Destination Station</label>
                        <select name="station_dest_id[leg2]" id="station_dest_id-2" onchange="checkStation(this.id,this.value)">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[1]['station_dest_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                        <span id="error_2" class="error"></span>
                    </div>
                    <div class="field">
                        <label for="trained">Trained</label>
                        <select name="trained[leg2]" id="trained-2">
                            <option value="">Select</option>    
                            <option value="1" {% if legmodel[1]['trained']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[1]['trained']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field">
                        <label for="sta">STA</label>
                        <input type="text" name="sta[leg2]" id="sta-2" value="{% if legmodel[1]['sta'] %}{{legmodel[1]['sta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="eta">ETA</label>
                        <input type="text" name="eta[leg2]" id="eta-2" value="{% if legmodel[1]['eta'] %}{{legmodel[1]['eta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="commercial">Commercial</label>
                        <select name="commercial[leg2]" id="commercial-2" onchange="commercial(this.value,'2')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[1]['commercial']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[1]['commercial']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimburs-2">
                        <label for="reimbursable">Reimbursable</label>
                        <select name="reimbursable[leg2]" id="reimbursable-2" onchange = reimbursment(this.value,'2')>
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[1]['reimbursable']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[1]['reimbursable']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimbursClient-2">
                        <label for="reimbursable">Reimbursable Client</label>
                        <select name="reimbursableClient[leg2]" id="reimbursableClient-2">
                            <option value="">Select</option>
                            {% for key,rClient in reimbursClient %}
                            <option value="{{key}}">{{rClient}}</option>
                            {% endfor%}
                        </select>
                    </div>
                    <input type="hidden" name="id[leg2]" value="{% if legmodel[1]['id'] %}{{legmodel[1]['id']}}{% endif %}">
            </div>
        </div>
        <div class="two fields" id="divleg3" style="display: none;">
            <div class="field">
                    <h4>Leg 3</h4>
                    <div class="field">
                        <label for="flight_no">Flight No</label>
                        <input type="text" name="flight_no[leg3]" id="flight_no-3" value="{% if legmodel[2]['flight_no'] %}{{legmodel[2]['flight_no']}}{% endif %}">
                    </div>
                    <div class="field">
                        <labelS for="station_source">Source Station</label>
                        <select name="station_source_id[leg3]" id="station_source_id-3">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[2]['station_source_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div class="field">
                        <label for="std">STD</label>
                        <input type="text" name="std[leg3]" id="std-3" value="{% if legmodel[2]['std'] %}{{legmodel[2]['std']}}{% endif %}">
                    </div>

                    <div class="field">
                        <label for="etd">ETD</label>
                        <input type="text" name="etd[leg3]" id="etd-3" value="{% if legmodel[2]['etd'] %}{{legmodel[2]['etd']}}{% endif %}">
                    </div>                    
                    <div class="field">
                        <label for="station_dest">Destination Station</label>
                        <select name="station_dest_id[leg3]" id="station_dest_id-3" onchange="checkStation(this.id,this.value)">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[2]['station_dest_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                        <span id="error_3" class="error"></span>
                    </div>
                    <div class="field">
                        <label for="trained">Trained</label>
                        <select name="trained[leg3]" id="trained-3">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[2]['trained']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[2]['trained']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field">
                        <label for="sta">STA</label>
                        <input type="text" name="sta[leg3]" id="sta-3"  value="{% if legmodel[2]['sta'] %}{{legmodel[2]['sta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="eta">ETA</label>
                        <input type="text" name="eta[leg3]" id="eta-3"  value="{% if legmodel[2]['eta'] %}{{legmodel[2]['eta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="commercial">Commercial</label>
                        <select name="commercial[leg3]" id="commercial-3" onchange="commercial(this.value,'3')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[2]['commercial']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[2]['commercial']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimburs-3">
                        <label for="reimbursable">Reimbursable</label>
                        <select name="reimbursable[leg3]" id="reimbursable-3" onchange = "reimbursment(this.value,'3')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[2]['reimbursable']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[2]['reimbursable']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimbursClient-3">
                        <label for="reimbursable">Reimbursable Client</label>
                        <select name="reimbursableClient[leg3]" id="reimbursableClient-3">
                            <option value="">Select</option>
                            {% for key,rClient in reimbursClient %}
                            <option value="{{key}}">{{rClient}}</option>
                            {% endfor%}
                        </select>
                    </div>
                    <input type="hidden" name="id[leg3]" value="{% if legmodel[2]['id'] %}{{legmodel[2]['id']}}{% endif %}">
            </div>
            <div class="field" id="localsupportdiv">
                    <h4>Leg 4</h4>
                    <div class="field">
                        <label for="flight_no">Flight No</label>
                        <input type="text" name="flight_no[leg4]" id="flight_no-4" value="{% if legmodel[3]['flight_no'] %}{{legmodel[3]['flight_no']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_source">Source Station</label>
                        <select name="station_source_id[leg4]" id="station_source_id-4">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[3]['station_source_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div class="field">
                        <label for="std">STD</label>
                        <input type="text" name="std[leg4]" id="std-4" value="{% if legmodel[3]['std'] %}{{legmodel[3]['std']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="etd">ETD</label>
                        <input type="text" name="etd[leg4]" id="etd-4" value="{% if legmodel[3]['etd'] %}{{legmodel[3]['etd']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_dest">Destination Station</label>
                        <select name="station_dest_id[leg4]" id="station_dest_id-4" onchange="checkStation(this.id,this.value)">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[3]['station_dest_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                        <span id="error_4" class="error"></span>
                    </div>
                    <div class="field">
                        <label for="trained">Trained</label>
                        <select name="trained[leg4]" id="trained-4">
                            <option value="">Select</option>    
                            <option value="1" {% if legmodel[3]['trained']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[3]['trained']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field">
                        <label for="sta">STA</label>
                        <input type="text" name="sta[leg4]" id="sta-4" value="{% if legmodel[3]['sta'] %}{{legmodel[3]['sta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="eta">ETA</label>
                        <input type="text" name="eta[leg4]" id="eta-4" value="{% if legmodel[3]['eta'] %}{{legmodel[3]['eta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="commercial">Commercial</label>
                        <select name="commercial[leg4]" id="commercial-4" onchange="commercial(this.value,'4')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[3]['commercial']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[3]['commercial']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimburs-4">
                        <label for="reimbursable">Reimbursable</label>
                        <select name="reimbursable[leg4]" id="reimbursable-4" onchange = "reimbursment(this.value,'4')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[3]['reimbursable']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[3]['reimbursable']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimbursClient-4">
                        <label for="reimbursable">Reimbursable Client</label>
                        <select name="reimbursableClient[leg4]" id="reimbursableClient-4">
                            <option value="">Select</option>
                            {% for key,rClient in reimbursClient %}
                            <option value="{{key}}">{{rClient}}</option>
                            {% endfor%}
                        </select>
                    </div>
                    <input type="hidden" name="id[leg4]" value="{% if legmodel[3]['id'] %}{{legmodel[3]['id']}}{% endif %}">
            </div>

        </div>
        <div class="two fields" id="divleg5" style="display: none;">
            <div class="field">
                    <h4>Leg 5</h4>
                    <div class="field">
                        <label for="flight_no">Flight No</label>
                        <input type="text" name="flight_no[leg5]" id="flight_no-5" value="{% if legmodel[4]['flight_no'] %}{{legmodel[4]['flight_no']}}{% endif %}">
                    </div>
                    <div class="field">
                        <labelS for="station_source">Source Station</label>
                        <select name="station_source_id[leg5]" id="station_source_id-5">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[4]['station_source_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div class="field">
                        <label for="std">STD</label>
                        <input type="text" name="std[leg5]" id="std-5" value="{% if legmodel[4]['std'] %}{{legmodel[4]['std']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="etd">ETD</label>
                        <input type="text" name="etd[leg5]" id="etd-5" value="{% if legmodel[4]['etd'] %}{{legmodel[4]['etd']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_dest">Destination Station</label>
                        <select name="station_dest_id[leg5]" id="station_dest_id-5" onchange="checkStation(this.id,this.value)">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[4]['station_dest_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                        <span id="error_4" class="error"></span>
                    </div>
                    <div class="field">
                        <label for="trained">Trained</label>
                        <select name="trained[leg5]" id="trained-5">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[4]['trained']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[4]['trained']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field">
                        <label for="sta">STA</label>
                        <input type="text" name="sta[leg5]" id="sta-5"  value="{% if legmodel[4]['sta'] %}{{legmodel[4]['sta']}}{% endif %}">
                    </div>

                    <div class="field">
                        <label for="sta">ETA</label>
                        <input type="text" name="eta[leg5]" id="eta-5"  value="{% if legmodel[4]['eta'] %}{{legmodel[4]['eta']}}{% endif %}">
                    </div>                    
                    <div class="field">
                        <label for="commercial">Commercial</label>
                        <select name="commercial[leg5]" id="commercial-5" onchange="commercial(this.value,'5')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[4]['commercial']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[4]['commercial']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimburs-5">
                        <label for="reimbursable">Reimbursable</label>
                        <select name="reimbursable[leg5]" id="reimbursable-5" onchange = "reimbursment(this.value,'5')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[4]['reimbursable']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[4]['reimbursable']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimbursClient-5">
                        <label for="reimbursable">Reimbursable Client</label>
                        <select name="reimbursableClient[leg5]" id="reimbursableClient-5">
                            <option value="">Select</option>
                            {% for key,rClient in reimbursClient %}
                            <option value="{{key}}">{{rClient}}</option>
                            {% endfor%}
                        </select>
                    </div>
                    <input type="hidden" name="id[leg5]" value="{% if legmodel[4]['id'] %}{{legmodel[4]['id']}}{% endif %}">
            </div>
            <div class="field">
                    <h4>Leg 6</h4>
                    <div class="field">
                        <label for="flight_no">Flight No</label>
                        <input type="text" name="flight_no[leg6]" id="flight_no-6" value="{% if legmodel[5]['flight_no'] %}{{legmodel[5]['flight_no']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_source">Source Station</label>
                        <select name="station_source_id[leg6]" id="station_source_id-6">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[5]['station_source_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div class="field">
                        <label for="std">STD</label>
                        <input type="text" name="std[leg6]" id="std-6" value="{% if legmodel[5]['std'] %}{{legmodel[5]['std']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="etd">ETD</label>
                        <input type="text" name="etd[leg6]" id="etd-6" value="{% if legmodel[5]['etd'] %}{{legmodel[5]['etd']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="station_dest">Destination Station</label>
                        <select name="station_dest_id[leg6]" id="station_dest_id-6" onchange="checkStation(this.id,this.value)">
                            <option value="">Select</option>
                            {% for st in station %}
                            <option value="{{st.id}}" {% if legmodel[5]['station_dest_id']==st.id %}selected{% endif%}>{{st.station_code}}</option>
                            {% endfor %}
                        </select>
                        <span id="error_6" class="error"></span>
                    </div>
                    <div class="field">
                        <label for="trained">Trained</label>
                        <select name="trained[leg6]" id="trained-6">
                            <option value="">Select</option>    
                            <option value="1" {% if legmodel[5]['trained']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[5]['trained']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field">
                        <label for="sta">STA</label>
                        <input type="text" name="sta[leg6]" id="sta-6" value="{% if legmodel[5]['sta'] %}{{legmodel[5]['sta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="sta">ETA</label>
                        <input type="text" name="eta[leg6]" id="eta-6" value="{% if legmodel[5]['eta'] %}{{legmodel[5]['eta']}}{% endif %}">
                    </div>
                    <div class="field">
                        <label for="commercial">Commercial</label>
                        <select name="commercial[leg6]" id="commercial-6" onchange="commercial(this.value,'6')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[5]['commercial']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[5]['commercial']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimburs-5">
                        <label for="reimbursable">Reimbursable</label>
                        <select name="reimbursable[leg6]" id="reimbursable-6" onchange = "reimbursment(this.value,'6')">
                            <option value="">Select</option>
                            <option value="1" {% if legmodel[5]['reimbursable']=='1' %}selected{% endif%}>Yes</option>
                            <option value="0" {% if legmodel[5]['reimbursable']=='0' %}selected{% endif%}>No</option>
                        </select>
                    </div>
                    <div class="field" style="display: none;" id="reimbursClient-6">
                        <label for="reimbursable">Reimbursable Client</label>
                        <select name="reimbursableClient[leg6]" id="reimbursableClient-6">
                            <option value="">Select</option>
                            {% for key,rClient in reimbursClient %}
                            <option value="{{key}}">{{rClient}}</option>
                            {% endfor%}
                        </select>
                    </div>
                    <input type="hidden" name="id[leg6]" value="{% if legmodel[5]['id'] %}{{legmodel[5]['id']}}{% endif %}">
            </div>

        </div>
         <div id="dynamicfield"></div>   
        <div class="ui button" onclick="displaydiv('1');" id="legbutton1">Add Leg</div>
         <div class="ui button" onclick="displaydiv('2');" id="legbutton2" style="display: none;">Add Leg</div>
    </div>

</form>
<link href="{{ url.path() }}static/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
{{ javascript_include("/static/js/moment.js") }}
{{ javascript_include("/static/js/bootstrap-datetimepicker.js") }}


<script type="text/javascript">

    $(function () {
                $('#std-1').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#std-2').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#std-3').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#std-4').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#std-5').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#std-6').datetimepicker({format: 'MM/DD/YYYY HH:mm'});

                $('#etd-1').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#etd-2').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#etd-3').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#etd-4').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#etd-5').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#etd-6').datetimepicker({format: 'MM/DD/YYYY HH:mm'});

                $('#sta-1').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#sta-2').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#sta-3').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#sta-4').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#sta-5').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#sta-6').datetimepicker({format: 'MM/DD/YYYY HH:mm'});

                $('#eta-1').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#eta-2').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#eta-3').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#eta-4').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#eta-5').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                $('#eta-6').datetimepicker({format: 'MM/DD/YYYY HH:mm'});
                

    });


    $(function(){
        $("#std-1").on("dp.change", function (e) {
        $('#etd-1').data("DateTimePicker").minDate(e.date);
        $('#sta-1').data("DateTimePicker").minDate(e.date);
        $('#eta-1').data("DateTimePicker").minDate(e.date);
        });
        $("#sta-1").on("dp.change", function (e) {
        $('#std-2').data("DateTimePicker").minDate(e.date);
        $('#etd-2').data("DateTimePicker").minDate(e.date);
        });
        $("#std-2").on("dp.change", function (e) {
        //$('#etd-2').data("DateTimePicker").minDate(e.date);
        $('#sta-2').data("DateTimePicker").minDate(e.date);
        $('#eta-2').data("DateTimePicker").minDate(e.date);
        });
        $("#sta-2").on("dp.change", function (e) {
        $('#std-3').data("DateTimePicker").minDate(e.date);
        $('#etd-3').data("DateTimePicker").minDate(e.date);
        });
        $("#std-3").on("dp.change", function (e) {
        $('#sta-3').data("DateTimePicker").minDate(e.date);
        $('#eta-3').data("DateTimePicker").minDate(e.date);
        });
        $("#sta-3").on("dp.change", function (e) {
        $('#std-4').data("DateTimePicker").minDate(e.date);
        $('#etd-4').data("DateTimePicker").minDate(e.date);
        });
        $("#std-4").on("dp.change", function (e) {
        $('#sta-4').data("DateTimePicker").minDate(e.date);
        $('#eta-4').data("DateTimePicker").minDate(e.date);
        });
        $("#sta-4").on("dp.change", function (e) {
        $('#std-5').data("DateTimePicker").minDate(e.date);
        $('#etd-5').data("DateTimePicker").minDate(e.date);
        });
        $("#std-5").on("dp.change", function (e) {
        $('#sta-5').data("DateTimePicker").minDate(e.date);
        $('#eta-5').data("DateTimePicker").minDate(e.date);
        });
        $("#sta-5").on("dp.change", function (e) {
        $('#std-6').data("DateTimePicker").minDate(e.date);
        $('#etd-6').data("DateTimePicker").minDate(e.date);
        });
        $("#std-6").on("dp.change", function (e) {
        $('#sta-6').data("DateTimePicker").minDate(e.date);
        $('#eta-6').data("DateTimePicker").minDate(e.date);
        });
    
    });

      
    $('.ui.form').form({
           fields: {
            client_id: {
                identifier: 'client_id',
                rules: [
                    {type: 'empty'}
                ]
            },
            support_type: {
                identifier: 'support_type',
                rules: [
                    {type: 'empty'}
                ]
            },
    }
 });

function checkStation(id,val){
    if(val!=""){
        var arr = id.split('-');
        var sstation = 'station_source_id-'+arr[1];
        var sstationval = $("#"+sstation).val();
        if(sstationval!=''){
            if(val==sstationval){
            $("#error_"+arr[1]).text('Destination and Source Station Can not be same');
            return false;
            }else{
                $("#error_"+arr[1]).text('');
                var num = parseInt(arr[1])+1;

             if($("#station_source_id-"+num).length){
                $("#station_source_id-"+num).val(val).attr("disabled", true);
                var dynamic = "<input type='hidden' name='station_source_id[leg"+num+"]' value='"+val+"'>";
                //alert(dynamic);
                $("#dynamicfield").append(dynamic);

             }    
            }    
        }
        
    }
}

{% if legmodel[0]['station_dest_id'] is not '' %}
checkStation('station_dest_id-1',{{legmodel[0]['station_dest_id']}});
{% endif%}
{% if legmodel[1]['station_dest_id'] is not '' %}
checkStation('station_dest_id-2',{{legmodel[1]['station_dest_id']}});
{% endif%}
{% if legmodel[2]['station_dest_id'] is not '' %}
checkStation('station_dest_id-3',{{legmodel[2]['station_dest_id']}});
displaydiv('1');
{% endif%}
{% if legmodel[3]['station_dest_id'] is not '' %}
checkStation('station_dest_id-4',{{legmodel[3]['station_dest_id']}});
{% endif%}
{% if legmodel[4]['station_dest_id'] is not '' %}
checkStation('station_dest_id-5',{{legmodel[4]['station_dest_id']}});
displaydiv('2');
{% endif%}
{% if legmodel[5]['station_dest_id'] is not '' %}
checkStation('station_dest_id-6',{{legmodel[5]['station_dest_id']}});
{% endif%}

function displaydiv(val){
    if(val=='1'){
        $("#legbutton1").hide();
        $("#legbutton2").show();
        $("#divleg3").show();
    }else if(val=='2'){
        $("#divleg5").show();
    }
}


function changelocalleg(val){
    //alert(val);
    if(val=='local'){
        displaydiv(1);
        $('#localsupportdiv').hide();
        $("#legbutton2").hide();
    }else{
        $('#localsupportdiv').show();
        $("#legbutton2").show();
    }
}

function commercial(val,id){
    if(val==1){
        $("#reimburs-"+id).show();
    }else{
        $("#reimburs-"+id).hide();
        $("#reimbursClient-"+id).hide();
    }
}

function reimbursment(val,id){
    if(val==1){
        $("#reimbursClient-"+id).show();
        var client = $("#client_id").val();
        if(client!=''){
            $("#reimbursableClient-"+id).val(client);    
        }
    }else{
        $("#reimbursClient-"+id).hide();
    }
}

</script>

<?php if($model->support_type){ ?>
<script>
changelocalleg('<?=$model->support_type?>');

{% if legmodel[0]['commercial'] is not '' %}
commercial({{legmodel[0]['commercial']}},'1');
{% endif%}
{% if legmodel[1]['commercial'] is not '' %}
commercial({{legmodel[1]['commercial']}},'2');
{% endif%}
{% if legmodel[2]['commercial'] is not '' %}
commercial({{legmodel[2]['commercial']}},'3');
{% endif%}
{% if legmodel[3]['commercial'] is not '' %}
commercial({{legmodel[3]['commercial']}},'4');
{% endif%}
{% if legmodel[4]['commercial'] is not '' %}
commercial({{legmodel[4]['commercial']}},'5');
{% endif%}
{% if legmodel[5]['commercial'] is not '' %}
commercial({{legmodel[5]['commercial']}},'6');
{% endif%}

{% if legmodel[0]['reimbursable'] is not '' %}
reimbursment({{legmodel[0]['reimbursable']}},'1');
{% endif%}
{% if legmodel[1]['reimbursable'] is not '' %}
reimbursment({{legmodel[1]['reimbursable']}},'2');
{% endif%}
{% if legmodel[2]['reimbursable'] is not '' %}
reimbursment({{legmodel[2]['reimbursable']}},'3');
{% endif%}
{% if legmodel[3]['reimbursable'] is not '' %}
reimbursment({{legmodel[3]['reimbursable']}},'4');
{% endif%}
{% if legmodel[4]['reimbursable'] is not '' %}
reimbursment({{legmodel[4]['reimbursable']}},'5');
{% endif%}
{% if legmodel[5]['reimbursable'] is not '' %}
reimbursment({{legmodel[5]['reimbursable']}},'6');
{% endif%}

</script>
<?php } ?>

