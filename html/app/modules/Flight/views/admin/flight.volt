<!doctype html>
<!--controls-->
<!-- <form method="post" action="" class="ui form"> -->

    <!--controls-->
    <!-- <div class="ui segment">

        <a href="{{ url.get() }}flight/admin" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
    </div> -->
    <!--end controls-->
   <!--  <div class="ui segment">
        <div class="two fields">
            <div class="field">    
                <div class="field">
                        <label for="flight_leg">Flight Legs</label>
                        <select name="leg_id" id="leg_id">
                            <option value="">Select</option>
                            {% for ft in flightleg %}
                            <option value="{{ft.id}}">{{ft.flight_no}}</option>
                            {% endfor %}
                        </select>
                </div>
                <div class="field">
                        <label for="load_master">Load Master</label>
                        <select name="load_master_id" id="load_master_id">
                            <option value="">Select</option>
                            {% for lm in loadmaster %}
                            <option value="{{lm.id}}">{{lm.first_name}}</option>
                            {% endfor %}
                        </select>
                </div>
            </div>

            <div class="field">
                <div class="field">
                        <label for="Support">Support</label>
                        <select name="support" id="support">
                            <option value="">Select</option>
                            <option value="Flying">Flying</option>
                            <option value="Local">Local</option>
                        </select>
                </div>
                <div class="field">
                        <label for="commercial">Commercial</label>
                        <select name="commercial" id="commercial">
                            <option value="">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                </div>
                <div class="field">
                        <label for="visa">Visa</label>
                        <select name="visa" id="visa">
                            <option value="">Select</option>
                            <option value="Crew">Crew</option>
                            <option value="General Declaration">General Declaration</option>
                            <option value="Residence Visa">Residence Visa</option>
                            <option value="Others">Others</option>
                        </select>
                </div>
            </div>
        </div>
    </div> -->

<!-- </form> -->

<!--/end controls-->
<div class="ui segment">
{% if flightleg|length is not '' %}
<table class="ui table very compact celled">
    <thead>
    <tr>
        <!-- <th style="width: 100px"></th> -->
        <th>STA</th>
        <th>Station Code</th>
        <th>STD</th>
        <th>Support Type</th>
        <th>Load Master</th>
        <th>Visa Type</th>
        <th>Action</th>
        
    </tr>
    </thead>
    <tbody>
    
   {% for item in flightleg %}
        
        <tr>
            <td>{{date('d-m-y H:i',strtotime(item['sta']))}}</td>
            <td>{{item['station_code']}}</td>
            <td>{% if item['std'] is not '' %}{{ date('d-m-y H:i',strtotime(item['std']))}}
            {%else %}
             ----
            {% endif%}
            </td>
            <td id="s{{item['id']}}">
            {% if item['support'] %}
            {{item['support']}}
            {% else %}
            <select name="support" id="support_{{item['id']}}">
                            <option value="">Select</option>
                            <option value="Flying">Flying</option>
                            <option value="Local">Local</option>
                        </select>
            {% endif %}
            </td>
            <td id="master{{item['id']}}">
            {% if item['first_name'] %}
            {{item['first_name']}}
            {% else %}        
            <select name="load_master_id" id="load_master_id_{{item['id']}}">
                            <option value="">Select</option>
                            {% for lm in loadmaster %}
                            <option value="{{lm.id}}">{{lm.first_name}}</option>
                            {% endfor %}
            </select>
             {% endif %}   
            </td>
            <td id="visa{{item['id']}}">
            {% if item['visa'] %}
            {{item['visa']}}
            {% else %}        
            <select name="visa" id="visa_{{item['id']}}">
                            <option value="">Select</option>
                            <option value="Crew">Crew</option>
                            <option value="General Declaration">General Declaration</option>
                            <option value="Residence Visa">Residence Visa</option>
                            <option value="Others">Others</option>
            </select>
             {% endif %}
            </td>
             <td id="b{{item['id']}}">
             {% if item['visa'] %}
             <a href="{{url.get()}}flight/admin/movement/{{item['assign_id']}}" class="btn">Movement</a>
            {% else %}
             <button onClick="savecurrent('{{item['id']}}')">Save</button>
            {% endif %}
             </td>
        
        </div>
        </tr>
    {% endfor %}
    </tbody>    
</table>
{% endif %}
</div>

<script>
function savecurrent(id){
    //alert(id);
 if(id){
    var support_id = $("#support_"+id).val();
    var loadmaster_id = $("#load_master_id_"+id).val();
    var visa_id = $("#visa_"+id).val();
    if(support_id==""){
        alert("Please select support type");
        return false;
    }else if(loadmaster_id==""){
        alert("Please select load Master");
        return false;
    }else if(visa_id==""){
        alert("Please select visa type");
        return false;
    }else{
            $.ajax({
                    url: '{{ url.get() }}flight/admin/assignflight',
                    data: {leg_id:id,support:support_id,load_master_id:loadmaster_id,visa:visa_id},
                    type: 'POST',
                    dataType: 'json',
                    success: function (response) {
                        if (response.value == 'success') {
                            
                            $("#s"+id).html(response.res.support);
                            $("#master"+id).html(response.res.load_master);
                            $("#visa"+id).html(response.res.visa);
                            $link = "<a href='{{url.get()}}flight/admin/movement/"+response.res.assign_id+"' class='btn'>Movement</a>";
                            $("#b"+id).html($link);
                        }
                    },
                    error: function (request, error) {

                         alert('Failed!');
                    }
                });
    }

 }


}
/*        $('.ui.form').form({
           fields: {
            leg_id: {
                identifier: 'leg_id',
                rules: [
                    {type: 'empty'}
                ]
            },
            load_master_id: {
                identifier: 'load_master_id',
                rules: [
                    {type: 'empty'}
                ]
            },
            support: {
                identifier: 'support',
                rules: [
                    {type: 'empty'}
                ]
            },
            commercial: {
                identifier: 'commercial',
                rules: [
                    {type: 'empty'}
                ]
            },
            visa: {
                identifier: 'visa',
                rules: [
                    {type: 'empty'}
                ]
            }
    }
 });*/
</script>