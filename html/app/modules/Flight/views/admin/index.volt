<!doctype html>
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_page.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_table.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/extras/TableTools/media/css/TableTools.css" rel="stylesheet" type="text/css">
<!--controls-->
<div class="ui segment">

    <a href="{{ url.get() }}flight/admin/add" class="ui button positive">
        <i class="icon plus"></i> Add New
    </a>
    
</div>
<!--/end controls-->

<table class="ui table very compact celled" {% if entries|length is not '' %} id='userId' {% endif%}>
    <thead>
    <tr>
        <th style="width: 100px"></th>
        <th>Date</th>
        <th>Client Code</th>
        <th>Service Type</th>
        <th>Commercial (Client)</th>
        <th>Flight No</th>
        <th>Routing</th>
        <th>Action</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    {% if entries|length is not '' %}
   {% for item in entries %}
        {% set link = url.get() ~ "flight/admin/edit/" ~ item['id'] %}
        {% set assignlink = url.get() ~ "flight/admin/flight/" ~ item['id'] %}
        <tr>
            <td><a href="{{ link }}" class="mini ui icon button"><i class="icon edit"></i>
                </a>
            </td>
            <td>{% if item['date'] %}{{item['date']}}{% endif %}</td>
            <td>{{item['client_name']}}</td>
            <td>{{item['support_type']}}</td>
            <td>{{item['commercial']}}</td>
            <td>{{item['flight_no']}}</td>
            <td>
                {% if item['routing'] %}
                <table style="width: 100%">
                {% for rt in item['routing'] %}
                    <tr>
                        <td>{{rt['station']}}</td>
                        <td><small>{{rt['std']}}</small></td>
                    </tr>
                {% endfor %}
                </table>
                {% endif %}
            </td>
            <td>{% if item['active'] %}<a href="{{assignlink}}">Assign</a>{% endif %}</td>
            <td>{% if item['active'] %}<i class="icon checkmark green"></i>{% endif %}</td>
        </tr>
    {% endfor %}
    {% else %}
        <tr><td colspan='5'>Sorry, No Data Found.</td></tr>
    {% endif %}
    </tbody>    
</table>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
        $(document).ready( function () {
                $('#userId').dataTable( {
                        "sDom": 'T<"clear">lfrtip',
                        "aaSorting": [],
                        "oTableTools": {
                                "sSwfPath": "{{ url.path() }}vendor/datagrid/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                } );

        } );


</script>