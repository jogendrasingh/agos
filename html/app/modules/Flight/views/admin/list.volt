<!doctype html>
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_page.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_table.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/extras/TableTools/media/css/TableTools.css" rel="stylesheet" type="text/css">
<!--controls-->

<!--/end controls-->

<table class="ui table very compact celled" {% if listAssign|length is not '' %} id='userId' {% endif%}>
    <thead>
    <tr>
        <th>Load Master</th>
        <th>Aircraft Registration No</th>
        <th>Leg</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    {% if listAssign|length is not '' %}
   {% for item in listAssign %}
        {% set assignlink = url.get() ~ "flight/admin/movement/" ~ item.id %}
        <tr>
            <td>{{item.first_name}}</td>
            <td>{{ item.aircraft_registration_no}}</a></td>
            <td>{{ item.flight_no}}</a></td>
            <td><a href="{{ assignlink }}">Movement</a></td>
        </tr>
    {% endfor %}
    {% else %}
        <tr><td colspan='5'>Sorry, No Data Found.</td></tr>
    {% endif %}
    </tbody>    
</table>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
        $(document).ready( function () {
                $('#userId').dataTable( {
                        "sDom": 'T<"clear">lfrtip',
                        "aaSorting": [],
                        "oTableTools": {
                                "sSwfPath": "{{ url.path() }}vendor/datagrid/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                } );

        } );


</script>