<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">
        <a href="{{ url.get() }}flight/admin/flight/{{flightId}}" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
        
        <a href="{{ url.get() }}flight/admin/report/{{model.id}}" class="ui button pull-right">
            <i class="icon right arrow"></i> Report
        </a>     
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <h4>Movement</h4>
        <div class="two fields">
            <div class="field">
                {{ form.renderDecorated('date') }}
                <div class="field">
                    <label for="source_etd">Source ETD</label>
                    <input type="text" name="source_etd" id="source_etd" value="{% if model.source_etd %}{{date('m/d/Y H:i',strtotime(model.source_etd))}}{% endif %}">
                </div>
                <div class="field">
                    <label for="push_back_time">Push Back Time /Chocks Off</label>
                    <input type="text" name="push_back_time" id="push_back_time" value="{% if model.push_back_time %}{{date('m/d/Y H:i',strtotime(model.push_back_time))}}{% endif %}">
                </div>   
                
                <div class="field">
                    <label for="airborne_time">Airborne Time</label>
                    <input type="text" name="airborne_time" id="airborne_time" value="{% if model.airborne_time %}{{date('m/d/Y H:i',strtotime(model.airborne_time))}}{% endif %}">
                </div>   

                <div class="field">
                    <label for="destination_eta">Destination ETA</label>
                    <input type="text" name="destination_eta" id="destination_eta" value="{% if model.destination_eta %}{{date('m/d/Y H:i',strtotime(model.destination_eta))}}{% endif %}">
                </div>
                <div class="field">
                    <label for="touch_down_time">Touch Down Time</label>
                    <input type="text" name="touch_down_time" id="touch_down_time" value="{% if model.touch_down_time %}{{date('m/d/Y H:i',strtotime(model.touch_down_time))}}{% endif %}">
                </div>
                <div class="field">
                    <label for="chocks_on">Checks On</label>
                    <input type="text" name="chocks_on" id="chocks_on" value="{% if model.chocks_on %}{{date('m/d/Y H:i',strtotime(model.chocks_on))}}{% endif %}">
                </div>   
                {{ form.renderDecorated('delay_reason') }}
            </div>
            <div class="field">
                {{ form.renderDecorated('incoming_payload') }}
                {{ form.renderDecorated('offload') }}
                {{ form.renderDecorated('transit') }}
                {{ form.renderDecorated('joining') }}
                {{ form.renderDecorated('total_payload') }}
                {{ form.renderDecorated('notes') }}
            </div>
        </div>
        <hr>
        <h4>PFR</h4>
        <div class="two fields">
                <div class="field">

                <div class="field">
                    <label for="uws_received">UWS Received</label>
                    <input type="text" name="uws_received" id="uws_received" value="{% if model.uws_received %}{{date('m/d/Y H:i',strtotime(model.uws_received))}}{% endif %}">
                </div>
                <div class="field">
                    <label for="uws_received">LIR Issued</label>
                    <input type="text" name="lir_issued" id="lir_issued" value="{% if model.lir_issued %}{{date('m/d/Y H:i',strtotime(model.lir_issued))}}{% endif %}">
                </div>
                </div>
                <div class="field">

                <div class="field">
                    <label for="uws_received">LoadinG Start</label>
                    <input type="text" name="loading_start" id="loading_start" value="{% if model.loading_start %}{{date('m/d/Y H:i',strtotime(model.loading_start))}}{% endif %}">
                </div>
                <div class="field">
                    <label for="uws_received">Loading Finish</label>
                    <input type="text" name="loading_finish" id="loading_finish" value="{% if model.loading_finish %}{{date('m/d/Y H:i',strtotime(model.loading_finish))}}{% endif %}">
                </div>
                <!-- <div class="field">
                    <label for="pfr">PFR</label>
                       <select name="pfr" id="pfr">
                           <option value="">Select</option>
                           <option value="Pending" {% if model.pfr =='Pending'%} selected {% endif%}>Pending</option>
                           <option value="Completed" {% if model.pfr =='Completed'%} selected {% endif%}> Completed</option>
                       </select>
                </div>
                <div class="field">
                    <label for="pfm">PFM</label>
                       <select name="pfm" id="pfm">
                           <option value="">Select</option>
                           <option value="Pending" {% if model.pfm =='Pending'%} selected {% endif%}>Pending</option>
                           <option value="Completed" {% if model.pfm =='Completed'%} selected {% endif%}>Completed</option>
                       </select>
                </div>
                <div class="field">
                    <label for="pfe">PFE</label>
                       <select name="pfe" id="pfe">
                           <option value="">Select</option>
                           <option value="Pending" {% if model.pfe =='Pending'%} selected {% endif%}>Pending</option>
                           <option value="Completed" {% if model.pfe =='Completed'%} selected {% endif%}>Completed</option>
                       </select>
                </div> -->
                </div>
            </div>
    </div>

</form>

<script>
    $('.ui.form').form({
           fields: {
            date: {
                identifier: 'date',
                rules: [
                    {type: 'empty'}
                ]
            },
            source_etd: {
                identifier: 'source_etd',
                rules: [
                    {type: 'empty'}
                ]
            },
            push_back_time: {
                identifier: 'push_back_time',
                rules: [
                    {type: 'empty'}
                ]
            },
            airborne_time: {
                identifier: 'airborne_time',
                rules: [
                    {type: 'empty'}
                ]
            },
            destination_eta: {
                identifier: 'destination_eta',
                rules: [
                    {type: 'empty'}
                ]
            },
            touch_down_time: {
                identifier: 'touch_down_time',
                rules: [
                    {type: 'empty'}
                ]
            },
            touch_down_time: {
                identifier: 'touch_down_time',
                rules: [
                    {type: 'empty'}
                ]
            },
            chocks_on: {
                identifier: 'chocks_on',
                rules: [
                    {type: 'empty'}
                ]
            },
            incoming_payload: {
                identifier: 'incoming_payload',
                rules: [
                    {type: 'empty'}
                ]
            },
            offload: {
                identifier: 'offload',
                rules: [
                    {type: 'empty'}
                ]
            },
            /*transit: {
                identifier: 'transit',
                rules: [
                    {type: 'empty'}
                ]
            },*/
            joining: {
                identifier: 'joining',
                rules: [
                    {type: 'empty'}
                ]
            },
            uws_received: {
                identifier: 'uws_received',
                rules: [
                    {type: 'empty'}
                ]
            },
            lir_issued: {
                identifier: 'lir_issued',
                rules: [
                    {type: 'empty'}
                ]
            },
            loading_start: {
                identifier: 'loading_start',
                rules: [
                    {type: 'empty'}
                ]
            },
            loading_finish: {
                identifier: 'loading_finish',
                rules: [
                    {type: 'empty'}
                ]
            },
    }
 });
    
</script>
{{ javascript_include("/static/js/datepicker/jquery-ui.js") }}
<link href="{{ url.path() }}static/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
{{ javascript_include("/static/js/moment.js") }}
{{ javascript_include("/static/js/bootstrap-datetimepicker.js") }}
<script>
    $(function () {
        $("#date").datepicker();
        $("#source_etd").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#push_back_time").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#airborne_time").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#destination_eta").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#touch_down_time").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#chocks_on").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#uws_received").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#lir_issued").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#loading_start").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
        $("#loading_finish").datetimepicker({format: 'MM/DD/YYYY HH:mm'});
    });

    $(function () {
    $("#offload").on("keyup",function(){
        var incoming_payload = $("#incoming_payload").val();
        var offload = $("#offload").val();
        var joining = $("#joining").val();
        var trans=0;
        if(incoming_payload!="" && offload!=""){
            trans = parseInt(incoming_payload)-parseInt(offload);
            $("#transit").val(trans);
        }else{
            $("#transit").val(incoming_payload);
        }
        if(joining!=""){
            var sum = parseInt(trans)+parseInt(joining);
            $("#total_payload").val(sum);
        }
    });
    $("#joining").on("keypress keyup blur",function(){
        var transit = $("#transit").val();
        var joining = $("#joining").val();
        var sum=0;
        if(transit!="" && joining!=""){
            var sum = parseInt(transit)+parseInt(joining);
            $("#total_payload").val(sum);
        }else{
            $("#total_payload").val(transit);
        }
    });
     });
</script>