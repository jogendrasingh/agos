<table class="ui table very compact celled">
    <tbody>
    <tr>
        <td>Station of Report</td>
        <td colspan="3">{{report['station_code']}}</td>
        <!-- <td></td>
        <td></td> -->
    </tr>
    <tr>
        <td>Flight Nb.</td>
        <td>{{report['flight_no']}}</td>
        <td>Aircraft Reg.</td>
        <td>{{report['aircraft_registration_no']}}</td>
    </tr>

    <tr>
        <td>Date</td>
        <td>{{date('d-M-y',strtotime(report['date']))}}</td>
        <td>STA / ATA </td>
        <td>{{report['sta']}}</td>
    </tr>
    <tr>
        <td>Routing</td>
        <td>{{report['routing']}}</td>
        <td>STD / ATD </td>
        <td></td>
    </tr>
    <tr>
        <td>Delay – reason</td>
        <td colspan="3">{{report['delay_reason']}}</td>
        <!-- <td></td>
        <td></td> -->
    </tr>
    <tr>
        <td>Station Report Details</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    
    </tbody>    
</table>
<table class="ui table very compact celled">
    <thead>
        <th>Timings:</th>
        <th></th>
        <th>Load details:</th>
        <th></th>
    </thead>
    <tbody>
        <tr>
            <td>UWS received</td>
            <td>{{report['uws_received']}}</td>
            <td>Offload (kg)</td>
            <td>{{report['offload']}}</td>
        </tr>
        <tr>
            <td>LIR issued</td>
            <td>{{report['lir_issued']}}</td>
            <td>Transit (kg)</td>
            <td>{{report['transit']}}</td>
        </tr>
        <tr>
            <td>Loading Start / Finish</td>
            <td>{{report['loading_start']}} / {{report['loading_finish']}}</td>
            <td>Onload / Joining (kg)</td>
            <td>{{report['joining']}}</td>
        </tr>
         
    </tbody>

</table>
