<?php

namespace Index\Controller;

use Application\Mvc\Controller;
use Admin\Model\AdminUser;
use Index\Form\LoginForm;
use Phalcon\Exception;
use Michelf\Markdown;
use Phalcon\Mvc\View;

class IndexController extends Controller
{

    public function indexAction()
    {
//echo "Hi";  die;      
        return $this->redirect($this->url->get() . 'user/index/login');
        die;
        //$this->view->bodyClass = 'home';
        //$auth = $this->session->get('auth');
        //print_r($auth);
        echo "welcome to userss  cccc dashboard ewrewqr werewq rwer wqrw er";die;
        /* $page = Page::findCachedBySlug('index');
        if (!$page) {
            throw new Exception("Page 'index' not found");
        }
        $this->helper->title()->append($page->getMeta_title());
        $this->helper->meta()->set('description', $page->getMeta_description());
        $this->helper->meta()->set('keywords', $page->getMeta_keywords());
        $this->view->page = $page;
      //$this->helper->menu->setActive('index');
     */
    }
    
    public function loginAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $form = new LoginForm();
       if ($this->request->isPost()) {
            if ($this->security->checkToken()) {
                if ($form->isValid($this->request->getPost())) {
                    $login = $this->request->getPost('login', 'string');
                    $password = $this->request->getPost('password', 'string');
                    $user = AdminUser::findFirst("login='$login'");
                    if ($user) {
                        if ($user->checkPassword($password)) {
                            if ($user->isActive()) {
                                $this->session->set('auth', $user->getAuthData());
                                $this->flash->success($this->helper->translate("Welcome to the administrative control panel!"));
                                return $this->redirect($this->url->get() . 'index/index/dashboard');
                            } else {
                                $this->flash->error($this->helper->translate("User is not activated yet"));
                            }
                        } else {
                            $this->flash->error($this->helper->translate("Incorrect login or password"));
                        }
                    } else {
                        $this->flash->error($this->helper->translate("Incorrect login or password"));
                    }
                } else {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                }
            } else {
                $this->flash->error($this->helper->translate("Security errors"));
            }
        } 

        $this->view->form = $form;

    }
    
    public function dashboardAction(){
        echo "Welcome";
        $auth = $this->session->get('auth');
        print_r($auth);
        die;
    }
    
    
    public function logoutAction()
    {
        if ($this->request->isPost()) {
            if ($this->security->checkToken()) {
                $this->session->remove('auth');
            } else {
                $this->flash->error("Security errors");
            }
        } else {
            $this->flash->error("Security errors");
        }
        $this->redirect($this->url->get());
    }

}
