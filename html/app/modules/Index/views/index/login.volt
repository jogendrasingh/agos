<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
                <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="{{ url.get() }}static/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <!--login modal-->
        <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
            
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1 class="text-center"><img src="{{ url.get() }}static/images/logo.png"></h1>
                    </div>
                    <div class=" ">
                        <form class="form col-md-12 center-block" name="reserve" AUTOCOMPLETE = "off" id="reserve" method="post" onsubmit="return ValidateForm();" action="{{ url.get() }}index/index/login">                           
                            <div class="form-group field-btm">
                            <input type="text" class="form-control input required inputData" name="login" id="login" value="" placeholder="Login"><span  id="error_username" ></span> 
                            </div>
                            <div class="form-group field-btm">
                            <input type="password" class="form-control input required inputData" type="password" name="password" id="password" value="" placeholder="Password"><span id="error_password" ></span> 
                            </div><br/>
                            <input type="hidden" name="{{ security.getTokenKey() }}"
                       value="{{ security.getToken() }}"/>
                            <div class="form-group text-center">
                                <button class="btn btn-primary btn-login alt_btn" type="submit" value="Submit" name="submit" id="submit" >Login</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12 text-center"><br/>
                            <a href="javascript:void(0);"><p>Forgot your password?</p></a>
                        </div>	
                    </div>
                </div>
            </div>
            <div class="footer text-center">
                <p style="color:#fff;">Copyright © 2015 FormZero. All rights reserved. </p>
            </div>
        </div>
        <!-- script references -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>



