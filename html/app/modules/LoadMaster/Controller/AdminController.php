<?php
namespace LoadMaster\Controller;
use Admin\Form\AdminUserForm;
use Admin\Model\AdminUser;
use YonaCMS\Plugin\Mail;
use Application\Mvc\Controller;
use LoadMaster\Model\LoadMaster;
use LoadMaster\Form\LoadMasterForm;
use Training\Model\Training;
use LoadMaster\Form\LoadMasterTrainingForm;
use LoadMaster\Model\LoadMasterTraining;
use Station\Model\Station;
use Flight\Model\FlightLeg;
use Flight\Model\FlightAssign;
use LoadMaster\Model\FlightExpenses;
use LoadMaster\Form\FlightExpensesForm;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AdminController extends Controller {

    public function initialize() {
        $this->setAdminEnvironment();
        $this->helper->activeMenu()->setActive('loadmaster');
        $this->mastercurrency = ['INR','USD','EUR','USD','RON','SAR','UGX','SDG'];

    }

    public function indexAction() {

        $this->view->entries = LoadMaster::find([
            "order" => "id DESC"
        ]);
        
        $this->helper->title($this->helper->at('Manage LoadMaster'), true);
    }

    public function addAction() {

        $this->view->pick(['admin/edit']);
        $model = new LoadMaster();
        $form = new LoadMasterForm();
        
        if ($this->request->isPost()) {

            $post = $this->request->getPost();
            
            $adminmodel = new AdminUser();
            $adminpost['login']     = $post['login'];
            $adminpost['password']  = $post['password'];
            $adminpost['role']      = 'loadmaster';
            $adminpost['name']      = $post['first_name'];
            $adminpost['email']      = $post['email'];
            $adminpost['active']    = '1';

            $adminform = new AdminUserForm();
            $adminform->bind($adminpost, $adminmodel);
            
            if ($adminform->isValid()) {
                $adminmodel->save();
                $admin_id = $adminmodel->id;
            }
            $model = new LoadMaster();

            $post['admin_id'] = $admin_id; 
            $post['date_of_birth'] = date("Y-m-d",strtotime($post['date_of_birth']));           
            $post['issue_date'] = date("Y-m-d",strtotime($post['issue_date']));           
            $post['expiry_date'] = date("Y-m-d",strtotime($post['expiry_date']));           
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $this->flash->success($this->helper->at('Load Master created', ['name' => $model->name]));
                    $this->redirect($this->url->get() . 'load-master/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->mastercurrency = $this->mastercurrency;
        $this->view->submitButton = $this->helper->at('Add New');

        $this->helper->title($this->helper->at('Manage Load Master'), true);
    
    }

    public function editAction($id) {
        $model = LoadMaster::findFirst($id);
        if (!$model) {
            $this->redirect($this->url->get() . 'load-master/admin');
        }

        $form = new LoadMasterForm();

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save() == true) {
                    $this->flash->success('Load Master <b>' . $model->first_name . '</b> has been saved');
                    return $this->redirect($this->url->get() . 'load-master/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        } else {
            $form->setEntity($model);
        }

        $this->view->submitButton = $this->helper->at('Save');
        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->mastercurrency = $this->mastercurrency;
        $this->helper->title($this->helper->at('Manage Load Masters'), true);
    }

    
    public function deleteAction($id) {
        $model = LoadMaster::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'load-master/admin');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('Deleting load master <b>' . $model->name . '</b>');
            return $this->redirect($this->url->get() . 'load-master/admin');
        }

        $this->view->model = $model;

        $this->helper->title($this->helper->at('Delete Load Master'), true);
    }

    public function expdeleteAction($id) {
        $model = FlightExpenses::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'load-master/admin/expenses');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('Deleting Expenses <b>' . $model->date . '</b>');
            return $this->redirect($this->url->get() . 'load-master/admin/expenses');
        }

        $this->view->model = $model;

        $this->helper->title($this->helper->at('Delete Load Master'), true);
    }


    public function trainingAction($id){
        if($id){
            $training =Training::find("active='1'");
            $this->view->training = $training;

            $trainingmodel = LoadMasterTraining::query()->columns(array('training_id','training_date','expiry'))->where("load_master_id='".$id."'")->execute()->toArray();
            if(count($trainingmodel)>0){
                $this->view->trainingmodel=$trainingmodel; 
            }

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form = new LoadMasterTrainingForm(); 
            if(count($post['training_date'])>0){
                $len = 0;
                foreach($post['training_date'] as $key=>$val){
                    $data['load_master_id'] = $id;
                    $data['training_id'] = $key;
                    $tr_date='';
                    if($val!=""){
                       $tr_date  = date("Y-m-d",strtotime($val));
                    }
                    $data['training_date'] = $tr_date;
                    
                    $expiry ='';
                    if($post['expiry'][$key]!=""){
                       $expiry  = date("Y-m-d",strtotime($post['expiry'][$key]));
                    }
                    $data['expiry'] = $expiry;

                    $loadMasterArr =LoadMasterTraining::query()->columns('id')->where("load_master_id='".$data['load_master_id']."'")->andWhere("training_id='".$data   ['training_id']."'")->execute()->toArray(); 
                    if(count($loadMasterArr)>0){
                    $model = LoadMasterTraining::findFirst($loadMasterArr[0]['id']);    
                    }else{
                    $model =  new LoadMasterTraining();    
                    }
                    $form->bind($data, $model);
                    if ($form->isValid()) {
                        $model->save();
                        $len++;
                    }        
                }
                if($len>0){
                    $this->flash->success('Traing updated successfully.');
                    return $this->redirect($this->url->get() . 'load-master/admin/training/'.$id);
                }

            }

        }

        }
        $this->helper->title($this->helper->at('Load Master Training'), true);
    }

    public function expensesAction(){

        $auth = $this->session->get('auth');
        if($auth->role!='loadmaster'){
            $this->redirect($this->url->get().'adminuser/admin');
            exit;
        }
        if($auth->id){
            $loadmaster = LoadMaster::findfirst("admin_id='".$auth->id."' and active='1'")->toArray();
            $loadmaster_id = $loadmaster['id'];
        }

        $flightArr = FlightAssign::query()->columns(array('Flight\Model\FlightAssign.id as assign_id','FL.id','FL.flight_no','FL.flight_id','FL.std','FL.sta','SD.station_code','FL.sta'))
        ->leftJoin('Flight\Model\FlightLeg',"FL.id=Flight\Model\FlightAssign.leg_id","FL")
        ->leftJoin("Station\Model\Station","FL.station_dest_id=SD.id","SD")
        ->where("Flight\Model\FlightAssign.load_master_id='".$loadmaster_id."'")
        ->orderBy("Flight\Model\FlightAssign.id asc")
        ->execute()->toArray();
        
        //echo "<pre>";
        //print_r($flightArr);
        $loadflightArr = [];
        if(count($flightArr)>0){
            foreach($flightArr as $k=>$result){
                $loadflightArr[$k]['flight_no'] = $result['flight_no'];
                $loadflightArr[$k]['station_code'] = $result['station_code'];
                $loadflightArr[$k]['sta'] = $result['sta'];
                $loadflightArr[$k]['assign_id'] = $result['assign_id'];
                if($result['flight_id']){
                    $nextleg = FlightLeg::query()->columns('std')->where("flight_id='".$result['flight_id']."'")->andWhere("id>'".$result['id']."'")->orderby("id asc limit 1")->execute()->toArray();
                    if(isset($nextleg[0]['std']))
                    $loadflightArr[$k]['std'] = $nextleg[0]['std'];
                    else
                    $loadflightArr[$k]['std'] = "";    
                }
            }
        }
        $this->view->loadflight = $loadflightArr;
        $this->helper->title($this->helper->at('Manage Expenses'), true);
    }


    public function flightexpensesAction($id){
        $form = new FlightExpensesForm();
        $getArrivalArr = $this->getArrivalDept($id);
        $fm = FlightExpenses::query()->columns('id')->where("flight_assign_id='".$id."'")->execute()->toArray();
            if(count($fm)>0){
                $model = FlightExpenses::findFirst($fm[0]['id']);
                $model->date = date('d/m/Y H:i',strtotime($model->date));
            }else{
                $model = new FlightExpenses();
            }

        if($this->request->isPost()){
            $post = $this->request->getPost();
            $post['flight_assign_id'] = $id;
            $d = \DateTime::createFromFormat('d/m/Y H:i', $post['date']);
            $post['date'] = $d->format('Y-m-d H:i:s');
            $post['load_master_id'] = $getArrivalArr[0]['load_master_id'];
            if(count($fm)>0){
                $model = FlightExpenses::findFirst($fm[0]['id']);
            }else{
                $model = new FlightExpenses();
            }

            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->save();
                 $this->flash->success('Flight Expenses has been saved.');
            }
        }else{
             $form->setEntity($model);
        }



        
        $this->view->arrivaltime = $getArrivalArr; 
        $this->view->form = $form;
        $this->view->model = $model;
        $this->helper->title($this->helper->at('Manage Flight Expenses'), true);
    }

    private function getArrivalDept($assignId){
        $flightAss = FlightAssign::query()->columns(array('Flight\Model\FlightAssign.leg_id','Flight\Model\FlightAssign.load_master_id','FL.sta','FL.flight_id'))
        ->leftJoin('Flight\Model\FlightLeg',"FL.id=Flight\Model\FlightAssign.leg_id","FL")
        ->where("Flight\Model\FlightAssign.id='".$assignId."'")->execute()->toArray();

        $loadflightArr = [];
        if(count($flightAss)>0){
            foreach($flightAss as $k=>$result){
                $loadflightArr[$k]['sta'] = $result['sta'];
                $loadflightArr[$k]['load_master_id'] = $result['load_master_id'];
                if($result['flight_id']){
                    $nextleg = FlightLeg::query()->columns('std')->where("flight_id='".$result['flight_id']."'")->andWhere("id>'".$result['leg_id']."'")->orderby("id asc limit 1")->execute()->toArray();
                    if(isset($nextleg[0]['std']))
                    $loadflightArr[$k]['std'] = $nextleg[0]['std'];
                    else
                    $loadflightArr[$k]['std'] = "";    
                }
            }
        }
        return $loadflightArr;
    }


    public function viewexpensesAction(){
        

        $allflightArr = FlightExpenses::query()->columns(array("LoadMaster\Model\FlightExpenses.id","LoadMaster\Model\FlightExpenses.date","LoadMaster\Model\FlightExpenses.category","LoadMaster\Model\FlightExpenses.amount","LoadMaster\Model\FlightExpenses.currency","LoadMaster\Model\FlightExpenses.fx_rate","LoadMaster\Model\FlightExpenses.assign_to","LoadMaster\Model\FlightExpenses.status","SD.station_code","LM.first_name"))
        ->leftJoin("Flight\Model\FlightAssign","FA.id=LoadMaster\Model\FlightExpenses.flight_assign_id","FA")
        ->leftJoin("Flight\Model\FlightLeg","FL.id=FA.leg_id","FL")
        ->leftJoin("Station\Model\Station","FL.station_dest_id=SD.id","SD")
        ->leftJoin("LoadMaster\Model\LoadMaster","LM.id=FA.load_master_id","LM")
        ->orderBy("LoadMaster\Model\FlightExpenses.date desc")
        ->execute();
        /*->toArray();*/
        /*echo "<pre>";
        print_r($allflightArr);
        exit;
*/
        $this->view->flightexp = $allflightArr; 
        $this->helper->title($this->helper->at('Expenses'), true);   
    }

    public function updateexpensesAction(){
        if ($this->request->isPost()) {
        $form  = new FlightExpensesForm();
        $post = $this->request->getPost();
        $model = FlightExpenses::findFirst($post['id']);
        $form->bind($post, $model);
            //echo "<pre>"; print_r($post); exit;
            //if ($form->isValid()) {
                
                if ($model->save()) {
                    $id = $model->id;
                    $assign_to = $post['assign_to'];
                    $status = $post['status']==1?'Approved':'Reject';
                    echo json_encode(array('value'=>'success','res'=>array('assign_to'=>$assign_to,'status'=>$status)));   
                } else {
                echo json_encode(array('value' => 'error'));  
                }
            //}
        }
        exit;
    }
}  


