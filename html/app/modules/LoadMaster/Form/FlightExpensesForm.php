<?php

namespace LoadMaster\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use LoadMaster\Model\FlightExpenses;
use Phalcon\Validation\Validator\PresenceOf;

class FlightExpensesForm extends Form
{
    public function initialize()
    {
     
        $this->add(
            (new Text('load_master_id'))
        );

        $this->add(
            (new Text('flight_assign_id'))
        );

        $this->add(
            (new Text('date'))->setLabel('Date')
        );

        
        $this->add(
            (new Select('category', FlightExpenses::$categoryArr,['useEmpty'  =>  false,
            'using'     => array('id', 'category'),
            'required' => false]))
                ->addValidator(new PresenceOf(['message' => 'category can not be empty']))
                ->setLabel('Category')
        );
        $this->add(
            (new Text('amount'))->setLabel('Amount')
        );
        $this->add(
            (new Select('currency',FlightExpenses::$mastercurrency))->setLabel('Currency')
        );
        $this->add(
            (new Text('fx_rate'))->setLabel('FX Rate')
        );
        $this->add(
            (new Text('status'))
        );
        $this->add(
            (new Text('assign_to'))
        );
        
 
    }

}   