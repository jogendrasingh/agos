<?php

namespace LoadMaster\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;

class LoadMasterForm extends Form
{
    public function initialize()
    {
     
        $this->add(
            (new Text('first_name', [
                'required' => true,
            ]))->setLabel('First Name')
        );

        $this->add(
            (new Text('middle_name', [
                'required' => true,
            ]))->setLabel('Middle Name')
        );
        
        $this->add(
            (new Text('last_name', [
                'required' => true,
            ]))->setLabel('Last Name')
        );

         $this->add(
            (new Text('date_of_birth', [
                'required' => true,
            ]))->setLabel('Date Of Birth')
        );

         $this->add(
            (new Text('pan_number', [
                'required' => true,
            ]))->setLabel('PAN Number')
        );
        
        $this->add(
            (new Text('country_of_residence', [
                'required' => true,
            ]))->setLabel('Country of Residence')
        );

        $this->add(
            (new Text('country_of_origin', [
                'required' => true,
            ]))->setLabel('Country of Origin')
        );

        $this->add(
            (new Text('visa', [
                'required' => true,
            ]))->setLabel('Visa')
        );

        $this->add(
            (new Text('country', [
                'required' => true,
            ]))->setLabel('Country')
        );

        $this->add(
            (new Text('issue_date', [
                'required' => true,
            ]))->setLabel('Issue Date')
        );

        $this->add(
            (new Text('expiry_date', [
                'required' => true,
            ]))->setLabel('Expiry Date')
        );

        $this->add(
            (new Text('block_hour_rate', [
                'required' => true,
            ]))->setLabel('Block Hour Rate')
        );  
        $this->add(
            (new Text('flight_allowance', [
                'required' => true,
            ]))->setLabel('Flight Allowance')
        ); 

        $this->add(
            (new Text('daily_allowance_intl', [
                'required' => true,
            ]))->setLabel('Daily Allowance International')
        );  

        $this->add(
            (new Text('daily_allowance_dom', [
                'required' => true,
            ]))->setLabel('Daily Allowance Domestic')
        ); 

        $this->add(new Text('block_cur'));
        $this->add(new Text('flight_allowance_cur'));
        $this->add(new Text('allowance_intl_cur'));
        $this->add(new Text('allowance_dom_cur'));
        $this->add(new Text('admin_id'));

        $this->add(
            (new Check('active'))
                ->setLabel('Active')
        );     

    }



}   