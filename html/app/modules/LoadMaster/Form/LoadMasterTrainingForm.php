<?php

namespace LoadMaster\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;

class LoadMasterTrainingForm extends Form
{
    public function initialize()
    {
     
         $this->add(
            (new Text('load_master_id'))
        );

        $this->add(
            (new Text('training_id'))
        );

        $this->add(
            (new Text('training_date'))
        );

        $this->add(
            (new Text('expiry'))
        );

        
    }

}   