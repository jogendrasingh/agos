<?php

namespace LoadMaster\Model;

use Application\Mvc\Model\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\PresenceOf;
use Application\Localization\Transliterator;

class FlightExpenses extends Model
{

    public function getSource()
    {
        return "flight_expenses";
    }

  
    public $id;
    public $load_master_id;
    public $flight_assign_id;
    public $date;
    public $category;
    public $amount;
    public $currency;
    public $fx_rate;
    public $country;
    public $assign_to;
    public $status;
    public static $categoryArr = [
        'Taxi (Hotel-AP)'   => 'Taxi (Hotel-AP)',
        'Taxi (AP-Hotel)'   => 'Taxi (AP-Hotel)',
        'Taxi (Outsation)'  => 'Taxi (Outsation)',
        'Hotel (Outstation)'=>'Hotel (Outstation)',
        'Misc'              => 'Misc'
        ];

    public static $assignTo = [
        'Reimbursible'   => 'Reimbursible',
        'Expense'   => 'Expense',
        'Others'  => 'Others'
        ];    

    public static $mastercurrency = ['INR'=>'INR','USD'=>'USD','EUR'=>'EUR','USD'=>'USD','RON'=>'RON','SAR'=>'SAR','UGX'=>'UGX','SDG'=>'SDG'];
   
    
    public function initialize()
    {
    }
    
 
    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }
  /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

  /*  public function setDate($date){
        $this->date = $date;
    }

    public function getDate($date){
        return $this->date;
    }*/
    
    


}
