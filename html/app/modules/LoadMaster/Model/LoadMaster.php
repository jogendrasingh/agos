<?php

namespace LoadMaster\Model;

use Application\Mvc\Model\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\PresenceOf;
use Application\Localization\Transliterator;

class LoadMaster extends Model
{

    public function getSource()
    {
        return "load_masters";
    }

  
    public $id;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $date_of_birth;
    public $pan_number;
    public $country_of_residence;
    public $country_of_origin;
    public $visa;
    public $country;
    public $issue_date;
    public $expiry_date;
    public $block_hour_rate;
    public $flight_allowance;
    public $daily_allowance_intl;
    public $daily_allowance_dom;
    public $block_cur;
    public $flight_allowance_cur;
    public $allowance_intl_cur;
    public $allowance_dom_cur;
    public $created_at;
    public $updated_at;
    public $active;
    public $admin_id;
    public function initialize()
    {
    }
    
 
    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }
  /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    public function getActive()
    {
        return $this->active;
    }
    

    public function isActive()
    {
        if ($this->active) {
            return true;
        }
    }
    
     public function setActive($active)
    {
        $this->active = $active;
    }
    
    public function setCheckboxes($post)
    {
        $this->setActive(isset($post['active']) ? 1 : 0);
    }

    public static function getClient(){
        $client = Client::query()->columns(array('id','name'))->where("active='1'")->orderBy("name")->execute();
        $list = [];
        foreach($client as $c){
            $list[$c->id] = $c->name; 
        }
        return $list;
    }
}
