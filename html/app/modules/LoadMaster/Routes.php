<?php

/**
 * Routes
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace LoadMaster;

use Application\Mvc\Router\DefaultRouter;
//use Category\Model\Category;

class Routes
{

     public function init($router)
    {    
    $router->add('/load-master', array(
            'module'     => 'load-master',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('client');
        
        $router->add('/load-master/', array(
            'module'     => 'load-master',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('load-master');
        
        $router->add('/load-master/admin/', array(
            'module'     => 'load-master',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('load-master');
        
        return $router;
    }     

}