<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}load-master/admin" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
        {% if(not(model.id is empty)) %}
            <a href="{{ url.get() }}load-master/admin/delete/{{ model.id }}" class="ui button red">
                <i class="icon trash"></i> Delete
            </a>
        {% endif %}
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <div class="two fields">
            <div class="field">
                {% if model.id=='' %}
                <div class="field">
                       <div><label>Login</label>
                       <input type="text" name="login" id="login">
                       </div> 
                </div>
                <div class="field">
                       <div><label>Password</label>
                       <input type="password" name="password" id="password">
                       </div> 
                </div>
                <div class="field">
                       <div><label>Email</label>
                       <input type="text" name="email" id="email">
                       </div> 
                </div>
                {% endif %}
                {{ form.renderDecorated('first_name') }}
                {{ form.renderDecorated('middle_name') }}
                {{ form.renderDecorated('last_name') }}
                {{ form.renderDecorated('date_of_birth') }}
                {{ form.renderDecorated('pan_number') }}
                {{ form.renderDecorated('country_of_residence') }}
                {{ form.renderDecorated('country_of_origin') }}
            </div>
            <div class="field">
                {{ form.renderDecorated('visa') }}
                {{ form.renderDecorated('country') }}
                {{ form.renderDecorated('issue_date') }}
                {{ form.renderDecorated('expiry_date') }}
                
                <!--{{ form.renderDecorated('block_hour_rate') }}
                {{ form.renderDecorated('flight_allowance') }}
                {{ form.renderDecorated('daily_allowance_intl') }}
                {{ form.renderDecorated('daily_allowance_dom') }}-->
                <!-- <div class="field">
                    <div width="50%"><label></label><input type></div>
                    <div style="float:left"><select><option></option></select></div>
                </div> -->
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('block_hour_rate') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="block_cur">
                            {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.block_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}

                       </select></div> 
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('flight_allowance') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="flight_allowance_cur">
                           {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.flight_allowance_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}
                       </select></div> 
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('daily_allowance_intl') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="allowance_intl_cur">
                           {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.allowance_intl_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}
                       </select></div> 
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('daily_allowance_dom') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="allowance_dom_cur">
                           {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.allowance_dom_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}
                       </select></div> 
                    </div>
                </div>
                {{ form.renderDecorated('active') }}
            </div>
        </div>
    </div>

</form>

<script>
    $('.ui.form').form({
           fields: {
            first_name: {
                identifier: 'first_name',
                rules: [
                    {type: 'empty'}
                ]
            },
            last_name: {
                identifier: 'last_name',
                rules: [
                    {type: 'empty'}
                ]
            },
            date_of_birth: {
                identifier: 'date_of_birth',
                rules: [
                    {type: 'empty'}
                ]
            },
            pan_number: {
                identifier: 'pan_number',
                rules: [
                    {type: 'empty'}
                ]
            },
            country_of_residence: {
                identifier: 'country_of_residence',
                rules: [
                    {type: 'empty'}
                ]
            },
            country_of_origin: {
                identifier: 'country_of_origin',
                rules: [
                    {type: 'empty'}
                ]
            },
            visa: {
                identifier: 'visa',
                rules: [
                    {type: 'empty'}
                ]
            },
            country: {
                identifier: 'country',
                rules: [
                    {type: 'empty'}
                ]
            },
            issue_date: {
                identifier: 'issue_date',
                rules: [
                    {type: 'empty'}
                ]
            },
            expiry_date: {
                identifier: 'expiry_date',
                rules: [
                    {type: 'empty'}
                ]
            },
            block_hour_rate: {
                identifier: 'block_hour_rate',
                rules: [
                    {type: 'empty'}
                ]
            },
            flight_allowance: {
                identifier: 'flight_allowance',
                rules: [
                    {type: 'empty'}
                ]
            },
            daily_allowance_intl: {
                identifier: 'daily_allowance_intl',
                rules: [
                    {type: 'empty'}
                ]
            },
            daily_allowance_dom: {
                identifier: 'daily_allowance_dom',
                rules: [
                    {type: 'empty'}
                ]
            },

    }
 });
    
</script>
{{ javascript_include("/static/js/datepicker/jquery-ui.js") }}
<script>
    $(function () {
        $("#date_of_birth").datepicker();
        $("#issue_date").datepicker();
        $("#expiry_date").datepicker();
    });
</script>