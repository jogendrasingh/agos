<!doctype html>
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_page.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_table.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/extras/TableTools/media/css/TableTools.css" rel="stylesheet" type="text/css">
<!--controls-->
<!-- <div class="ui segment">

    <a href="{{ url.get() }}load-master/admin/add" class="ui button positive">
        <i class="icon plus"></i> Add New
    </a>
    
</div> -->
<!--/end controls-->

<table class="ui table very compact celled" {% if loadflight|length is not '' %} id='userId' {% endif%}>
    <thead>
    <tr>
        <th>Station Code</th>
        <th>Flight No</th>
        <th>STA</th>
        <th>STD</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    {% if loadflight|length is not '' %}
   {% for item in loadflight %}
        <tr>
            <td>{{item['station_code']}}</td>
            <td>{{ item['flight_no']}}</a></td>
            <td>{{ item['sta']}}</a></td>
            <td>{{ item['std']}}</a></td>
            <td><a href="{{url.get()}}load-master/admin/flightexpenses/{{item['assign_id']}}">Expenses</a></td>
        </tr>
    {% endfor %}
    {% else %}
        <tr><td colspan='5'>Sorry, No Data Found.</td></tr>
    {% endif %}
    </tbody>    
</table>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
        $(document).ready( function () {
                $('#userId').dataTable( {
                        "sDom": 'T<"clear">lfrtip',
                        "aaSorting": [],
                        "oTableTools": {
                                "sSwfPath": "{{ url.path() }}vendor/datagrid/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                } );

        } );


</script>