<!doctype html><link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}load-master/admin/expenses" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
        {% if(not(model.id is empty)) %}
            <!-- <a href="{{ url.get() }}load-master/admin/expdelete/{{ model.id }}" class="ui button red">
                <i class="icon trash"></i> Delete
            </a> -->
        {% endif %}
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <div class="two fields">
            <div class="field">
                {{ form.renderDecorated('date') }}
                {{ form.renderDecorated('category') }}
                {{ form.renderDecorated('amount') }}
                {{ form.renderDecorated('currency') }}
                {{ form.renderDecorated('fx_rate') }}
            </div>
            <!-- <div class="field">
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('block_hour_rate') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="block_cur">
                            {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.block_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}

                       </select></div> 
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('flight_allowance') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="flight_allowance_cur">
                           {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.flight_allowance_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}
                       </select></div> 
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('daily_allowance_intl') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="allowance_intl_cur">
                           {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.allowance_intl_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}
                       </select></div> 
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        {{ form.renderDecorated('daily_allowance_dom') }}
                    </div>
                    <div class="field">
                       <div><label>Currency</label>
                       <select name="allowance_dom_cur">
                           {% for item in mastercurrency %}
                            <option value="{{item}}" {% if model.allowance_dom_cur==item %} selected {% endif %}>{{item}}</option>
                            {% endfor %}
                       </select></div> 
                    </div>
                </div>
                {{ form.renderDecorated('active') }}
            </div> -->
        </div>
    </div>

</form>
<script>
    $('.ui.form').form({
           fields: {
            date: {
                identifier: 'date',
                rules: [
                    {type: 'empty'}
                ]
            },
            category: {
                identifier: 'category',
                rules: [
                    {type: 'empty'}
                ]
            },
            amount: {
                identifier: 'amount',
                rules: [
                    {type: 'empty'}
                ]
            },
            currency: {
                identifier: 'currency',
                rules: [
                    {type: 'empty'}
                ]
            },
            fx_rate: {
                identifier: 'fx_rate',
                rules: [
                    {type: 'empty'}
                ]
            },

    }
 });
    
</script>
<link href="{{ url.path() }}static/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">

{{ javascript_include("/static/js/moment.js") }}
{{ javascript_include("/static/js/bootstrap-datetimepicker.js") }}
<script>
    $(function () {
       var mindate ='<?php echo date("m/d/Y H:i",strtotime($arrivaltime[0]['sta']));?>';
       var maxdate ='<?php echo date("m/d/Y H:i",strtotime($arrivaltime[0]['std']));?>';

         $('#date').datetimepicker({format: 'MM/DD/YYYY HH:mm',
            minDate: mindate,
            }).on('dp.show', function(){
  $('#date').data("DateTimePicker").maxDate(maxdate);
});;
    });
</script>