<!doctype html>
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_page.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_table.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/extras/TableTools/media/css/TableTools.css" rel="stylesheet" type="text/css">
<!--controls-->
<div class="ui segment">

    <a href="{{ url.get() }}load-master/admin/add" class="ui button positive">
        <i class="icon plus"></i> Add New
    </a>
    
</div>
<!--/end controls-->

<table class="ui table very compact celled" {% if entries|length is not '' %} id='userId' {% endif%}>
    <thead>
    <tr>
        <th style="width: 100px"></th>
        <th>First Name</th>
        <th>Date Of Birth</th>
        <th>PAN Number</th>
        <th>Visa</th>
        <th>Action</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    {% if entries|length is not '' %}
   {% for item in entries %}
        {% set link = url.get() ~ "load-master/admin/edit/" ~ item.getId() %}
        <tr>
            <td><a href="{{ link }}" class="mini ui icon button"><i class="icon edit"></i>
                </a>
            </td>
            <td>{{item.first_name}}</td>
            <td>{{ item.date_of_birth}}</a></td>
            <td>{{ item.pan_number}}</a></td>
            <td>{{ item.visa}}</a></td>
            <td><a href="{{url.get()}}load-master/admin/training/{{item.id}}">Training</a></td>
            <td>{% if item.getActive() %}<i class="icon checkmark green"></i>{% endif %}</td>
        </tr>
    {% endfor %}
    {% else %}
        <tr><td colspan='5'>Sorry, No Data Found.</td></tr>
    {% endif %}
    </tbody>    
</table>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
        $(document).ready( function () {
                $('#userId').dataTable( {
                        "sDom": 'T<"clear">lfrtip',
                        "aaSorting": [],
                        "oTableTools": {
                                "sSwfPath": "{{ url.path() }}vendor/datagrid/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                } );

        } );


</script>