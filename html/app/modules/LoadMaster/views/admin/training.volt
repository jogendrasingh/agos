<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}load-master/admin" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
       
    </div>
    <!--end controls-->
	<div class="ui segment">
	    <table class="ui table very compact celled">
	    	<thead>
	    	<tr>
		        <th>Category</th>
		        <th>Training</th>
		        <th>Training Date</th>
		        <th>Expiry</th>
	        </tr>
	    </thead>
	    <tbody>
	    	 {% for k,item in training %}
	    	<tr class="field">
	    		<td>{{item.category}}</td>
	    		<td>{{item.training}}</td>
	    		
	    		<?php if(isset($trainingmodel) && !empty($trainingmodel)) {
	    			

	    			if($trainingmodel[$k]['training_date']=="" || $trainingmodel[$k]['training_date']=="0000-00-00"){
	    			$tr_date = "";
	    			}else{
	    			$tr_date = date('m/d/Y',strtotime($trainingmodel[$k]['training_date']));
	    			}
	    			
	    			
	    			if($trainingmodel[$k]['expiry']=="" || $trainingmodel[$k]['expiry']=="0000-00-00"){
	    			$expiry = "";
	    			}else{
	    			$expiry =  date('m/d/Y',strtotime($trainingmodel[$k]['expiry']));
	    			}

	    		}else{
	    			$tr_date = "";
	    			$expiry  = "";
	    		 }?>


	    		<td><input type="text" class="date" name="training_date[{{item.id}}]" id="training_id_{{item.id}}" value={{tr_date}}></td>
	    		<td><input type="text" class="date" name="expiry[{{item.id}}]" id="expiry_id_{{item.id}}" value="{{expiry}}"></td>
	    	</tr>
	    	 {% endfor %}
	    </tbody>
	    </table>
	</div>
</form>

{{ javascript_include("/static/js/datepicker/jquery-ui.js") }}
<script>
    $('.ui.form').form({

 });
    
</script>
<script>
    $(function () {
        $(".date").datepicker();
        
    });
</script>



