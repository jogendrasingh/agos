<!doctype html>
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_page.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_table.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/extras/TableTools/media/css/TableTools.css" rel="stylesheet" type="text/css">
<!--controls-->
<div class="ui segment">

    <a href="{{ url.get() }}load-master/admin/add" class="ui button positive">
        <i class="icon plus"></i> Add New
    </a>
    
</div>
<!--/end controls-->

<table class="ui table very compact celled" {% if flightexp|length is not '' %} id='userId' {% endif%}>
    <thead>
    <tr>
        <th>Station</th>
        <th>Load Master</th>
        <th>Date</th>
        <th>Category</th>
        <th>Amount</th>
        <th>Currency</th>
        <th>Fx Rate</th>
        <th>Assign To</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    {% if flightexp|length is not '' %}
   {% for item in flightexp %}
        <tr>
            <td>{{item.station_code}}</td>
            <td>{{ item.first_name}}</td>
            <td>{{ item.date}}</td>
            <td>{{ item.category}}</td>
            <td>{{ item.amount}}</td>
            <td>{{ item.currency}}</td>
            <td>{{ item.fx_rate}}</td>
            <td id=assign_to_{{item.id}}>
            {% if item.assign_to is not '' %}
            {{ item.assign_to}}
            {% else%}
            <select name="assign_to" id=assign_{{item.id}}>
                    <option value="">Select</option>
                    <option value="Reimbursible">Reimbursible</option>
                    <option value="Expense">Expense</option>
                    <option value="Others">Others</option>
                </select>
            {% endif%}
            </td>
            <td id="status_{{item.id}}">
            {% if item.status is not ''%}
            {% if item.status==1%}Approved{%else%}Rejected{%endif%}
            {% else%}
            
            <button name="approved" onclick="savecurrent({{item.id}},'approve')">A</button> | 
            <button name="reject" onclick="savecurrent({{item.id}},'reject')">R</button>
            {% endif%}
            </td>
        </tr>
    {% endfor %}
    {% else %}
        <tr><td colspan='5'>Sorry, No Data Found.</td></tr>
    {% endif %}
    </tbody>    
</table>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
        $(document).ready( function () {
                $('#userId').dataTable( {
                        "sDom": 'T<"clear">lfrtip',
                        "aaSorting": [],
                        "oTableTools": {
                                "sSwfPath": "{{ url.path() }}vendor/datagrid/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                } );

        } );


</script>

<script>
    
 function savecurrent(id,type){
 if(id){
    var flag = true;
    var status="";
    if(type=="approve"){
        status=1;
    var assign_to = $("#assign_"+id).val();
        if(assign_to==""){
            flag = false;
            alert("Please select assign to");
            return false;
                
        }    
    }else{
        status=0;
        flag = false;
        return confirm("Are you sure, you want to reject");
        
    }
    if(flag ==true){
        $.ajax({
        url: '{{ url.get() }}load-master/admin/updateexpenses',
        data: {id:id,assign_to:assign_to,status:status},
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            if (response.value == 'success') {
                
                $("#assign_to_"+id).html(response.res.assign_to);
                $("#status_"+id).html(response.res.status);
                           }
        },
        error: function (request, error) {

             alert('Failed!');
        }
    });
    }

   
    }
}



</script>