<?php
namespace Station\Controller;
use YonaCMS\Plugin\Mail;
use Application\Mvc\Controller;
use Station\Model\Station;
use Station\Form\StationForm;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AdminController extends Controller {

    public function initialize() {
        $this->setAdminEnvironment();
        $this->helper->activeMenu()->setActive('station');

    }

    public function indexAction() {

        $this->view->entries = Station::find([
            "order" => "id DESC"
        ]);
        
        $this->helper->title($this->helper->at('Manage Station'), true);
    }

    public function addAction() {

        $this->view->pick(['admin/edit']);
        $model = new Station();
        $form = new StationForm();
        
        if ($this->request->isPost()) {
            $model = new Station();
            $post = $this->request->getPost();
        
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $this->flash->success($this->helper->at('Station created', ['airport_name' => $model->airport_name]));
                    $this->redirect($this->url->get() . 'station/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->submitButton = $this->helper->at('Add New');

        $this->helper->title($this->helper->at('Manage Station'), true);
    
    }

    public function editAction($id) {
        $model = Station::findFirst($id);
        if (!$model) {
            $this->redirect($this->url->get() . 'station/admin');
        }

        $form = new StationForm();

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save() == true) {
                    $this->flash->success('Station <b>' . $model->airport_name . '</b> has been saved');
                    return $this->redirect($this->url->get() . 'station/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        } else {
            $form->setEntity($model);
        }

        $this->view->submitButton = $this->helper->at('Save');
        $this->view->form = $form;
        $this->view->model = $model;

        $this->helper->title($this->helper->at('Manage Stations'), true);
    }

    
    public function deleteAction($id) {
        $model = Station::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'station/admin');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('Deleting station <b>' . $model->airport_name . '</b>');
            return $this->redirect($this->url->get() . 'station/admin');
        }

        $this->view->model = $model;

        $this->helper->title($this->helper->at('Delete Station'), true);
    }

}
