<?php

namespace Block\Controller;

use Application\Mvc\Controller;
use Block\Model\Block;
use Phalcon\Mvc\Dispatcher\Exception;

class IndexController extends Controller
{

    public function indexAction()
    {
        $slug = $this->dispatcher->getParam('slug', 'string');
        $page = Block::findCachedBySlug($slug);
        if (!$page) {
            throw new Exception("Block '$slug.html' not found");
        }

        $this->helper->title()->append($page->getMeta_title());
        $this->helper->meta()->set('description', $page->getMeta_description());
        $this->helper->meta()->set('keywords', $page->getMeta_keywords());

        $this->view->page = $page;
    }

    public function contactsAction()
    {
        $page = Block::findCachedBySlug('contacts');
        if (!$page) {
            throw new Exception("Block 'contacts' not found");
        }

        $this->helper->title()->append($page->getMeta_title());
        $this->helper->meta()->set('description', $page->getMeta_description());
        $this->helper->meta()->set('keywords', $page->getMeta_keywords());
        $this->view->page = $page;

        $this->helper->menu->setActive('contacts');
    }

} 