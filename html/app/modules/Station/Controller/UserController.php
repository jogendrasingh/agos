<?php

namespace Audit\Controller;

ini_set('max_execution_time', 600);
use Application\Mvc\Controller;
use Audit\Model\Audit;
use Audit\Model\AuditInspection;
use Audit\Model\AuditMaster;
use Audit\Form\AuditInspectionForm;
use Audit\Form\AuditlogForm;
use Audit\Model\Auditlog;
use Audit\Model\AuditReason;
use Audit\Form\AuditReasonForm;
use User\Model\User;
use Audit\Model\AuditQuantity;
use Audit\Form\AuditQuantityForm;
use YonaCMS\Plugin\Mail;
use Audit\Model\AuditFieldMapper;
use Audit\Model\FieldMapper;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use PhpOffice\PHPWord;
use Audit\Model\AuditQuality;
use Audit\Form\AuditQualityForm;

class UserController extends Controller
{
   
    public function initialize()
    {
        $this->logApprove  =  $_SERVER['DOCUMENT_ROOT']."/../../logApprove.txt";
        $this->logApproveError = $_SERVER['DOCUMENT_ROOT']."/../../logApproveError.txt";
        $this->logInspection = $_SERVER['DOCUMENT_ROOT']."/../../logInspection.txt";

        $this->view->languages_disabled = true;

        $this->year = 2016;
    }

    public function indexAction()
    {
        

        $auth = $this->session->get('auth');

        
        if ($this->request->hasQuery('page')) {
            $currentPage = $this->request->getQuery('page');
        } else {
            $currentPage = '';
        }

        if (!empty($_GET['type'])) {
            $approveType = $_GET['type'];
        } else {
            if ($auth->role == 'maker') {
                $approveType = 0;
            } elseif ($auth->role == 'checker') {
                $approveType = 1;
            } elseif ($auth->role == 'bank') {
                $approveType = 2;
            }
        }

        $warehouse_condition = '1=1';
        $from_condition = '1=1';
        $to_condition = '1=1';
        $auditmonth_condition='1=1';

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if (!empty($post['warehouse_code'])) {
                $warehouse_condition = " Audit\Model\AuditInspection.warehouse_code = '".$post['warehouse_code']."'";
                $this->view->setVar('warehouse_code', $post['warehouse_code']);
            }

            if (!empty($post['from_date'])) {
                $from = date('Y-m-d', strtotime($post['from_date']));
                $from_condition = "  date(Audit\Model\AuditInspection.warehouse_visit_date) >='".$from."'";
                $this->view->setVar('from_date', $post['from_date']);
            }
            if (!empty($post['to_date'])) {
                $to = date('Y-m-d', strtotime($post['to_date']));
                $to_condition = "  date(Audit\Model\AuditInspection.warehouse_visit_date) <='".$to."'";
                $this->view->setVar('to_date', $post['to_date']);
            }

            if(!empty($post['audit_month'])){
                $audit_month = $post['audit_year'].'-'.$post['audit_month'];
                $auditmonth_condition = "  Audit\Model\AuditInspection.audit_month ='".$audit_month."'";
                $this->view->setVar('amonth', $post['audit_month']);
                $this->view->setVar('ayear', $post['audit_year']);

            }
        }



        $makerCondition = '1=1';    $countmakerCondition='1=1';
        $checkerCondition = '1=1';
        $bankCondition = '1=1';
        
        if ($auth->role == 'maker') {
            
            $makerCondition = "Audit\Model\AuditInspection.maker_id='".$auth->id."' and AM.status='1'";
            $countmakerCondition ="AM.status='1' And Audit\Model\AuditInspection.maker_id='".$auth->id."'";

        } elseif ($auth->role == 'checker') {
            $makerList = User::getMakerByCheckerId($auth->id);
            $condition = "('-1')";
            if (count($makerList) > 0) {
                $condition = '(';
                $i = 0;
                foreach ($makerList as $robot) {
                    if ($i == 0) {
                        $condition .= $robot->id;
                    } else {
                        $condition .= ','.$robot->id;
                    }
                    ++$i;
                }
                $condition .= ')';
            }

            $checkerCondition = 'Audit\Model\AuditInspection.maker_id in '.$condition." and Audit\Model\AuditInspection.approved!='0'";

        } elseif ($auth->role == 'bank') {

            $userState = User::findFirst(array('columns'=>'state',$auth->id,))->toArray();
            $bankCon = "";
            if($userState['state']){
                $bankCon =" AND AM.state IN ('".str_replace(",", "','", $userState['state'])."')";
            }
            $bankCondition = "Audit\Model\AuditInspection.approved='2'".$bankCon."";
        }

        /* 6 Month Date */
        $presixMonth = (new \DateTime())->sub(new \DateInterval('P6M'))->format('Y-m-d');
        /* 6 Month DATE */

        $auditInspObject = AuditInspection::query()->columns(array("Audit\Model\AuditInspection.id","Audit\Model\AuditInspection.warehouse_code",'Audit\Model\AuditInspection.full_address','Audit\Model\AuditInspection.approved','Audit\Model\AuditInspection.warehouse_visit_date','Audit\Model\AuditInspection.maker_id','Audit\Model\AuditInspection.active','Audit\Model\AuditInspection.audit_month','User.name'))
                ->leftJoin("Audit\Model\AuditMaster","AM.audit_id=Audit\Model\AuditInspection.audit_id AND AM.warehouse_code=Audit\Model\AuditInspection.warehouse_code","AM")
                ->leftJoin("User\Model\User","User.id=Audit\Model\AuditInspection.maker_id","User")
                ->where($makerCondition)
                ->andWhere($checkerCondition)
                ->andWhere($bankCondition)
                ->andWhere($warehouse_condition)
                ->andWhere($from_condition)
                ->andWhere($to_condition)
                ->andWhere($auditmonth_condition)
                ->andWhere("Audit\Model\AuditInspection.approved='" . $approveType . "'")
                ->andWhere("Audit\Model\AuditInspection.audit_id!='0'")
                ->andWhere("Audit\Model\AuditInspection.created_at > '".$presixMonth."'")
                ->groupBy(array('AM.audit_id','AM.warehouse_code'))
                ->orderBy('Audit\Model\AuditInspection.id desc')
                ->execute();
                
            
        /* COUNT AUDIT STATUS */
        $countAuditInsp = AuditInspection::query()->columns(array("Audit\Model\AuditInspection.approved","count(distinct(Audit\Model\AuditInspection.audit_id)) as countApproved"))
        ->innerJoin("Audit\Model\AuditMaster","AM.audit_id=Audit\Model\AuditInspection.audit_id","AM")
        ->where("Audit\Model\AuditInspection.audit_id!='0'")
        ->andWhere($warehouse_condition)
        ->andWhere($from_condition)
        ->andWhere($to_condition)
        ->andWhere($countmakerCondition)
        ->andWhere($checkerCondition)
        ->andWhere($bankCondition)
        ->andWhere($auditmonth_condition)
        ->groupBy("Audit\Model\AuditInspection.approved")
        ->execute()->toArray();
        $countAudit = array('pending'=>0,'submitted'=>0,'approved'=>0,'rejected'=>0);
        if(count($countAuditInsp)>0){
            foreach($countAuditInsp as $countVal){
                if($countVal['approved']==0){  $countAudit['pending'] = $countVal['countApproved']; }
                if($countVal['approved']==1){  $countAudit['submitted'] = $countVal['countApproved']; }
                if($countVal['approved']==2){  $countAudit['approved'] = $countVal['countApproved']; }
                if($countVal['approved']==3){  $countAudit['rejected'] = $countVal['countApproved']; }
            }
        }
        $this->view->countAudit = $countAudit;
        /* END COUNT AUDIT STATUS */
        $label = array('pclass'=>'unactive','sclass'=>'unactive','aclass'=>'unactive','rclass'=>'unactive');
        
        if($approveType==0){ $label['pclass'] = "active"; $label['label'] = "Pending";}
        if($approveType==1){ $label['sclass'] = "active"; if($auth->role=='maker') {$label['label'] = "Submitted";}else{$label['label'] = "Pending";}}
        if($approveType==2){ $label['aclass'] = "active"; $label['label'] = "Approved";}
        if($approveType==3){ $label['rclass'] = "active"; $label['label'] = "Rejected";}  
        
        $this->view->type = $approveType;
        $this->view->label=$label;      

        
        $model = new AuditInspection();
        $this->view->monthArray = $model->monthArray;

        for($y=date('Y'); $y>=$this->year;$y--){
            $yearArray[$y] = $y;               
        }

        $this->view->yearArray = $yearArray;

        /*echo "<pre>";
        print_r($label);
        exit;*/
        if (!empty($auditInspObject)) {
            $paginator = new PaginatorModel(
                    array(
                'data' => $auditInspObject,
                'limit' => 50,
                'page' => $currentPage,
                    )
            );
            $entries = $paginator->getPaginate();
            $this->view->auditInspObject = $entries;
            
        }
    }

    public function viewAction($id)
    {
         
        $auth = $this->session->get('auth');
        $makerList = User::getMakerByCheckerId($auth->id);
        $makerIds = array();
        foreach ($makerList as $mkVal) {
            $makerIds[] = $mkVal->id;
        }

        $model = AuditInspection::findFirst($id);
        

        if ($auth->role == 'maker') {
            if (!$model || ($auth->id != $model->maker_id)) {
                $this->redirect($this->url->get().'audit/user/index');
            }
        } elseif ($auth->role == 'checker') {
            if (!$model || (!in_array($model->maker_id, $makerIds))) {
                $this->redirect($this->url->get().'audit/user/index');
            }
        }
        if ($model->id) {
            $staticData = $model->staticData();
            $this->view->staticData = $staticData;
            //'state', 'sol_id', 'location','wh_address', 'type_of_wh', 'warehouse_location','col_mgr_name', 'qty_unit'
            $qualityArray = AuditMaster::query()
            ->columns(array('id', 'receipt', 'commodity_name','bal_no_bags','os_quantity'))
            ->where("audit_id='".$model->audit_id."'")
            ->andWhere("warehouse_code = '".$model->warehouse_code."'")
            ->execute()->toArray();
            $this->view->qualityData = $qualityArray;

            $dbdata = array();
            $quantityArray = AuditQuantity::query()->where("audit_inspection_id='".$model->id."'")->execute()->toArray();
            if (count($quantityArray) > 0) {
                $dbdata = $quantityArray;
            }
            $this->view->dbdata = $dbdata;

            $qData = array();
            $qualityArray = AuditQuality::query()->where("audit_inspection_id='".$model->id."'")->execute()->toArray();
            if (count($qualityArray) > 0) {
                $qData = $qualityArray;
            }
            $this->view->qData = $qData;




            $errorLog = Auditlog::query()
                    ->columns(array('Audit\Model\Auditlog.comment', 'Audit\Model\Auditlog.created_at', 'user.name'))
                    ->innerJoin('User\Model\User', 'user.id = Audit\Model\Auditlog.checker_id', 'user')
                    ->where("Audit\Model\Auditlog.audit_inspection_id='".$model->id."'")
                    ->orderBy('Audit\Model\Auditlog.id DESC')
                    ->execute();
            $this->view->errorLog = $errorLog;

            $auditReasonArr = AuditReason::query()->columns(array('field', 'reason'))->where("audit_inspection_id = '".$model->id."'")->execute()->toArray();

            $reasonData = array();
            if (count($auditReasonArr) > 0) {
                foreach ($auditReasonArr as $resdata) {
                    $reasonData[$resdata['field']] = $resdata['reason'];
                }
            }
            $this->view->reasonData = $reasonData;
        }

        $this->view->model = $model;
        $this->view->monthArray = $model->monthArray;

        for($y=$this->year; $y<=date('Y');$y++){
            $yearArray[$y] = $y;               
        }

        $this->view->yearArray = $yearArray;
    }

    public function listAction()
    {
        $auth = $this->session->get('auth');
        if ($auth->role != 'maker') {
            return $this->redirect($this->url->get().'audit/user/index');
        }
        $auditObject = Audit::query()
                ->columns(array('Audit\Model\Audit.id', 'Audit\Model\Audit.warehouse_code', 'AM.customer_name', 'AI.approved', 'AI.created_at'))
                ->innerJoin("Audit\Model\AuditMaster", "AM.audit_id = Audit\Model\Audit.id", 'AM')
                ->leftJoin("Audit\Model\AuditInspection", "AI.audit_id=Audit\Model\Audit.id", 'AI')
                ->where("Audit\Model\Audit.maker_id = ".$auth->id.'')
                ->andWhere("Audit\Model\Audit.active = '1'")
                ->andWhere("AM.status='1'")
                ->andWhere('AI.warehouse_code is NULL')
                ->groupBy('AM.warehouse_code')
                ->execute();
        $this->view->auditObject = $auditObject;
    }

    public function addAction($id = null)
    {
        $audit_id = (int) $id;
        $audit = new AuditInspection();
        $audit2 = new AuditInspection();
        $staticData = $audit->staticData();
        $form = new AuditInspectionForm();
        $inspectionField = $audit2->toArray();

        if ($audit_id) {
            $auditDetail = Audit::findfirst($audit_id);
            
            /* GET LAST PREVIOUS WAREHOUSE AUDIT */
            $prevAuditDetail = AuditInspection::query()->where("warehouse_code='".$auditDetail->warehouse_code."'")->andWhere("approved='2'")->orderBy("id desc limit 1")->execute();
            if(count($prevAuditDetail)>0){
                $audit2 = $prevAuditDetail[0];
                $audit2->disclaimer=0;
                $audit2->warehouse_photo='';
                $audit2->visitor_register_photo='';
                $audit2->stock_register_photo='';
                $audit2->stack_card_photo='';
                $audit2->stack_arrangement_photo='';
                $audit2->commodity_photo='';
                $audit2->extra_photo1='';
                $audit2->extra_photo2='';

            $remarkData = array();
            $AuditRemark = AuditReason::find("audit_inspection_id = '".$audit2->id."'")->toArray();

            if (count($AuditRemark) > 0) {
                foreach ($AuditRemark as $data) {
                    $remarkData[$data['field']] = $data['reason'];
                }
            }

            /* CHECK REMARK VALUE */
            //$inspectionField = array();
            //$inspectionField = $audit2->toArray();
            if (count($inspectionField) > 0) {
                foreach ($inspectionField as $field => $value) {
                    if (array_key_exists($field, $remarkData)) {
                        $finalRemarkData[$field] = $remarkData[$field];
                    } else {
                        $finalRemarkData[$field] = '';
                    }
                }
            }
           

            }
            /* END LAST PREVIOUS WAREHOUSE AUDIT */
        }

        $this->view->model = $audit2;
        $this->view->staticData = $staticData;

        $this->view->monthAudit  = $audit->monthArray2;
        //$this->view->remarkData = $audit->toArray();
        if(empty($finalRemarkData)){
            $this->view->remarkData = $inspectionField; 
        }else{
         $this->view->remarkData = $finalRemarkData;   
        }
        //$this->view->remarkData = $finalRemarkData;

        if ($audit_id) {
            
            $this->view->pick(['user/edit']);

            $auditPrefilledObject = AuditMaster::query()
                    ->columns(array('state','location', 'warehouse_code','wh_name', 'wh_address'))
                    ->where("warehouse_code='".$auditDetail->warehouse_code."'")
                    ->andWhere("audit_id='".$auditDetail->id."'")
                    ->execute();

            $resData = array();
            if (count($auditPrefilledObject) > 0) {
                foreach ($auditPrefilledObject as $data) {
                    $resData['state'] = $data->state;
                    $resData['location'] = $data->location;
                    $resData['warehouse_code'] = $data->warehouse_code;
                    $resData['full_address'] = $data->wh_address;
                    $resData['warehouse_name'] = $data->wh_name;
                }
                $this->view->auditPrefilledData = $resData;
            }
        }

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $auth = $this->session->get('auth');
            if (!empty($post['warehouse_visit_date'])) {
                $post['warehouse_visit_date'] = date_format(new \Datetime($post['warehouse_visit_date']), 'Y-m-d');
            }

            $post['audit_id'] = $audit_id;
            $post['maker_id'] = $auth->id;
            $post['approved'] = '0';

            $form->bind($post, $audit);
            if ($form->isValid()) {
                $audit->save();
                $inspection_id = $audit->id;
                if ($inspection_id) {
                    error_log(date("d-m-Y H:i:s")." Audit Inspection Start  ".json_encode($auth)." \n", 3, $this->logInspection);
                    error_log(date("d-m-Y H:i:s")." Audit Inspection  ".json_encode($post)." \n", 3, $this->logInspection);
                    if (isset($post['remark']) && !empty($post['remark'])) {
                        if (count($post['remark']) > 0) {
                            $remarkForm = new AuditReasonForm();

                            foreach ($post['remark'] as $key => $value) {
                                $remarkData['audit_inspection_id'] = $inspection_id;
                                $remarkData['field'] = $key;
                                $remarkData['reason'] = $value;
                                $remarkModel = new AuditReason();
                                $remarkForm->bind($remarkData, $remarkModel);
                                $remarkModel->save();
                            }
                        }
                    }

                    $this->redirect($this->url->get().'audit/user/quantity/'.$inspection_id);
                } else {
                    echo 'Sorry, the following problems were generated: ';
                    foreach ($audit->getMessages() as $message) {
                        echo $message->getMessage(), '<br/>';
                        die;
                    }
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }
    //}
    }

    public function editAction($id)
    {
        $auth = $this->session->get('auth');
        $model = AuditInspection::findFirst($id);

        $this->view->pick(['user/edit']);
        if (!empty($model)) {
            $staticData = $model->staticData();
            $this->view->staticData = $staticData;

            $resData = array();
            $resData['state'] = $model->state;
            $resData['location'] = $model->location;
            $resData['warehouse_code'] = $model->warehouse_code;
            $resData['full_address'] = $model->full_address;
            $resData['warehouse_name'] = $model->warehouse_name;
        }

        if (!empty($model)) {
            $remarkData = array();
            $AuditRemark = AuditReason::find("audit_inspection_id = '".$model->id."'")->toArray();

            if (count($AuditRemark) > 0) {
                foreach ($AuditRemark as $data) {
                    $remarkData[$data['field']] = $data['reason'];
                }
            }

            /* CHECK REMARK VALUE */
            $inspectionField = array();
            $inspectionField = $model->toArray();
            if (count($inspectionField) > 0) {
                foreach ($inspectionField as $field => $value) {
                    if (array_key_exists($field, $remarkData)) {
                        $finalRemarkData[$field] = $remarkData[$field];
                    } else {
                        $finalRemarkData[$field] = '';
                    }
                }
            }
            $this->view->remarkData = $finalRemarkData;
            /* END CHECK REMARK VALUE */
        }

        $form = new AuditInspectionForm();


        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            if (!empty($post['warehouse_visit_date'])) {
                $post['warehouse_visit_date'] = date_format(new \Datetime($post['warehouse_visit_date']), 'Y-m-d');
            }

            if($post['compromised_lock_key']==6){
                $colArr = $this->hidemodelfields();
                 foreach($colArr as $val){
                    $post[$val]="";
                 }
            }
            
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->save();
                $inspection_id = $model->id;
                $completeArr = array();
                if ($inspection_id) {
                    if (isset($post['remark']) && !empty($post['remark'])) {
                        if (count($post['remark']) > 0) {
                            $remarkForm = new AuditReasonForm();

                            foreach ($post['remark'] as $key => $value) {
                                $remarkData['audit_inspection_id'] = $inspection_id;
                                $remarkData['field'] = $key;
                                $remarkData['reason'] = $value;
                                $remarkData['created_at'] = date('Y-m-d');

                                $reasonArr = AuditReason::query()->columns(array('id'))
                                    ->where("audit_inspection_id='".$inspection_id."'")
                                    ->andWhere("field='".$key."'")
                                    ->execute();

                                if (isset($reasonArr) && count($reasonArr) > 0) {
                                    foreach ($reasonArr as $resData) {
                                        $remarkModel = AuditReason::findfirst($resData->id);
                                    }
                                } else {
                                    $remarkModel = new AuditReason();
                                }
                                $remarkForm->bind($remarkData, $remarkModel);
                                $remarkModel->save();
                            }
                        }
                    }
                    /* DELETE OLD Enteries*/
                    // $oldReasonData = AuditReason::query()->columns(array('id','field'))->where("audit_inspection_id = '".$inspection_id."'")->execute()->toArray();
                    // if(count($oldReasonData)>0){
                    //     foreach($oldReasonData as $data){
                    //         if(!in_array($data['field'],$completeArr)){
                    //             $deleteModel = AuditReason::findFirst($data['id']);
                    //             $deleteModel->delete();
                    //         }
                    //     }
                    // }

                    /* End */
                    $this->redirect($this->url->get().'audit/user/quantity/'.$inspection_id);
                } else {
                    echo 'Sorry, the following problems were generated: ';
                    foreach ($model->getMessages() as $message) {
                        echo $message->getMessage(), '<br/>';
                        die;
                    }
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
        $this->view->auditPrefilledData = $resData;
        $this->view->model = $model;
        $this->view->monthAudit  = $model->monthArray2;
        
    }

    public function deleteAction($id)
    {
        $auth = $this->session->get('auth');
        $form = new AuditInspectionForm();
        $model = AuditInspection::findFirst($id);
        $post['active'] = 0;
        $form->bind($post, $model);
        $model->save();

        return $this->redirect($this->url->get().'audit/user/index');
    }

    public function uploadAction()
    {
        $this->view->disable();
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if ($this->request->hasFiles() == true) {
            $path = ROOT.'/audit/images/';
            foreach ($this->request->getUploadedFiles() as $file) {
                $fileName = date('Y-m-d').$file->getName();
                $tempName = $file->getTempName();
                if ($file->moveTo($path.$fileName)) {
                    echo '<img width="120px" src="'.'../../images/'.$fileName.'" alt="Thumbnail">'
                .'<input type="hidden" name="audit_image" value='.$fileName.'>';
                }
            }
        }
    }

    public function approveAction()
    {
        $auth = $this->session->get('auth');
        $form = new AuditInspectionForm();
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $model = AuditInspection::findFirst($post['id']);
            $post['updated_at'] = date('Y-m-d H:i:s');
            $post['approved'] = 1;
            $form->bind($post, $model);
            if ($form->isValid()) {
                if ($model->save() == true) {
                    $mailId = User::getEmailByUserId($auth->parent_id);
                    $to = $mailId['email'];
                    $name = $mailId['name'];
                    $subject = $model->warehouse_code.' Audit Assigned';
                    $body = '<html><body>';
                    $body .= 'Dear '.$name.',';
                    $body .= '<p> Warehouse Code - '.$model->warehouse_code.' , Location - '.$model->location.' has been assigned.</p>';
                    $body .= 'Regards <br/> '.$auth->name;
                    $body .= '</body></html>';
                    $mail = new Mail();
                    $mail->sendMail($to, $name, $subject, $body);
                    $this->flash->success('Audit has been assigned');
                    echo json_encode(array('value' => 'Success'));
                    die;
                } else {
                    echo json_encode(array('value' => 'Error'));
                    die;
                }
            } else {
                echo json_encode(array('value' => 'Error'));
                die;
            }
        } else {
            echo json_encode(array('value' => 'Error'));
            die;
        }
    }

 
    public function checkAction()
    {

        if($this->request->getQuery('action')!="log"){
            
            error_log(date("d-m-Y H:i:s")." Starting  \n", 3, $this->logApprove);
            
        }
        $post['updated_at'] = date('Y-m-d H:i:s');


        $responseMessage = "Error";

        $auth = $this->session->get('auth');


 
        $form = new AuditInspectionForm();
        if ($this->request->isPost()) {

            
            $post = $this->request->getPost();

            

            $model = AuditInspection::findFirst($post['id']);

            if($this->request->getQuery('action')=="log"){
                

                if($this->request->getQuery('type')!=""){
                    $logType = $this->request->getQuery('type');
                }
                else{

                   $logType = "Approve";
                }

                error_log(date("d-m-Y H:i:s")." Error - ".$logType." ".$post['id']." ".$model->warehouse_code." by ".$auth->name." \n", 3, $file);

                error_log(date("d-m-Y H:i:s")." Request ".$post['request']." \n", 3, $this->logApproveError);
                error_log(date("d-m-Y H:i:s")." Message ".$post['error']." \n", 3, $this->logApproveError);
                 echo "Here";
                 exit;
            }


            error_log(date("d-m-Y H:i:s")." Processing ".$post['id']." ".$model->warehouse_code." by ".$auth->name." \n", 3, $this->logApprove);
            $post['updated_at'] = date('Y-m-d H:i:s');
            if ($auth->role == 'checker' && $model->approved == '1') {
                $post['approved'] = 2;
                $post['approved_by'] = $auth->id;
                $post['audit_month'] = $post['audit_year'].'-'.$post['audit_month'];
                unset($post['audit_year']);
            } else {
                echo json_encode(array('value' => 'Error'));
                die;
            }
            
            $form->bind($post, $model);
            if ($form->isValid()) {
                if ($model->save() == true) {
                    $mail = new Mail();

                    $subject = $model->warehouse_code.' audit report has been submitted by Arya Collateral';
                    
                    $stateArr = Audit::query()->columns(['id','state'])->where("warehouse_code = '".$model->warehouse_code."'")->andWhere("active='1'")->orderBy('id desc limit 1')->execute()->toArray();
                    $state = '';
                    if (count($stateArr) > 0) {
                        $state = $stateArr[0]['state'];
                    }
                    
                    error_log(date("d-m-Y H:i:s")." warehouse ID ".$stateArr[0]['id']."  State ".$state."\n", 3, $this->logApprove);
                    
                    if($state!=''){
                    
                    $bankUserArray = User::query()->columns(array('id', 'name', 'email'))->where("role='bank'")->andWhere("active='1'")->andWhere("automailer='1'")->andWhere("find_in_set('".trim($state)."',state)")->orderBy('id')->execute()->toArray();
                    if (count($bankUserArray) > 0) {
                        $sentTo=array();
                        error_log(date("d-m-Y H:i:s")." Staring Email\n", 3, $this->logApprove);
                        
                        $staticData = $model->staticData();

                        /* GET DISCREPANCY COMMENTS */
                        $fieldMapper = FieldMapper::query()->where("active='1'")->execute()->toArray();

                        $audit = $model->toArray();

                        $textCommentArray = ['warehouse_related_remarks','commodity_related_remarks','collateral_manager_remarks','other_comments'];

                        foreach($fieldMapper as $mapper){

                            if($mapper['data_field']=="compromised_lock_key"){
                                $audit[$mapper['data_field']] = $staticData['lockKey'][$audit['compromised_lock_key']];

                               if($audit['compromised_lock_key']!='With CM Agency'){
                              $discrepancy_type_detail[] = 'Custody Of Lock N key '.$audit['compromised_lock_key'];
                            }

                            }
                            
                            if(!empty($mapper['field_value']) && (array_key_exists($mapper['data_field'],$audit)) && $mapper['field_value']==$audit[$mapper['data_field']]){
                                
                                $discrepancy_type_detail[] = $mapper['discrepancy_value'];
                                
                            }

                            if(in_array($mapper['data_field'],$textCommentArray) && !empty($audit[$mapper['data_field']]) && (strtolower($audit[$mapper['data_field']])!='no')){

                                $discrepancy_type_detail[] = $audit[$mapper['data_field']];
                                
                            }

                            
                        }

                        /* END DISCREPANCY COMMENTS*/

                        
                        //$filename = $this->downloadQuantityAction($model->id,'save');
                        $filename = $this->downloadwordAction($model->id,'save');

                        $filename = $filename.".doc";

                        $fname = ROOT."/download/".$filename;
                        
                        $attachment = "";

                        if(file_exists($fname)){

                            $attachment = $fname;   
                        
                        }

                        foreach ($bankUserArray as $bankuser) {
                            $to = $bankuser['email'];
                            $name = $bankuser['name'];
                            $body = '<html><body>';
                            $body .= 'Dear '.$name.',';
                            $body .= '<p> Warehouse Code - '.$model->warehouse_code.' , location - '.$model->location.' audit report has been submitted by Arya Collateral.</p>';
                            $body .= "<p>Furnished below are the findings in brief, of Audit Report of the following Warehouse in your region:</p>";
                            $body .= "<table width='100%' border='1'  cellpadding='0' cellspacing='0'>
                            <tr><td>State:</td><td>".$state."</td></tr>
                            <tr><td>location:</td><td>".$model->location."</td></tr>
                            <tr><td>Warehouse code:</td><td>".$model->warehouse_code."</td></tr>
                            <tr><td>Address:</td><td>".$model->full_address."</td></tr>
                            <tr><td>Type of Structure:</td><td>".$staticData['typeOfStructure'][$model->type_of_structure]."</td></tr>
                            <tr><td>Audit Date:</td><td>".date('d-M-Y',strtotime($model->warehouse_visit_date))."</td></tr>
                            <tr><td>Auditor Name:</td><td>".$model->auditor_name."</td></tr>";

                            if(count($discrepancy_type_detail)>0){
                            $body .="<tr><td>Discrepancy Type Details:</td><td><ul style='padding-left:20px;'><li type='1'>" . implode("</li><li type='1'>", $discrepancy_type_detail) . "</li></ul></td></tr>";
                            }
                             $body .="<tr><td colspan='2'>The detailed Audit report and the corresponding photographs are attached herewith for your perusal & records.</td></tr>
                            </table>";
                            
                            $body .= 'Regards <br/> '.$auth->name;
                            $body .= '</body></html>';
                            $sentTo[]=$bankuser;
                            $mail->sendMail($to, $name, $subject, $body,null,$attachment);
                            if(file_exists($fname)){
                                //unlink($fname);    
                            }
                            
                        }

                        error_log(date("d-m-Y H:i:s")." Mail Sent to ".count($sentTo)."  ".json_encode($sentTo)." \n", 3, $this->logApprove);
                    }
                    }
                    error_log(date("d-m-Y H:i:s")." End Done ".$post['id']." ".$model->warehouse_code." by ".$auth->name." \n\n", 3, $this->logApprove);

                    $responseMessage = 'Success';
                     
                } else {
                    error_log(date("d-m-Y H:i:s")." Error Could not save ".$post['id']." ".$model->warehouse_code." by ".$auth->name." \n\n", 3, $this->logApprove);
                   // echo json_encode(array('value' => 'Error'));
                     
                }
            } else {

                error_log(date("d-m-Y H:i:s")." Error In Valid ".$post['id']." ".$model->warehouse_code." by ".$auth->name." \n\n", 3, $this->logApprove);
               // echo json_encode(array('value' => 'Error'));
                 
            }
        }
        echo json_encode(array('value' => $responseMessage));
        exit;
    }

    public function addcommentAction()
    {
        $log = new Auditlog();
        $form = new AuditlogForm();
        $wh_form = new AuditInspectionForm();

        $this->view->model = $log;
        $this->view->wh_form = $wh_form;

        $auth = $this->session->get('auth');
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $post['created_at'] = date('Y-m-d H:i:s');
            $post['checker_id'] = $auth->id;
            $wh_model = AuditInspection::findFirst($post['audit_inspection_id']);
            if (!$wh_model) {
                $this->redirect($this->url->get().'audit/user/index');
            }
            $this->view->wh_model = $wh_model;
            $form->bind($post, $log);
            if ($form->isValid()) {
                $log->save();
                $log_id = $log->id;
                if ($log_id) {
                    $data = array('value' => 200, 'message' => 'Success');
                    $wh_post['updated_at'] = date('Y-m-d H:i:s');
                    $wh_post['approved'] = '3';
                    $wh_post['approved_by'] = $auth->id;
                    $wh_form->bind($wh_post, $wh_model);
                    if ($wh_form->isValid()) {
                        if ($wh_model->save() == true) {
                            $mailId = User::getEmailByUserId($wh_model->maker_id);
                            $to = $mailId['email'];
                            $name = $mailId['name'];
                            $comment = htmlspecialchars($post['comment']);
                            $subject = $wh_model->warehouse_code.' Audit Rejected';
                            $body = '<html><body>';
                            $body .= 'Dear '.$name.',';
                            $body .= '<p> Warehouse Code - '.$wh_model->warehouse_code.' , location - '.$wh_model->location.' has been rejected.</p>';
                            $body .= '<p>Comments- '.$comment.'</p>';
                            $body .= 'Regards <br/> '.$auth->name;
                            $body .= '</body></html>';
                            $mail = new Mail();
                            $mail->sendMail($to, $name, $subject, $body);
                            echo json_encode(array('value' => 'Success'));
                            die;
                        } else {
                            echo json_encode(array('value' => 'Error'));
                            die;
                        }
                    } else {
                        echo json_encode(array('value' => 'Error'));
                        die;
                    }

                    // die;
                } else {
                    echo 'Sorry, the following problems were generated: ';
                    foreach ($log->getMessages() as $message) {
                        echo json_encode($message->getMessage());
                        die;
                    }
                }
            } else {
                echo json_encode(array('value' => 'Error'));
                die;
            }
        }
    }

    public function quantityAction($inspectionId)
    {
        $inspectionId = (int) $inspectionId;

        $auditInsp = AuditInspection::findfirst($inspectionId);

        $this->view->model = $auditInsp;

        //'state', 'sol_id', 'location','wh_address', 'type_of_wh', 'warehouse_location','col_mgr_name', 'qty_unit'
        $qualityArray = AuditMaster::query()
            ->columns(array('id', 'receipt', 'commodity_name','bal_no_bags','os_quantity'))
            ->where("audit_id='".$auditInsp->audit_id."'")
            ->execute()->toArray();

        $this->view->qualityData = $qualityArray;

        $inspObject = new AuditInspection();

        $staticData = $inspObject->staticData();

        $this->view->staticData = $staticData['qualityData'];

        $this->view->audit_inspection_id = $inspectionId;

        if ($inspectionId) {
            
            $dbdata = array();
            
            $quantityArray = AuditQuantity::query()->where("audit_inspection_id='".$inspectionId."'")->execute()->toArray();
            if (count($quantityArray) > 0) {
                $dbdata = $quantityArray;
            }else{
            /* GET LAST AUDIT INSPECTION QUANTITY DATA */
            $prevAuditDetail = AuditInspection::query()->columns(array("id","audit_id"))->where("warehouse_code='".$auditInsp->warehouse_code."'")
            ->andWhere("id!='".$inspectionId."'")->andWhere("approved='2'")->orderBy("id desc limit 1")->execute();
            
            if(count($prevAuditDetail)>0){
                $prevInspectionId = $prevAuditDetail[0]['id'];

                if(count($qualityArray)>0){
                    foreach($qualityArray as $newQuality){
                        $preQuantityArray = AuditQuantity::query()->where("audit_inspection_id='".$prevInspectionId."'")->andWhere("receipt='".$newQuality['receipt']."'")->execute()->toArray();
                            if(count($preQuantityArray) > 0) {
                                $dbdata = array_merge($dbdata, $preQuantityArray);
                                //$dbdata = $preQuantityArray;
                           }
                    }
                }
                
            }    
            /* END LAST AUDIT INSPECTION QUANTITY DATA*/ 
            }

            $this->view->dbdata = $dbdata;
        }

        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            $data = array();

            $quantityForm = new AuditQuantityForm();
            
            if (!empty($post['physical_quantity'])) {
                foreach ($post['physical_quantity'] as $key => $val) {
                    $data['audit_inspection_id'] = $inspectionId;
                    $data['audit_master_id'] = $key;
                    $data['audit_date'] = date('Y-m-d', strtotime($auditInsp->created_at));
                    $data['physical_quantity'] = $val;
                    $data['no_of_bags'] = $post['no_of_bags'][$key];
                    $data['diff_quantity'] = $post['diffQty'][$key];
                    $data['diff_bags'] = $post['diffbags'][$key];
                    $data['remarks'] = $post['remarks'][$key];
                    $data['receipt'] = $post['receipt'][$key];
                    
                    $checkupdate = AuditQuantity:: query()->columns('id')->where("audit_inspection_id='".$inspectionId."'")->andWhere("audit_master_id='".$key."'")->execute()->toArray();
                    if (count($checkupdate) > 0) {
                        $quantityId = $checkupdate[0]['id'];
                        $quantityModel = AuditQuantity::findfirst($quantityId);
                    } else {
                        $quantityModel = new AuditQuantity();
                    }

                    $quantityForm->bind($data, $quantityModel);

                    if ($quantityForm->isValid()) {
                        $quantityModel->save();
                    }
                }
                        if($post['qualityFormId'] == 1){
                            $inspectionpost= array();
                            $auditInspForm = new AuditInspectionForm();
                            $inspectionpost['active']='1';
                            $auditInspForm->bind($inspectionpost,$auditInsp);
                            if($auditInspForm->isValid()){
                                $auditInsp->save();
                            }
                        }
                //$this->flash->success('Audit has been created successfully.');
                $this->redirect($this->url->get().'audit/user/quality/'.$inspectionId);
            }
        }
    }

    public function downloadQuantityAction($id,$type=null)
    {
        if ($id) {
            
            $auditModel = AuditInspection::findFirst($id);
            
            $fileName = str_replace("/","_",date('Y-m-d')."_".$auditModel->warehouse_code.'.csv');

            $staticData = $auditModel->staticData();

            $filepath = ROOT."/download/".$fileName;
 
            if($type=="save"){

                
                $f = fopen($filepath,'w'); 

                chmod($filepath, 0755); 

            }else{
                
                header('Content-Type: application/csv');
            
                header('Content-Disposition: attachment; filename='.$fileName.';'); 

                $f = fopen('php://output', 'w');   
            }
            
            
            //$f = fopen('php://output', 'w'); 
            
            $auditcaption = array('FINAL REPORT');
            
            fputcsv($f, $auditcaption, ',');
            
            $auditlabel = array('INSPECTING AGENCY: Arya Collateral Warehousing Services Pvt. Ltd.');   
            fputcsv($f, $auditlabel, ',');

            $auditsublabel = array('S.No.','DESCRIPTION','VALUE','REMARKS');   
            fputcsv($f, $auditsublabel, ',');

            $auditPrefilledData = $auditModel->toArray();

            if($auditModel->id){
                $auditReasonArr = AuditReason::query()->columns(array('field', 'reason'))->where("audit_inspection_id = '".$auditModel->id."'")->execute()->toArray();

                $reasonData = array();

                if (count($auditReasonArr) > 0) {
                    foreach ($auditReasonArr as $resdata) {
                        $reasonData[$resdata['field']] = $resdata['reason'];
                    }
                }
            }

            
            $auditheader = AuditFieldMapper::query()->execute()->toArray();

            $auditArr = [];
            
            $i =1 ;

            $hideColumns = $this->hidemodelfields();
            
            foreach($auditheader as $head){

                $staticArray  = array('type_of_structure'=>'typeOfStructure','col_mgr_name'=>'collateralManger','compromised_lock_key'=>'lockKey');

                if(array_key_exists($head['field_name'],$staticArray)){
                        $auditPrefilledData[$head['field_name']] = $staticData[$staticArray[$head['field_name']]][$auditPrefilledData[$head['field_name']]];
                    }

               //$str = '';
               $str1= ''; 

               if($head['remark']=='1' && !empty($reasonData[$head['field_name']])){ //$str = 'Remark'; 
               $str1=$reasonData[$head['field_name']];  }

                    if($auditPrefilledData['compromised_lock_key']=="With CM Agency" && in_array($head['field_name'],$hideColumns)){
                        continue; 
                    }else{
                        $auditArr[] = array($i,$head['label'],$auditPrefilledData[$head['field_name']],$str1);         
                    }
               $i++;
            }
            
            foreach($auditArr as $audit){
                $audit = preg_replace('/[^(\x20-\x7F)]*/','', $audit);
                fputcsv($f, $audit, ',');    
            }
              
            
            $auditblank =array();

            fputcsv($f, $auditblank, ',');

            fputcsv($f, ['QUANTITY REPORT'], ',');

            $header = array();
            
            foreach ($staticData['qualityData'] as $heads) {
                $header[] = $heads;
            }
           
            $staticheader = array('Bags as per PV','Qty as per PV','Difference in no. of Bags','Difference in Qty','Remark');
            
            $fheader = array_merge($header, $staticheader);

            fputcsv($f, $fheader, ',');
            
            $qualityArray = AuditMaster::query()
            ->columns(array('id', 'receipt', 'commodity_name', 'bal_no_bags',  'os_quantity'))
            ->where("audit_id='".$auditModel->audit_id."'")
            ->execute()->toArray();

            $finalData = array();
            if (count($qualityArray) > 0) {
                foreach ($qualityArray as $data) {
                    $quantityData = array();
                    $val = array();
                    $quantityArray = AuditQuantity::query()->columns(array('no_of_bags','physical_quantity', 'diff_bags','diff_quantity','remarks'))->where("audit_inspection_id='".$auditModel->id."'")->andWhere("audit_master_id='".$data['id']."'")->execute()->toArray();
                    if (count($quantityArray) > 0) {
                        foreach ($quantityArray as $value) {
                            //$val['audit_date'] = $auditModel->warehouse_visit_date;
                            //$quantityData = array_merge($val,$value);
                            $quantityData = $value;
                        }
                    }
                    unset($data['id']);
                    $finalData = array_merge($data, $quantityData);
                    fputcsv($f, $finalData, ',');
                }
            }

            $auditblank = array();

            fputcsv($f, $auditblank, ',');

            fputcsv($f, ['QUALITY REPORT'], ',');

            $qheader = array('Commodity Name','Parameter','Limits','Result','Remarks');
            
            fputcsv($f, $qheader, ',');
            
            
            //$qArray = array();
            $qArray = AuditQuality::query()
            ->columns(array('commodity_name', 'parameter', 'limits',  'result','remarks'))
            ->where("audit_inspection_id='".$auditModel->id."'")
            ->execute()->toArray();
             
            $qDataArr = array();
            if (count($qArray) > 0) {
                    foreach($qArray as $val){
                       fputcsv($f, $val, ',');
 
                    }
                }
                
                //fputcsv($f, $qDataArr, ',');
        }
        fclose($f);
            
        if($type=="save"){

            
            return $fileName;
        }else{
        exit;    
        }    
        
    }

    public function imageuploadAction()
    {
        $this->view->disable();
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        $uploadImg = array();

        if ($this->request->hasFiles() == true) {
            $fileExt = array('jpg', 'jpeg', 'png', 'gif');
            $path = ROOT.'/audit/images/';
            $thumbpath = ROOT.'/audit/images/thumb/';

            foreach ($this->request->getUploadedFiles() as $file) {
                if (in_array(strtolower($file->getExtension()), $fileExt)) {
                    if ($file->getSize() <= '5242880') {
                        $fileName = time().$file->getName();
                    //echo $file->getName();

                    $tempName = $file->getTempName();
                        if ($file->moveTo($path.$fileName)) {
                            $uploadImg['id'] = $file->getKey();
                            $uploadImg['value'] = $fileName;
                            
                            $image = new \Phalcon\Image\Adapter\GD(ROOT."/audit/images/" . $fileName);
                            $image->resize(210, 140);
                            $image->save(ROOT."/audit/images/thumb/" . $fileName);   
                        }
                    }
                }
            }
        }
        echo json_encode($uploadImg);
        exit;
    }


    public function delimageAction(){
        $this->view->disable();
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $auth = $this->session->get('auth');
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $id = $post['id'];
            $fieldName = $post['fieldName']; 
            if($post['imgName']!="" && file_exists(ROOT.'/audit/images/'.$post['imgName'])){
                unlink(ROOT.'/audit/images/'.$post['imgName']);
                unlink(ROOT.'/audit/images/thumb/'.$post['imgName']);
                $sql = "update Audit\Model\AuditInspection set ".$fieldName."='' where id='".$id."'";
                $query = $this->modelsManager->createQuery($sql);
                $list = $query->execute();
                if($list){
                    echo 'Success';
                }else{
                    echo 'Error';
                    }
            }else{
                echo 'Error';
                }
        }
        exit;

    }



    private function hidemodelfields(){
        return $validationArr = ['dunnage','live_wire','flooring','roofing','ventilation','plinth_height','fire_fighting_equipment','no_of_ffe','bank_nameplate','bank_fund_stock','stack_card_bank_name','stacking_arrangement','stock_countable','heavy_insect_infestation','stock_health','commodity_related_remarks','cm_available','frequency_of_audit_team','security_guard_available','security_type','security_shift','security_exclusivity','overall_quality','stock_register_available','lien_register','visitor_register','stack_card_maintain','yes_lien_register','fumigation_register','cm_attendance_register','cm_bearing_id_card','capture_location_name','capture_warehouse_address','other_observation','preventive_action','stock_release_register','corrective_action'];
    }


public function downloadwordAction($id,$type=null)
    {
        if ($id) {
            
            $auditModel = AuditInspection::findFirst($id);
            
            $fileName = str_replace("/","_",date('Y-m-d')."_".$auditModel->warehouse_code);

            $staticData = $auditModel->staticData();

            $html = "<style type='text/css'> th,td {font-family: Cambria,Georgia,serif !important;   font-size:12px ;} table {
    border-collapse: collapse;} table, th, td { border: 1px solid black; }.pagebreak {page-break-before:always;clear:both}
    @page Section1
    {
        size:841.9pt 595.3pt;
        mso-page-orientation:landscape;
        margin: 0.7cm 0.7cm 0.7cm 0.7cm;
        mso-header-margin: 42.55pt;
        mso-footer-margin: 49.6pt;
        mso-paper-source: 0;
        layout-grid: 18.0pt;
    }
    div.Section1
    {
        page: Section1;
    }  </style>"; 
            
            $html .="<html><head><title></title></head><body><div class='Section1'>";

            
            $html .= "<h2 align='center' class='pagebreak'>FINAL REPORT</h2><h4>INSPECTING AGENCY: Arya Collateral Warehousing Services Pvt. Ltd.</h4>";

            $html .= "<table><thead>
                <tr>
                    <th>S.No.</th>
                    <th>DESCRIPTION</th>
                    <th>VALUE</th>
                    <th>REMARKS</th>
                </tr>    
            </thead>";
            $html .="<tbody>"; 
            
            $auditPrefilledData = $auditModel->toArray();


            if($auditModel->id){
                
                $auditReasonArr = AuditReason::query()->columns(array('field', 'reason'))->where("audit_inspection_id = '".$auditModel->id."'")->execute()->toArray();

                $reasonData = array();

                if (count($auditReasonArr) > 0) {
                    foreach ($auditReasonArr as $resdata) {
                        $reasonData[$resdata['field']] = $resdata['reason'];
                    }
                }
            }

            $auditheader = AuditFieldMapper::query()->execute()->toArray();

           
            $auditArr = [];
            
            $i =1 ;

            $hideColumns = $this->hidemodelfields();
            
            foreach($auditheader as $head){

                $staticArray  = array('type_of_structure'=>'typeOfStructure','control_agreement'=>'controlAgreement','compromised_lock_key'=>'lockKey');

                if(array_key_exists($head['field_name'],$staticArray)){
                        $auditPrefilledData[$head['field_name']] = $staticData[$staticArray[$head['field_name']]][$auditPrefilledData[$head['field_name']]];
                    }

               $str1= ''; 

               if($head['remark']=='1' && !empty($reasonData[$head['field_name']])){ $str1=$reasonData[$head['field_name']];  }

                if($auditPrefilledData['compromised_lock_key']=="AUDIT NOT DONE" && in_array($head['field_name'],$hideColumns)){
                        continue; 
                }else{
                        $html.="<tr>
                            <td>".$i."</td>
                            <td>".$head['label']."</td>
                            <td>".$auditPrefilledData[$head['field_name']]."</td>
                            <td>".$str1."</td>
                        </tr>";
                }

               $i++;
            }

            /*ADD IMAGES*/
            $imageArray = ['warehouse_photo'=>'Warehouse Photo with Auditor and CM Photo','visitor_register_photo'=>'Visitor Register Photo','stock_register_photo'=>'Stock Register Photo','stack_card_photo'=>'Stack Card Photo','stack_arrangement_photo'=>'Stack Arrangement Photo','commodity_photo'=>'Commodity photo','extra_photo1'=>'Extra Photo 1','extra_photo2'=>'Extra Photo 2'];

            foreach($imageArray as $k=> $img){
                if(!empty($auditPrefilledData[$k]) && file_exists(ROOT.'/audit/images/thumb/'.$auditPrefilledData[$k])){
                
                $photo ="";
                $photo ="<img src='"."http://" . $_SERVER['SERVER_NAME']."/audit/images/thumb/".$auditPrefilledData[$k]."' width='120' height='120'>";

                    $html.="<tr>
                        <td>".$i."</td>
                        <td>".$img."</td>
                        <td>".$photo ."</td>
                        <td>".$str1."</td>
                    </tr>";
            $i++;
            }

            }

            /*END */
            
            $html .="</tbody></table>";
 
            $html .="<h2 align='center' class='pagebreak'>QUANTITY REPORT</h2>";

            $html .="<table class='table' width='100%'><thead><tr>";
            foreach ($staticData['qualityData'] as $heads) {
                $html .="<th>".$heads."</th>";
            }
            $html .="<th>Bags as per PV</th>";
            $html .="<th>Qty as per PV</th>";
            $html .="<th>Difference in no. of Bags</th>";
            $html .="<th>Difference in Qty</th>";
            $html .="<th>Remark</th>";
            $html .="</thead>";
           
            $qualityArray = AuditMaster::query()
            ->columns(array('id', 'receipt', 'commodity_name', 'bal_no_bags',  'os_quantity'))
            ->where("audit_id='".$auditModel->audit_id."'")
            ->execute()->toArray();

            $finalData = array();
            if (count($qualityArray) > 0) {
                foreach ($qualityArray as $data) {
                    $quantityData = array();
                    $val = array();
                    $quantityArray = AuditQuantity::query()->columns(array( 'no_of_bags','physical_quantity', 'diff_bags','diff_quantity','remarks'))->where("audit_inspection_id='".$auditModel->id."'")->andWhere("audit_master_id='".$data['id']."'")->execute()->toArray();
                    if (count($quantityArray) > 0) {
                        foreach ($quantityArray as $value) {
                            //$val['audit_date'] = $auditModel->warehouse_visit_date;
                            //$quantityData = array_merge($val,$value);
                            $quantityData = $value;
                        }
                    }
                    unset($data['id']);
                    $finalData = array_merge($data, $quantityData);
                    
                    $html.="<tbody><tr>";
                    foreach($finalData as $k=>$db){
                        $html.="<td>".$db."</td>";
                    }
                    $html.="</tr>";
                }
            }
        }
        $html .="</tbody></table>";

        
        $html .="<h2 align='center' class='pagebreak'>QUALITY REPORT</h2>";

            $html .="<table class='table' width='100%'><thead><tr>";
            $html .="<th>Commodity Name</th>";
            $html .="<th>Parameter</th>";
            $html .="<th>Limits</th>";
            $html .="<th>Result</th>";
            $html .="<th>Remarks</th>";
            $html .="</thead>";

            $qArr = AuditQuality::query()->where("audit_inspection_id='".$auditModel->id."'")->execute()->toArray();
            if(count($qArr)>0){
                $html.="<tbody>";
                foreach($qArr as $qdata){
                    $html .="<tr>";
                    $html.="<td>".$qdata['commodity_name']."</td>";
                    $html.="<td>".$qdata['parameter']."</td>";
                    $html.="<td>".$qdata['limits']."</td>";
                    $html.="<td>".$qdata['result']."</td>";
                    $html.="<td>".$qdata['remarks']."</td>";
                    $html.="</tr>";
                }
                //$html.="</tr>";
            }

         $html .="</div></body></html>";
         $html = str_replace("\"/static","\"http://".$_SERVER['HTTP_HOST']."/static",$html);
         
         $htmltodoc = new \PhpOffice\PhpWord\HtmlToDoc();
         if($type=="save"){$flag = false;}else{$flag = true; }
         $htmltodoc->createDoc($html,$fileName,$flag);
         if($type=="save"){ return $fileName; }else{ exit; }
    }


    public function downloadcsvAction(){

            
            $warehouse_code = $this->request->getQuery('wh');
            $fdate = $this->request->getQuery('fdate');
            $tdate  = $this->request->getQuery('tdate');
            $audit_month = $this->request->getQuery('amonth');
            $audit_year = $this->request->getQuery('ayear');

            $warehouse_condition = "1=1";
            $from_condition =  "1=1";
            $to_condition = "1=1";
            $auditmonth_condition="1=1";
            if (!empty($warehouse_code)) {
                $warehouse_condition = "Audit\Model\AuditInspection.warehouse_code = '".$warehouse_code."'";
            }
            if (!empty($fdate)) {
                $from = date('Y-m-d', strtotime($fdate));
                $from_condition = "date(Audit\Model\AuditInspection.warehouse_visit_date) >='".$from."'";
            }
            if (!empty($tdate)) {
                $to = date('Y-m-d', strtotime($tdate));
                $to_condition = "date(Audit\Model\AuditInspection.warehouse_visit_date) <='".$to."'";
            }

            if (!empty($audit_month)) {
                $amonth = $audit_year.'-'.$audit_month;
                $auditmonth_condition = "Audit\Model\AuditInspection.audit_month ='".$amonth."'";
            }



        $auditheader = AuditFieldMapper::query()->execute()->toArray();
        
        $columns = [];
        $csvheader = [];
        if(count($auditheader)>0){
            foreach($auditheader as $head){
                $columns[] = "Audit\Model\AuditInspection.".$head['field_name']; 
                $csvheader[] = $head['label'];
            }
        }
            /* 6 Month Date */
        //$presixMonth = (new \DateTime())->sub(new \DateInterval('P6M'))->format('Y-m-d');
        /* 6 Month DATE */
        
            $auth = $this->session->get('auth');

            $userState = User::findFirst(array('columns'=>'state',$auth->id))->toArray();
            if($userState['state']){
                $bankCon ="AM.state IN ('".str_replace(",", "','", $userState['state'])."')";
            }

        $auditInspObject = AuditInspection::query()->columns($columns)
                ->leftJoin("Audit\Model\AuditMaster","AM.audit_id=Audit\Model\AuditInspection.audit_id AND AM.warehouse_code=Audit\Model\AuditInspection.warehouse_code","AM")
                ->where($warehouse_condition)
                ->andWhere($from_condition)
                ->andWhere($to_condition)
                ->andWhere($auditmonth_condition)
                ->andWhere($bankCon)
                ->andWhere("Audit\Model\AuditInspection.approved='2'")
                ->andWhere("Audit\Model\AuditInspection.audit_id!='0'")
                //->andWhere("created_at > '".$presixMonth."'")
                ->groupBy(array('AM.audit_id','AM.warehouse_code'))
                ->orderBy('Audit\Model\AuditInspection.id desc')
                ->execute()->toArray();

        /*Quantity list */
        $auditObj = new AuditInspection();
        $staticData = $auditObj->staticData();
        /*$qunatityheader = $staticData['qualityData'];
        $qunatityheader[] = 'Physical Quantity (in kgs/quintals)';
        $qunatityheader[] = 'No of Bags';
        $qunatityheader[] = 'Difference in Quantity if any (in kgs/quintals)';
        $qunatityheader[] = 'Whether Infestation / Deterioration observed in the stocks';
        $qunatityheader[] = 'Fumigation / Spraying Required';
        $qunatityheader[] = 'Commodity Age (in months)';
        $qunatityheader[] = 'Remark';*/

        //$header = array_merge($csvheader,$qunatityheader);
        $fileName ="audit.csv";
        
        header('Content-Type: application/csv');
            
        header('Content-Disposition: attachment; filename='.$fileName.';'); 

        $f = fopen('php://output', 'w'); 
        
        fputcsv($f, $csvheader, ',');

        if(count($auditInspObject)>0){
           foreach($auditInspObject as $auditR){

                $staticArray  = array('type_of_structure'=>'typeOfStructure','col_mgr_name'=>'collateralManger','compromised_lock_key'=>'lockKey');
                
                $auditR['type_of_structure'] = $staticData['typeOfStructure'][$auditR['type_of_structure']];
                $auditR['col_mgr_name'] = $staticData['collateralManger'][$auditR['col_mgr_name']];
                $auditR['compromised_lock_key'] = $staticData['lockKey'][$auditR['compromised_lock_key']];
                $auditR = preg_replace('/[^(\x20-\x7F)]*/','', $auditR);
                fputcsv($f, $auditR, ','); 
           } 
        } 
        exit;
    }


    public function qualityAction($inspectionId){

        $inspectionId = (int) $inspectionId;

        $auditInsp = AuditInspection::findfirst($inspectionId);

        $this->view->model = $auditInsp;

        if($inspectionId){

            $dbdata = array();
            $qualityArray = AuditQuality::query()->where("audit_inspection_id='".$inspectionId."'")->execute()->toArray();
            if (count($qualityArray) > 0) {
                $dbdata = $qualityArray;
            }

            $this->view->dbdata = $dbdata;
        }


        if ($this->request->isPost()) {
            
            $post = $this->request->getPost();

            $data = array();

            $qualityForm = new AuditQualityForm();
            
            if (!empty($post['commodity_name'])) {
                foreach ($post['commodity_name'] as $key => $val) {
                    if(!empty($val)){
                    $data['audit_inspection_id'] = $inspectionId;
                    $data['commodity_name'] = strtoupper($val);
                    $data['parameter'] = strtoupper($post['parameter'][$key]);
                    $data['limits'] = $post['limits'][$key];
                    $data['result'] = $post['result'][$key];
                    $data['remarks'] = $post['remarks'][$key];
                    
                    
                    $checkupdate = AuditQuality:: query()->columns('id')->where("audit_inspection_id='".$inspectionId."'")->andWhere("commodity_name='".strtoupper($val)."'")->andWhere("parameter='".strtoupper($data['parameter'])."'")->execute()->toArray();
                    if (count($checkupdate) > 0) {
                        $qualityId = $checkupdate[0]['id'];
                        $qualityModel = AuditQuality::findfirst($qualityId);
                    } else {
                        $qualityModel = new AuditQuality();
                    }

                    $qualityForm->bind($data, $qualityModel);

                    if ($qualityForm->isValid()) {
                        $qualityModel->save();
                    }
                }
                }
            }
            $this->flash->success('Audit has been created successfully.');
            $this->redirect($this->url->get().'audit/user/');
        }
    }

}


