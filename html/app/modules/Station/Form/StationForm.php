<?php

namespace Station\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;

class StationForm extends Form
{
    public function initialize()
    {
     
        $this->add(
            (new Text('airport_name', [
                'required' => true,
            ]))->setLabel('Airport Name')
        );

        $this->add(
            (new Text('station_code', [
                'required' => true,
            ]))->setLabel('Station Code')
        );

        $this->add(
            (new Text('country', [
                'required' => true,
            ]))->setLabel('Country')
        );
        $this->add(
            (new Text('state', [
                'required' => true,
            ]))->setLabel('State')
        );
        $this->add(
            (new Text('city', [
                'required' => true,
            ]))->setLabel('City')
        );
        

        $this->add(
            (new Check('active'))
                ->setLabel('Active')
        );     

    }

}   