<?php

/**
 * Routes
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace Station;

use Application\Mvc\Router\DefaultRouter;
//use Category\Model\Category;

class Routes
{

     public function init($router)
    {    
    $router->add('/station', array(
            'module'     => 'station',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('station');
        
        $router->add('/station/', array(
            'module'     => 'station',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('station');
        
        $router->add('/station/admin/', array(
            'module'     => 'station',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('station');
        
        
        return $router;
    }     

}