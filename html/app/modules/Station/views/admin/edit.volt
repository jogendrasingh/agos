<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}station/admin" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
        {% if(not(model.id is empty)) %}
            <a href="{{ url.get() }}station/admin/delete/{{ model.id }}" class="ui button red">
                <i class="icon trash"></i> Delete
            </a>
        {% endif %}
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <div class="two fields">
            <div class="field">
                {{ form.renderDecorated('airport_name') }}
                {{ form.renderDecorated('station_code') }}
                {{ form.renderDecorated('country') }}
                {{ form.renderDecorated('state') }}
                {{ form.renderDecorated('city') }}
                {{ form.renderDecorated('active') }}
            </div>
            <div class="field">
            </div>
        </div>
    </div>

</form>

<script>
    $('.ui.form').form({
           fields: {
            airport_name: {
                identifier: 'airport_name',
                rules: [
                    {type: 'empty'}
                ]
            },
            station_code: {
                identifier: 'station_code',
                rules: [
                    {type: 'empty'}
                ]
            },
            country: {
                identifier: 'country',
                rules: [
                    {type: 'empty'}
                ]
            },
            state: {
                identifier: 'state',
                rules: [
                    {type: 'empty'}
                ]
            },
            city: {
                identifier: 'city',
                rules: [
                    {type: 'empty'}
                ]
            }

    }
 });
          
</script>