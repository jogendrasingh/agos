<?php
namespace Training\Controller;
use YonaCMS\Plugin\Mail;
use Application\Mvc\Controller;
use Training\Model\Training;
use Training\Form\TrainingForm;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AdminController extends Controller {

    public function initialize() {
        $this->setAdminEnvironment();
        $this->helper->activeMenu()->setActive('training');

    }

    public function indexAction() {

        $this->view->entries = Training::find([
            "order" => "id DESC"
        ]);

        $this->helper->title($this->helper->at('Manage Training'), true);
    }

    public function addAction() {

        $this->view->pick(['admin/edit']);
        $model = new Training();
        $form = new TrainingForm();
        
        if ($this->request->isPost()) {
            $model = new Training();
            $post = $this->request->getPost();
            if($post['duration']!=""){
                if (!preg_match('/^[0-9]*$/', $post['duration'])) {
                    return $this->redirect($this->url->get() . 'training/admin/add');
                    exit;
                }
            }
        
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $this->flash->success($this->helper->at('Training created', ['name' => $model->training]));
                    $this->redirect($this->url->get() . 'training/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }

        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->submitButton = $this->helper->at('Add New');

        $this->helper->title($this->helper->at('Manage Training'), true);
    
    }

    public function editAction($id) {
        $model = Training::findFirst($id);
        if (!$model) {
            $this->redirect($this->url->get() . 'training/admin');
        }

        $form = new TrainingForm();

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            if($post['duration']!=""){
                if (!preg_match('/^[0-9]*$/', $post['duration'])) {
                    $this->flash->error('Duration should be numeric.');
                    return $this->redirect($this->url->get() . 'training/admin/edit/'.$id);
                    exit;
                }
            }
            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save() == true) {
                    $this->flash->success('Training <b>' . $model->name . '</b> has been saved');
                    return $this->redirect($this->url->get() . 'training/admin');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        } else {
            $form->setEntity($model);
        }

        $this->view->submitButton = $this->helper->at('Save');
        $this->view->form = $form;
        $this->view->model = $model;

        $this->helper->title($this->helper->at('Manage Training'), true);
    }

    
    public function deleteAction($id) {
        $model = Training::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'training/admin');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('Deleting Training <b>' . $model->training . '</b>');
            return $this->redirect($this->url->get() . 'training/admin');
        }

        $this->view->model = $model;

        $this->helper->title($this->helper->at('Delete Training'), true);
    }

}
