<?php

namespace Training\Form;

use Application\Form\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Digit as DigitValidator;

class TrainingForm extends Form
{
    public function initialize()
    {
     
        $this->add(
            (new Text('category', [
                'required' => true,
            ]))->setLabel('Category')
        );

        $this->add(
            (new Text('training', [
                'required' => true,
            ]))->setLabel('Training')
        );


        $this->add((new Text('duration', [
                'required' => false,
            ]))
        /*->addValidator(new DigitValidator([
            'message' => 'Duration must be numeric'
        ]))*/
        ->setLabel('Duration (In Days)')
        );



         $this->add(
            (new Check('active'))
                ->setLabel('Active')
        );
           

    }

}   