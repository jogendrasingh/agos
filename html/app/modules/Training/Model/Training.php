<?php

namespace Training\Model;

use Application\Mvc\Model\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\PresenceOf;
use Application\Localization\Transliterator;

class Training extends Model
{

    public function getSource()
    {
        return "training";
    }

  
    public $id;
    public $category;
    public $training;
    public $duration;
    public $created_at;
    public $active;
    
    public function initialize()
    {
    }
    
 
    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    
  /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

      public function getActive()
    {
        return $this->active;
    }
    

    public function isActive()
    {
        if ($this->active) {
            return true;
        }
    }
    
     public function setActive($active)
    {
        $this->active = $active;
    }
    
    public function setCheckboxes($post)
    {
        $this->setActive(isset($post['active']) ? 1 : 0);
    }

   
}
