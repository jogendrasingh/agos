<?php

/**
 * Routes
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace Training;

use Application\Mvc\Router\DefaultRouter;
//use Category\Model\Category;

class Routes
{

     public function init($router)
    {    
    $router->add('/training', array(
            'module'     => 'training',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('training');
        
        $router->add('/training/', array(
            'module'     => 'training',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('training');
        
        $router->add('/training/admin/', array(
            'module'     => 'training',
            'controller' => 'admin',
            'action'     => 'index',
        ))->setName('training');
        
        
        return $router;
    }     

}