<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}training/admin" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
        {% if(not(model.id is empty)) %}
            <a href="{{ url.get() }}training/admin/delete/{{ model.id }}" class="ui button red">
                <i class="icon trash"></i> Delete
            </a>
        {% endif %}
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <div class="two fields">
            <div class="field">
                {{ form.renderDecorated('category') }}
                {{ form.renderDecorated('training') }}
                {{ form.renderDecorated('duration') }}
                {{ form.renderDecorated('active') }}
            </div>
            <div class="field">
            </div>
        </div>
    </div>

</form>

<script>
    $('.ui.form').form({
           fields: {
            category: {
                identifier: 'category',
                rules: [
                    {type: 'empty'}
                ]
            },
            training: {
                identifier: 'training',
                rules: [
                    {type: 'empty'}
                ]
            },
    }
 });
          
</script>