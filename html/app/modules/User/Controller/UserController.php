<?php
/**
 * AdminUserController
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace User\Controller;
//ini_set('max_execution_time', 600);
use Application\Mvc\Controller;
use User\Form\UserForm;
use User\Model\User;
use Category\Model\Category;
use Phalcon\Ext\Mailer\Message;
use User\Model\State;
use YonaCMS\Plugin\Mail;
//use Audit\Model\AuditMaster;
class UserController extends Controller
{

    var $logUser =  "/sftpusers/chroot/arya/arya_axis/logUser.txt" ;
    //var $logUser =  "D:/xampp/htdocs/projects/arya_external_audit/logUser.txt" ;


    public function initialize()
    {
        $this->setAdminEnvironment();
        $this->helper->activeMenu()->setActive('user');
        $this->view->languages_disabled = true;

        //$this->cc ="gajendra.parihar@aryacma.co.in";
        //$this->cc2 ="ritesh.raman@aryacma.co.in";
        $this->cc ="jogi.pal2003@gmail.com";
        $this->cc2 ="jogendra.singh@formzero.in";
    }

    public function indexAction()
    {
        $this->view->entries = User::find([
            "order" => "id DESC"
        ]);

        $this->helper->title($this->helper->at('Manage Users'), true);
    }

    public function addAction()
    {
        $this->view->pick(['user/edit']);

        $model = new User();
        $form = new UserForm();
        $form->initAdding();

        if ($this->request->isPost()) {
            $model = new User();
            $post = $this->request->getPost();
            
        if($post['role']=="bank" && $post['state']==""){
            $this->flash->error("State can not be empty");
        }else{    
        
        if($post['role']=='bank' || $post['role']=='checker'){
                    $post['parent_id']=0;
        }
        if($post['state']!=""){
            $post['state'] = implode(",",$post['state']);
        }


            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save()) {
                    $to=$post['email'];
                    $name=$post['name'];
                    $subject='Account Confirmation';
                    $this->url->get();
                    $body = '<html><body>';
                    $body .= "Dear ".$post['name'].",";
                    $body .= "<p>Your ".$post['role']." account has been created.</p>";
                    $body .= "<p>Please find login details:</p>";
                    $body .= "<p>URL:"."http://".$_SERVER['HTTP_HOST']."</p>";
                    $body .= "<p>Username : ".$post['login']."</p>";
                    $body .= "<p>Password : ".$post['password']."</p>";
                    $body .= "Regards <br/>Arya Warehouse";
                    $body .= "</body></html>";
                    $mail = new Mail();
                    //$sendLoginMail=$this->sendMail($to, $name, $subject, $body);
                    $mail->sendMail($to, $name, $subject, $body);
                    $this->flash->success($this->helper->at('User created', ['name' => $model->getLogin()]));
                    $this->redirect($this->url->get() . 'user/user');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
        }
        }

        $this->view->form = $form;
        $this->view->model = $model;
        $this->view->submitButton = $this->helper->at('Add New');

        $this->helper->title($this->helper->at('User'), true);
    }

    public function editAction($id)
    {
        $model = User::findFirst($id);
        if (!$model) {
            $this->redirect($this->url->get() . 'user/user');
        }
        
        
        $form = new UserForm();

        if ($this->request->isPost()) {
            $post = $this->request->getPost();
           
            if($post['role']=="bank" && $post['state']==""){
            $this->flash->error("State can not be empty");
            }else{ 

            if($post['role']=='bank' || $post['role']=='checker'){
                $post['parent_id']=0;
            }

            if($post['state']!=""){
            $post['state'] = implode(",",$post['state']);
        }

            $form->bind($post, $model);
            if ($form->isValid()) {
                $model->setCheckboxes($post);
                if ($model->save() == true) {

                    error_log(date("d-m-Y H:i:s")." Update User - ".$model->id."  ".$model->name." ".json_encode($post)." \n", 3, $this->logUser);
                    error_log(date("d-m-Y H:i:s")." End Done ".$model->id." ".$model->name."\n\n", 3, $this->logUser);

                        if($post['password']){
                              $to     = $model->email;
                              $name   = $model->name;
                              $subject='Password Changed';
                              $body   = '<html><body>';
                              $body .= "Dear ".$model->name.",";
                              $body .= "<p>Your password has been changed.</p>";
                              $body .= "<p>Password :".$post['password']."</p>";
                              $body .= "Regards <br/>Arya Warehouse";
                              $body .= "</body></html>";
                              $mail = new Mail();
                              $mailc = array();
                              $mailc[] = $this->cc;
                              $mailc[] = $this->cc2;
                              $mail->sendMail($to, $name, $subject, $body,$mailc);
                                
                        }
                    
                    $this->flash->success('User <b>' . $model->getLogin() . '</b> has been saved');
                    return $this->redirect($this->url->get() . 'user/user');
                } else {
                    $this->flashErrors($model);
                }
            } else {
                $this->flashErrors($form);
            }
            }
        } else {
            $form->setEntity($model);
        }

        $this->view->submitButton = $this->helper->at('Save');
        $this->view->form = $form;
        $this->view->model = $model;

        $this->helper->title($this->helper->at('Manage Users'), true);
    }
    
    
    public function deleteAction($id)
    {
        $model = User::findFirst($id);
        if (!$model) {
            return $this->redirect($this->url->get() . 'user/user');
        }

        if ($model->getLogin() == 'admin') {
            $this->flash->error('Admin user cannot be deleted');
            return $this->redirect($this->url->get() . 'user/user');
        }

        if ($this->request->isPost()) {
            $model->delete();
            $this->flash->warning('User <b>' . $model->getLogin() . ' has been deleted.</b>');
            return $this->redirect($this->url->get() . 'user/user/index');
        }

        $this->view->model = $model;

        $this->helper->title($this->helper->at('Delete User'), true);
    }
    
    
    public function getDataAction(){
        if($this->request->isPost()){
             $post = $this->request->getPost();
             if(!empty($post['role'])){
                 $jsonData = User::getData($post['role']);
                 echo json_encode($jsonData);
                 die;
             }
        }
    }

    

    // private function randomCode($count = 6, $type="") {
    //     if($type==""){
            
    //         $count=$count/2;
    //     }
        
    //     $character_set_array = array();
    //     if($type=="" || $type=="chars"){
    //         $character_set_array[] = array('count' => $count, 'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    //     }
    //      if($type=="" || $type=="nums"){
    //             $character_set_array[] = array('count' => $count, 'characters' => '0123456789');
    //      }
    //     $temp_array = array();
    //     foreach ($character_set_array as $character_set) {
    //         for ($i = 0; $i < $character_set['count']; $i++) {
    //             $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
    //         }
    //     }
    //     shuffle($temp_array);
    //     return implode('', $temp_array);
    // }
    
  
}
