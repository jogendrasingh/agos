<?php

/**
 * AdminUser
 * @copyright Copyright (c) 2011 - 2014 Aleksandr Torosh (http://wezoom.com.ua)
 * @author Aleksandr Torosh <webtorua@gmail.com>
 */

namespace User\Form;

use User\Model\User;
use Application\Form\Form;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\Email as ValidatorEmail;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;
use Phalcon\Validation\Validator\Confirmation;
use User\Model\State;


class UserForm extends Form
{

    public function initialize()
    {
        $this->add(
            (new Text('login', [
                'required' => true,
            ]))->setLabel('Login')
        );

        $this->add(
            (new Email('email', [
                'required' => true,
            ]))
                ->addValidator(new ValidatorEmail([
                    'message' => 'Email format is invalid',
                ]))
                ->setLabel('Email')
        );

        $this->add(
            (new Text('name'))
                ->setLabel('Name')
        );

        $this->add(
            (new Select('role', User::$roles,['onChange'=>'getParent()','useEmpty'  =>  false,
//          'emptyText' =>  'Select',
            'using'     => array('id', 'role'),
            'required' => false]))
                ->addValidator(new PresenceOf(['message' => 'role can not be empty']))
                ->setLabel('Role')
        );
        
       
        $this->add(
            (new Select('parent_id', User::getChecker()))
                ->setLabel('Select Checker')
        );
        
        
         $this->add(
            (new Text('mobile_no'))
                 ->addValidator(new RegexValidator([
                     'pattern'  => '/^[0-9]{10}/',
                     'message' => 'Mobile Number Format is not Valid',
                ])) 
                ->setLabel('Mobile No')
        );
       
        
        $this->add(
            (new Password('password',['placeholder'=>'*******']))
                ->addValidator( new Confirmation(array(
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'confirm_password'
            )))
                ->setLabel('Password')
        );
        
        $this->add(
            (new Password('confirm_password'))
                ->setLabel('Confirm Password')
        );

        $this->add(
            (new Check('active'))
                ->setLabel('Active')
        );

        /*$this->add(
            (new TextArea('state'))
                ->setLabel('State')
        );*/

        $this->add(new Text("created_at"));
        $this->add(new Text("updated_at"));

        $this->add((new Select('state', State::query()->columns(array('state_name'))
            ->execute(),
            array(
            'useEmpty'  =>  true,
            'emptyText' =>  'Select',
            'using'     => array('state_name','state_name'),
            'required' => true,
            'multiple' =>true,
            'name' => 'state[]',
            'value'=>$entity->state
            )))
            ->setLabel('Select State'));

        $this->add((new Select('automailer',['1'=>'Yes','0'=>'No']))
            ->setLabel('Auto Mailer'));
    }

    public function initAdding()
    {
        $password = $this->get('password');
        $password->setAttribute('required', true);
        $password->addValidator(new PresenceOf([
            'message' => 'Password is required',
        ]));
    }

}
