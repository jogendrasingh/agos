<?php

namespace User\Model;

class Accesslog extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return "access_log";
    }

    public $id;
    public $login;
    public $role;
    public $user_id;
    public $name;
    public $ip_address;
    public $created_at;
    
    public function initialize()
    {
      
    }
    

}