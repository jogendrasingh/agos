<?php


namespace User\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use stdClass;
//use Phalcon\Mvc\Model\Criteria;
//use Phalcon\Mvc\Model\ValidatorInterface;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class User extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return "user";
    }

    public $id;
    public $role;
    public $login;
    public $email;
    public $name;
    public $password;
    public $state_id;
    public $location_id;
    public $parent_id;
    public $mobile_no;
    public $active = 0;
    public $request_token;
    public $state;
    public $automailer;
    public $created_at;
    public $updated_at; 
    public static $roles = [
        'maker' 	=> 'Maker',
        'checker'       => 'Checker',
        'bank'        => 'Bank User',
        ];
   
    public static function getChecker($params = []){
        $result = self::find(array(
        "conditions" => "role = 'checker'",
        "order" => "id ASC",
       ));
        $list = [];
        foreach ($result as $el) {
         if (isset($params['value']) && $params['value']) {
                    $value = $el->{$params['value']};
                } else {
                    $value = $el->getName();
                }
                if (isset($params['key']) && $params['key']) {
                    $list[$el->{$params['key']}] = $value;
                } else {
                    $list[$el->getId()] = $value;
                }
            }
            return $list;
    }
      
   
    public function initialize()
    {
        require_once __DIR__ . '/../../../../vendor/password.php';
    }

    public function setCheckboxes($post)
    {
        $this->setActive(isset($post['active']) ? 1 : 0);
    }

/*    public function validation()
    {
        
        $this->validate(new Uniqueness(
            [
                "field"   => "login",
                "message" => $this->getDi()->get('helper')->translate("The Login must be unique")
            ]
        ));

        $this->validate(new Uniqueness(
            [
                "field"   => "email",
                "message" => $this->getDi()->get('helper')->translate("The Email must be unique")
            ]
        ));
      
        return $this->validationHasFailed() != true;

    }*/
    
   public function validation()
   {
       $validator = new Validation($context = null);

       $validator->add(
               'email',
               new \Phalcon\Validation\Validator\Uniqueness(array(
                   "model" => $this,
                   "message" => "Value of field 'email' is already present in another record"
               )));

       $validator->add(
           'login',
           new \Phalcon\Validation\Validator\Uniqueness(array(
                   "model" => $this,
                   "message" => "Value of field 'login' is already present in another record"
               )));
      return $this->validate($validator);
   }

    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getEmail()
    {
        return $this->email;
    }
    
    public function getEmailByUserId($id){
        $emails=  User::query()
             ->columns(array('email','name'))
             ->where("id = '".$id."'")
             ->andWhere("active = '1'") 
             ->execute();
            $userInfo=array();
            foreach($emails as $em){
                $userInfo['name']=$em->name;
                $userInfo['email']=$em->email;
            }
            return $userInfo;
    }
    public function getRole()
    {
        return $this->role;
    }
    public function getParent_id()
    {
        return $this->parent_id;
    }

    public function getRoleTitle()
    {
        if (array_key_exists($this->role, self::$roles)) {
            return self::$roles[$this->role];
        }
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPassword()
    {
        return ''; // We don't need hash of password. Just return empty string.
    }

    public function checkPassword($password)
    {
        if (password_verify($password, $this->password)) {
            return true;
        }
    }

    public function getActive()
    {
        return $this->active;
    }

    public function isActive()
    {
        if ($this->active) {
            return true;
        }
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPassword($password)
    {
        if ($password) {
            $this->password = password_hash($password, PASSWORD_DEFAULT);
        }
    }

    public function setActive($active)
    {
        $this->active = $active;
    }
    
    public function getMakerByCheckerId($parId){
        $conditions = "parent_id = ".$parId." AND active = 1";
         $parameters = array(1 => "id");
          
            $childUsers    = User::find(
                array(
                    $conditions,
                     "bind" => $parameters
                  )
            );
            return $childUsers;
    }
    
    public function getCheckerByMakerId($makId){
         $conditions = "id = ".$makId." AND active = 1";
         $parameters = array(1 => "id");
            $parentUsers    = User::find(
                array(
                    $conditions,
                     "bind" => $parameters
                  )
            );
            return $parentUsers;
    }
    
    public function getAuthData()
    {
        $authData = new stdClass();
        $authData->id = $this->getId();
        $authData->user_session = true;
        $authData->login = $this->getLogin();
        $authData->email = $this->getEmail();
        $authData->name = $this->getName();
        $authData->role = $this->getRole();
        $authData->parent_id = $this->getParent_id();
        return $authData;
    }

    public static function getRoleById($id)
    {
        $role = self::findFirst([
            'conditions' => 'id = :id:',
            'bind'       => ['id' => $id],
            'columns'    => ['role'],
            'cache'      => [
                'key'      => HOST_HASH . md5('User\Model\User::getRoleById::' . $id),
                'lifetime' => 60,
            ]
        ]);
        if ($role) {
           return $role->role;
        } else {
            return 'maker';
        }
    }
    
    public static function getData($role){
        if(!empty($role)){
            $result = User::query()->where("role = '".$role."'")->andWhere("active='1'")->execute();
            $list = array();
        if(count($result)>0){
            foreach($result as $data){
                $list[$data->id]= $data->name;  
            }
        }
            return $list;
        }else{
            return false;
        }
    }

    public function beforeCreate()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function beforeUpdate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }
  /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

     public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
  
}
