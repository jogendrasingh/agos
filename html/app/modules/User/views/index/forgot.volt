<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{ url.path() }}vendor/semantic-2.1/semantic.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url.path() }}vendor/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url.get() }}static/css/styles.css" rel="stylesheet">
            <style>
            .error{
                color:red;
                margin:0 0 0 1px;
            }
            </style>
   </head>
    <body>
        <!--login modal-->
        <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
       
            <div class="modal-dialog" style="margin-top:130px !important;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1 class="text-center"><img src="{{ url.get() }}static/images/logo.png"></h1>
                    </div>
                    <div class=" ">
                     
                        <?php if(!empty($_GET['rt'])){ $param="?rt=".$_GET['rt']; }else{$param="";}?>
                        <form class="form col-md-12 center-block" name="reserve" AUTOCOMPLETE = "off" id="reserve" method="post" onsubmit="return ValidateForm();" action="{{ url.get() }}user/index/forgot<?=$param?>">                           
                           <div>{{ flash.output() }}</div>
                            <div class="errorClass error"></div> 
			  <?php if(!empty($_GET['rt'])){?>
                            <div class="form-group field-btm">

                            <input type="password" class="form-control my-form-control input required inputData" name="password" id="password" value="" placeholder="Enter your Password"><span  id="error_username" ></span> 
                                <input type='hidden' name='login' value='<?=$_GET["login"]?>'>
                            </div>
                          <div class="form-group field-btm">

                            <input type="password" class="form-control my-form-control input required inputData" name="confirmPassword" id="cnfPassword" value="" placeholder="Confirm your Password"><span  id="error_username" ></span> 
                            </div>
                           <?php }else{?>
                           <div class="form-group field-btm">

                            <input type="text" class="form-control my-form-control input required inputData" name="login" id="login" value="" placeholder="Enter your login"><span  id="error_username" ></span> 
                            </div>
                           <?php }?>
                            <input type="hidden" name="{{ security.getTokenKey() }}" value="{{ security.getToken() }}"/>
                            <div class="form-group text-center">
                                <button class="btn btn-primary btn-login alt_btn" type="submit" value="Submit" name="submit" id="submit" >Submit</button>
                            </div>
                        </form>
                    </div>
                <div class="modal-footer">
                        <div class="col-md-12 text-center"><br/>
                            <a href="{{ url.get()}}user/index/login"><p>login?</p></a>
                        </div>	
                    </div>
                </div>
            </div>
            <div class="footer text-center">
                <p style="color:#fff;">Copyright © 2016 FormZero. All rights reserved. </p>
            </div>
        </div>
        <!-- script references -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
                        <?php if(!empty($_GET['rt'])){?>
                        <script>
                            function ValidateForm(){
                                var password = $("#password").val();
                                var cnfPassword = $("#cnfPassword").val();
                                if(password==''){
                                   //alert("Password can't be empty");
                                   $(".errorClass").html("Password can't be empty");
                                    return false; 
                                }else if(cnfPassword==''){
                                    //alert("Confirm Password can't be empty");
                                    $(".errorClass").html("Confirm Password can't be empty");                        
                                    return false; 
                                }else if(password!==cnfPassword){
                                    //alert("Password not Match");
                                    $(".errorClass").html("Password not Match");
                                    return false;
                                }else{
                                    return true;
                                }
                            }
                        </script>
<?php } ?>



