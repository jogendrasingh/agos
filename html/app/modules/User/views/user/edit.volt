<form method="post" action="" class="ui form">

    <!--controls-->
    <div class="ui segment">

        <a href="{{ url.get() }}user/user" class="ui button">
            <i class="icon left arrow"></i> Back
        </a>

        <div class="ui positive submit button">
            <i class="save icon"></i> Save
        </div>
             
        {% if(not(model.id is empty)) %}
            <a href="{{ url.get() }}user/user/delete/{{ model.id }}" class="ui button red">
                <i class="icon trash"></i> Delete
            </a>
        {% endif %}
        
    </div>
    <!--end controls-->
    <div class="ui segment">
        <div class="two fields">
            <div class="field">
                {{ form.renderDecorated('login') }}
                {{ form.renderDecorated('email') }}
                {{ form.renderDecorated('name') }}
                {{ form.renderDecorated('mobile_no') }}
                {{ form.renderDecorated('role') }}
            </div>
            <div class="field">
                <div id='parentDiv'>
                    <div class="field">
                        <label for="parent_id">Select Checker</label>
                        <select name="parent_id" id="parent_id">
                        </select>
                    </div>
                </div>
                {{ form.renderDecorated('password') }}
                {{ form.renderDecorated('confirm_password') }}
                {{ form.renderDecorated('active') }}
                <div id="stateDiv" style="display:none;">
                {{ form.renderDecorated('state') }}
                {{ form.renderDecorated('automailer') }}
                </div>

            </div>
        </div>
    </div>

</form>

<script>
    $('.ui.form').form({
           fields: {
            login: {
                identifier: 'login',
                rules: [
                    {type: 'empty'}
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {type: 'empty'}
                ]
            },
            name: {
                identifier: 'name',
                rules: [
                    {type: 'empty'}
                ]
            },
            role: {
                identifier: 'role',
                rules: [
                    {type: 'empty'}
                ]
            }
    }
 });
    
    
    
    function getParent(){
    var roleOfUser=$("#role").val();
    if(roleOfUser=='maker'){
      $("#parentDiv").show();
    }else{
      $("#parentDiv").hide();
    }

    if(roleOfUser=='bank'){
      $("#stateDiv").show();
    }else{
      $("#stateDiv").hide();
    }
    
    if(roleOfUser=='maker'){
        $("#parent_id").append().empty();
            $.ajax({
            url:'{{ url.get() }}user/user/getData',
            data:{role:'checker'},
            type:'POST',
            dataType:'json',
            success:function(response){
              $.each(response,function(index,value){
              $("#parent_id").append("<option value='"+index+"'>"+value+"</option>");
                });
                    {% if(not(parent is empty)) %}
                    {% for parentitem in parent %}
                        $("#parent_id > option[value='{{parentitem}}']").attr("selected", "true");
                    {% endfor%}
                    {% endif%} 
            
            },
            error:function(){alert('Failed!')}
        });     
    }
  } 

function parentEmpty(){
         $("#parent_id").append().empty();
         $("#role").val('');
 }



$(document).ready(function(){
    getParent();
}); 

{% if model.state %}
var state = '{{model.state}}';
    $.each(state.split(','), function(key, value) {
        $("#state option[value='" + $.trim(value) + "']").prop("selected", true);
    });

{% endif %}
          
</script>