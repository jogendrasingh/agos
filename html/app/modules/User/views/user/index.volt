<link href="{{ url.path() }}vendor/datagrid/media/css/demo_page.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/media/css/demo_table.css" rel="stylesheet" type="text/css">
<link href="{{ url.path() }}vendor/datagrid/extras/TableTools/media/css/TableTools.css" rel="stylesheet" type="text/css">
<!--controls-->
<div class="ui segment">
 <a href="{{ url.get() }}user/user/add" class="ui button positive">
        <i class="icon plus"></i> {{ helper.at('Add New') }}
    </a>

</div>
<!--/end controls-->

<table class="ui table very compact celled" {% if entries|length is not '' %} id='userId' {% endif%}>
    <thead>
    <tr>
        <th style="width:2%"></th>
        <th style="width:20%">{{ helper.at('Login') }}</th>
        <th style="width:15%">{{ helper.at('Email') }}</th>
        <th style="width:15%">{{ helper.at('Name') }}</th>
        <th style="width:5%">{{ helper.at('Role') }}</th>
        <th style="width:40%">{{ helper.at('State') }}</th>
        <th style="width:40%">{{ helper.at('Auto mailer') }}</th>
        <th style="width:5%">{{ helper.at('Active') }}</th>
    </tr>
    </thead>
    <tbody>
    {% for user in entries %}
        <tr>
           
            {% set url = url.get() ~ 'user/user/edit/' ~ user.getId() %}
            <td><a href="{{ url }}" class="mini ui icon button"><i class="pencil icon"></i></a></td>
            <td><a href="{{ url }}">{{ user.getLogin() }}</a></td>
            <td>{{ user.getEmail() }}</td>
            <td>{{ user.getName() }}</td>
            <td>{{ user.getRoleTitle() }}</td>
            <td> <?php echo str_replace(',',', ',$user->state);?></td>
            <td><?php if($user->role=='bank') {if($user->automailer=='1'){?>Yes<?php }else{?>No<?php }}else{?>-<?php }?></td>
            <td>{% if user.getActive() %}<i class="icon checkmark green"></i>{% endif %}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="{{ url.path() }}vendor/datagrid/extras/TableTools/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
        $(document).ready( function () {
                $('#userId').dataTable( {
                        "sDom": 'T<"clear">lfrtip',
                        "aaSorting": [],
                        "oTableTools": {
                                "sSwfPath": "{{ url.path() }}vendor/datagrid/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                } );

        } );


</script>