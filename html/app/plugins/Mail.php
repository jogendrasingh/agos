<?php

/**
 * @copyright Copyright (c) 2011 - 2015 Oleksandr Torosh (http://wezoom.net)
 * @author Oleksandr Torosh <web@wezoom.net>
 */

namespace YonaCMS\Plugin;

//use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\User\Plugin,
 Phalcon\Ext\Mailer\Message;
class Mail extends Plugin
{

    public function __construct()
    {
        
    }
    
        public function sendMail($to,$name,$subject,$body,$cc=null,$attachment=null){
       
        $configSite = $this->di->get('config');         
        $config = [
            'driver' => $configSite['mail']->driver,
            'host' => $configSite['mail']->host,
            'port' => $configSite['mail']->port,
            'username' => $configSite['mail']->username,
            'password' => $configSite['mail']->password,
            'from' => [
                'email' => $configSite['mail']->from->email,
                'name' => $configSite['mail']->from->name
            ]
        ];
        
        $mailer = new \Phalcon\Ext\Mailer\Manager($config);

        $message = $mailer->createMessage()
                ->to($to, $name)
                ->subject($subject)
                ->content($body);
        if($attachment!=null){
            $message->attachment($attachment);                
        }
        $message->cc($cc);
        $message->bcc('logsformzero@gmail.com');
        $message->send();

        $trace = debug_backtrace();
        $line = $trace[0];
        $mailSent['calledFrom'] = $line['file'] . "  " . $line['line'];
        $mailSent['to'] = $to;
        $mailSent['subject'] = $subject;
        $mailSent['body'] = strip_tags($body);
        $mailSent['addedOn'] = date("Y-m-d H:i:s");
        
        $model =  new SystemEmailsLog();
        $model->calledFrom  =$mailSent['calledFrom'];
        $model->to  =$mailSent['to'];
        $model->subject  = $mailSent['subject'];
        $model->body  = $mailSent['body'];
        $model->addedOn  = $mailSent['addedOn'];
        $model->save();

        return true;
    }
 
} 