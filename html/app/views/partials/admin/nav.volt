<!doctype html><div class="ui left fixed vertical pointing inverted menu">
    <a class="item{{ helper.activeMenu().activeClass('admin-home') }} header" href="{{ url(['for': 'admin']) }}">
        AGOS
    </a>
    <div class="item">
        <div class="header">{{ helper.at('Admin') }} <i class="wrench icon"></i></div>

        <div class="menu">
            <?php $auth = $this->session->get('auth');
            if($auth->role=='admin'){
            ?>
            <a class="item{{ helper.activeMenu().activeClass('admin-user') }}" href="{{ url.get() }}admin/admin-user">
                {{ helper.at('Manage Admin Users') }} <i class="user icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('station') }}" href="{{ url.get() }}station/admin">
                {{ helper.at('Manage Stations') }} <i class="user icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('client') }}" href="{{ url.get() }}client/admin">
                {{ helper.at('Manage Clients') }} <i class="code icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('flight') }}" href="{{ url.get() }}flight/admin">
                {{ helper.at('Manage Flights') }} <i class="globe icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('loadmaster') }}" href="{{ url.get() }}load-master/admin">
                {{ helper.at('Manage LoadMaster') }} <i class="globe icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('training') }}" href="{{ url.get() }}training/admin">
                {{ helper.at('Manage Training') }} <i class="user icon"></i>
            </a>
            <a class="item{{ helper.activeMenu().activeClass('viewexpenses') }}" href="{{ url.get() }}load-master/admin/viewexpenses">
                {{ helper.at('View Expenses') }} <i class="user icon"></i>
            </a>
            <?php }else{ ?>
            <a class="item{{ helper.activeMenu().activeClass('expenses') }}" href="{{ url.get() }}load-master/admin/expenses">
                {{ helper.at('Manage Expenses') }} <i class="globe icon"></i>
            </a>
            <?php } ?>

        </div>
    </div>
    <div class="item">
        <a href="javascript:void(0);" class="ui tiny button" onclick="document.getElementById('logout-form').submit()">
            <i class="plane icon"></i>{{ helper.at('Logout') }}
        </a>

        <form action="{{ url.get() }}admin/index/logout" method="post" style="display: none;" id="logout-form">
            <input type="hidden" name="{{ security.getTokenKey() }}"
                   value="{{ security.getToken() }}">
        </form>
    </div>
</div>