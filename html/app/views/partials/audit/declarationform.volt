<!doctype html><h4>D. Document</h4>    
<div class='row'>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether stock register available & updated?:<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("stock_register_available", "class":"warehouse-form form-control",["":"SELECT","YES":"YES","NO":"NO"], "value":model.stock_register_available) }}</div>
    </div>
     
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether lien/pledge register available & updated? :<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("lien_register", "class":"warehouse-form form-control",["":"SELECT","YES":"YES","NO":"NO"], "value":model.lien_register) }}</div>
    </div>
    
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether visitor register available & updated? :<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("visitor_register", "class":"warehouse-form form-control",["":"SELECT","YES":"YES","NO":"NO"], "value":model.visitor_register) }}</div>
    </div>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether supervisor/security guard attendance register available & updated?:<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("guard_attendance_register", "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.guard_attendance_register) }}</div>
    </div>
     <h4>E. Others</h4>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether stock shifted to another location without approval?:<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("stock_shifted_without_approval", "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"],  "value":model.stock_shifted_without_approval) }}</div>
    </div>
     <div class='col-md-12'>   
      <div class='col-md-4'><label>Other comments, if any :<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ text_field("other_comments", "class":"warehouse-form form-control", "value":model.other_comments) }}</div>
     </div>
     <h4>F. Photographic Report</h4>
     <div class='col-md-12'>   
      <div class='col-md-4'><label>Warehouse Photo with Auditor and CM Photo (With outside wh structure in view):<span class='red'>*</span> </label></div> <div class='col-md-4'>
      <input type="file" name="warehouse_photo" value="" class="warehouse-form form-control" accept="image/*;capture=camera"></div>
    </div>
    
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Visitor Register Photo (With your visit date visible in photo):<span class='red'>*</span> </label></div> <div class='col-md-4'>
      <input type="file" name="visitor_register_photo" value="" class="warehouse-form form-control" accept="image/*;capture=camera"></div>
    </div>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Stock Register Photo :<span class='red'>*</span> </label></div> <div class='col-md-4'>
      <input type="file" name="stock_register_photo" value="" class="warehouse-form form-control" accept="image/*;capture=camera"></div>
    </div>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Stack Card Photo :<span class='red'>*</span> </label></div> <div class='col-md-4'><input type="file" name="stack_card_photo" value="" class="warehouse-form form-control" accept="image/*;capture=camera"></div>
    </div>
    
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Stack Arrangement Photo   :<span class='red'>*</span> </label></div> <div class='col-md-4'><input type="file" name="stack_arrangement_photo" value="" class="warehouse-form form-control" accept="image/*;capture=camera">
      </div>
    </div>
    
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Commodity photo:<span class='red'>*</span> </label></div> <div class='col-md-4'><input type="file" name="commodity_photo" value="" class="warehouse-form form-control" accept="image/*;capture=camera"></div>
    </div>
     
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Extra Photo 1 :<span class='red'>*</span> </label></div> <div class='col-md-4'><input type="file" name="extra_photo1" value="" class="warehouse-form form-control" accept="image/*;capture=camera"></div>
    </div>
    
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Extra Photo 2:<span class='red'>*</span> </label></div> <div class='col-md-4'><input type="file" name="extra_photo2" value="" class="warehouse-form form-control" accept="image/*;capture=camera"></div>
    </div>
    </div>
    
    <div class='col-md-12 pull-right' style="margin-top:15px;">   
       {{ submit_button("Save Warehouse", "class":"noajax btn btn-primary btn-login", 'onSubmit':"validate()") }}
    </div> 