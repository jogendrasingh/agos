<!doctype html><h4>B. Commodity </h4>    
<div class='row'>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>In case of factory godown, whether commodity other than sugar, paddy, rice, cotton, maize, wheat & smp is funded?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("commodity","class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.commodity) }}</div>
    </div>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether shortage in bags found (In number of units): <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("bags", "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.bags) }}</div>
    </div>
    <div class='col-md-12'>   
         <div class='col-md-4'><label>Whether shortage in weight (In volume terms): <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("weight", "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.weight) }}</div>
    </div>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Same stock under fumigation since last 2 month: <span class='red'>*</span></label></div> <div class='col-md-4'>{{ select_static("v","class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.stock_under_fumigation) }}</div>
    </div>
    <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether heavy insect infestation / deterioration/ fungal growth observed in the stocks?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("heavy_insect_infestation","class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.heavy_insect_infestation) }}</div>
    </div>
     <div class='col-md-12'>   
      <div class='col-md-4'><label>Whether fumigation / spraying required? : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("fumigation_required","class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.fumigation_required) }}</div>
     </div>
    

     <div class='col-md-12'>   
         <div class='col-md-4'><label>Overall stock health: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("stock_health","class":"warehouse-form form-control", ["":"SELECT","GOOD":"GOOD","AVERAGE":"AVERAGE","POOR":"POOR"], "value":model.stock_health) }}</div>
     </div>
    
      <div class='col-md-12'>   
      <div class='col-md-4'><label>Any other commodity related remarks: <span class='red'>*</span></label></div> <div class='col-md-4'>{{ text_field("commodity_related_remarks","class":"warehouse-form form-control","value":model.commodity_related_remarks) }}</div>
      </div>
    </div>