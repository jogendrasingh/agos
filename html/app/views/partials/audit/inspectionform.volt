<!doctype html><h4>A. Warehoouse</h4>    
<div class='row'>
    <div class=' col-md-12 main-section'> 
        <div class='col-md-4'><label>warehouse Code:<span class='red'>*</span> </label></div><div class='col-md-4'>{{ text_field("warehouse_code",  "class":"warehouse-form form-control form-control", "value":auditPrefilledData['warehouse_code']) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Full address of the warehouse given by bank:<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ text_field("full_address", "class":"warehouse-form form-control","value":auditPrefilledData['full_address']) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Actual address of the warehouse:<span class='red'>*</span> </label></div> <div class='col-md-4'>
            {{ text_field("actual_address", "class":"warehouse-form form-control form-control","value":model.actual_address) }}
        </div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Type of structure:<span class='red'>*</span> </label></div> <div class='col-md-4 updatedCode'>{{ text_field("type_of_structure", "class":"warehouse-form form-control","value":model.type_of_structure) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
    
        <div class='col-md-4'><label>Name of the collateral manager:<span class='red'>*</span> </label></div> <div class='col-md-4 updatedCode'>{{ select("col_mgr_name",  "class":"warehouse-form form-control",staticData['collateralManger'],"value":model.col_mgr_name) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Who has the control on lock and key?:<span class='red'>*</span> </label></div> <div class='col-md-4 '>{{ select("control_on_lock_key",  "class":"warehouse-form form-control",staticData['lockKey'],"value":model.control_on_lock_key) }}</div>
    </div>

    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether the lock & key has been compromised?:<span class='red'>*</span> </label></div> <div class='col-md-4 '>{{ text_field("compromised_lock_key",  "class":"warehouse-form form-control","value":model.compromised_lock_key) }}</div>
    </div>

    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether the warehouse owner related to the borrower/ borrowing unit? If yes, specify the details :<span class='red'>*</span> </label></div> <div class='col-md-4'>{{ text_field("owner_related_to_borrower",  "class":"warehouse-form form-control","value":model.owner_related_to_borrower) }}</div>
    </div>

    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Plinth height:<span class='red'>*</span> </label></div> <div class='col-md-4 updatedCode'>{{ select("plinth_height", "class":"warehouse-form form-control",staticData['plinthHeight'],"value":model.plinth_height) }}</div>
    </div>
    <div class=' col-md-12 main-section'> 
        <div class='col-md-4'><label>Live electricity noticed in the warehouse: <span class='red'>*</span> </label></div><div class='col-md-4 updatedCode'>{{ select_static("live_electricity_noticed", "class":"warehouse-form form-control form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.live_electricity_noticed) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether flooring of the warehouse is proper?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("flooring","class":"warehouse-form form-control",["":"SELECT","YES":"YES","NO":"NO","NA":"NA"],"value":model.flooring) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether roofing of the warehouse is proper?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("roofing", "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.roofing) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether there is proper ventilation in warehouse/godown? : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ text_field("ventilation",  "class":"warehouse-form form-control","value":model.ventilation) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether fire fighting equipments have been installed at the warehouse? If yes, mention the expiry date of the fire fighting equipment? : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("fire_fighting_equipment",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.fire_fighting_equipment) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether bank's nameplate displayed outside the warehouse? : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("bank_nameplate",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.bank_nameplate) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether stack cards has the banks name as pledgee and are updated?  : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("stack_card_bank_name",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.stack_card_bank_name) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether polythene sheet, Tarpaulin sheets or wooden racks used as dunnage? If no, please mention the type of dunnage used. : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ text_field("dunnage",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","YES-PARTIAL":"YES-PARTIAL","NO":"NO","NA":"NA"], "value":model.dunnage) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether the stacking arrangement is proper?   : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("stacking_arrangement",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","MAIN CASE":"MAIN CASE"], "value":model.stacking_arrangement) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Wheter stock is in countable position?  : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("stock_countable",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.stock_countable) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether bank funded stocks are properly segregated/demarcated? : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("bank_fund_stock",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.bank_fund_stock) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether borrower wise stocks are segregated/demarcated? : <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("borrower_wise_stock",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.borrower_wise_stock) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether any instance of different stocks funded by multiple banks is kept inside the storage structure? ( Appicable for warehouses other than professional, private licensed warehouses/cold storages) If yes, specify details of stock funded by other banks (Quantity(quintals), Number of bags) etc, whether there is segregation of stocks of multiple banks?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("instance_of_different_stock",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.instance_of_different_stock) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Whether proper alleyways are left ofr inspection of the stock?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("proper_alleyways",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA (IN CASE OF SILO)"], "value":model.proper_alleyways) }}</div>
    </div>

    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Rodents noticed in the warehouse?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("rodents",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.rodents) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Spillage noticed in the warehouse?: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("spillage",  "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO"], "value":model.spillage) }}</div>
    </div> 
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Overall condition of storage structure: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ select_static("overall_condition_of_storage",  "class":"warehouse-form form-control", ["":"SELECT","GOOD":"GOOD","AVERAGE":"AVERAGE","POOR","POOR"], "value":model.overall_condition_of_storage) }}</div>
    </div>
    <div class=' col-md-12 main-section'>   
        <div class='col-md-4'><label>Any other warehouse related remarks: <span class='red'>*</span> </label></div> <div class='col-md-4'>{{ text_field("warehouse_related_remarks",  "class":"warehouse-form form-control","value":model.warehouse_related_remarks) }}</div>
    </div>      


</div>
