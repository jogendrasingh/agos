<!doctype html><h4>C. Collateral Manager / Warehouse Manager</h4>    
<div class='row'>
    <div class='col-md-12'>   
        <div class='col-md-4'><label>Whether CM's supervisor available during visit?: <span class='red'>*</span></label></div> <div class='col-md-4'>{{ select_static("cm_available", "class":"warehouse-form form-control",["":"SELECT","YES":"YES","NO":"NO"], "value":model.cm_available) }}</div>
    </div>
    
    <div class='col-md-12'>   
        <div class='col-md-4'><label>Frequency of godown inspection by CM's supervisor: <span class='red'>*</span></label></div> <div class='col-md-4'>{{ select_static("frequency_of_inspection","class":"warehouse-form form-control", ["":"SELECT","DAILY":"DAILY","ALTERNATE DAYS":"ALTERNATE DAYS","ONCE IN3-4 DAYS":"ONCE IN3-4 DAYS","WEEKLY":"WEEKLY"], "value":model.frequency_of_inspection) }}</div>
    </div>

    <div class='col-md-12'>   
        <div class='col-md-4'><label>Frequency of the visits of the audit team of the cms to the godowns/warehouses?: <span class='red'>*</span></label></div> <div class='col-md-4'>{{ select_static("frequency_of_audit_team",  "class":"warehouse-form form-control", ["":"SELECT","MONTHLY":"MONTHLY","QUARTERLY":"QUARTERLY","HALF YEARLY":"HALF YEARLY","YEARLY":"YEARLY","ADHOC":"ADHOC"], "value":model.frequency_of_audit_team) }}</div>
    </div>

    <div class='col-md-12'>   
        <div class='col-md-4'><label>Whether security guard available during visit?: <span class='red'>*</span></label></div> <div class='col-md-4'>{{ select_static("security_guard_available", "class":"warehouse-form form-control", ["":"SELECT","YES":"YES","NO":"NO","NA":"NA"], "value":model.security_guard_available) }}</div>
    </div>
    
    <div class='col-md-12'>       
        <div class='col-md-4'><label>Overall quality of the CM Services: <span class='red'>*</span></label></div> <div class='col-md-4'>{{ select_static("overall_quality", "class":"warehouse-form form-control", ["":"SELECT","EXCELLENT":"EXCELLENT","GOOD":"GOOD","AVERAGE":"AVERAGE","NOT SATISFACTORY":"NOT SATISFACTORY"], "value":model.overall_quality) }}</div>
    </div>
    
    <div class='col-md-12'>  
        <div class='col-md-4'><label>Collateral manager remarks : <span class='red'>*</span></label></div> <div class='col-md-4'>{{ text_field("collateral_manager_remarks", "class":"warehouse-form form-control","value":model.collateral_manager_remarks) }}</div>
    </div>
    
</div>
