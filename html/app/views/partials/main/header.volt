<!doctype html>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<link href="{{ url.get() }}static/css/styles.css" rel="stylesheet">
<link href="{{ url.get() }}static/css/jquery.steps.css" rel="stylesheet">
<script src="{{ url.get() }}static/js/jquery.steps.js"></script>
<div style="margin-left:4%;margin-top:2%;" class="col-sm-11">
    <?php
    $module = $this->router->getModuleName();
    $action = $this->router->getActionName();
    ?>
    <?php
    if($module == 'audit' && $action == 'index'){
    ?>
    <?php
    }else{
    ?>
    <a href="{{ url.get()}}audit/user/index" style="font-size:16px;color:#fff;" class="noajax"><i class="fa fa-angle-left"></i> Back To Index</a> 

    <?php
    }
    ?>
    <a href="{{ url.get()}}user/index/logout" style="font-size:16px;color:#fff;" class="noajax pull-right"><i class="fa fa-power-off"></i> Logout</a> 
    <a class="pull-right" style="font-size:16px; color:#fff;margin-right:25px;" href="">Logged In User: {{session.get('auth').name}}({{session.get('auth').role}})</a>
</div>
         